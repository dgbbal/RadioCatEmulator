#ifndef ELECRAFT_H
#define ELECRAFT_H

#include <QObject>
#include "rigbase.h"
#include "rigcapabilities.h"
#include "rigfactory.h"
#include "radiostatus.h"
#include "configrotserial.h"


const QString K3 = "Elecraft K3";


class Elecraft : public RigBase
{
    Q_OBJECT

public:
    explicit Elecraft(int id, QString modelName, RadioStatus *radioStatus_, ConfigRotSerial *configSerial_, QObject *parent = nullptr);
    virtual ~Elecraft() override;



    static void register_rigs(RigFactory::Rigs*);
    void initRadio( Bandlist* bandList_, QStringList *supportedModes, QStringList *supportedModeCodes, QChar terminator_, QChar civAddress_ = ' ', QString kenwoodYaesuId_ = "") override;


    void setMaxRitFreq(int f) override;
    void setMinRitFreq(int f) override;
signals:
    //void catRxMsg(QString);
    //void catTxMsg(QString);


private:

    RadioStatus* radioStatus;

    QString inMsg;
    QString rxMessage;
    QString catTxMessage;
    QString modelName;
    int id;
    QChar terminator;

    Bandlist* bandList;
    //QList<int> supportedBandStartFreq;

    QStringList *supportedModes;
    QStringList *supportedModeCodes;
    int maxRitFreq = +9999;
    int minRitFreq = -9999;
    QMap<QString, QString> modeStrToModeTable;

    void handleCatMessage(QByteArray) override;

    void readCatFA_Response();
    void readCatFB_Response();
    void assembleMsgToSendToCat(QString &preamble, QString &msgBody, QString &terminator);
    void getFreqToSendToCat(QString &f, bool vfo);
    void getRitFreqToSendToCat(QString &f);
    void readCatAI_Response();
    void readCatDA_Response();
    void readCatMD_Response();
    void readCatRX_Response();


    void readCatPS_Response();
    void readCatID_Response();
    void readCatFV_Response();

    void readCatIF_Response();
    void TS590S_IF_Response();
    void TS2000_IF_Response();



    QString getStatusOfMode();
    void setFACommand();
    void setFBCommand();
    void setMDCommand();

    QString convertModetoQString(QString mode);

    QChar convertQStringtoModeCode(QString m);



    void setRCCommand();
    void readCatRT_Response();
    void setRTCommand();
    void setRIT_Up();
    void setRIT_Down();
    void sendCatTxMsg();
    void readCatK2_Response();
    void readCatK3_Response();
    void readCatRV_response(QString subCmd);
    void readCatBW_Response(QString subRx);
    void readCatAG_Response(QString subRx);
    void readCatSM_Response(QString subRx);
    void setAGCommand(QString subRx, QString value);
    void setRitOffsetCommand();
};

#endif // ELECRAFT_H
