#ifndef BANDLIST_H
#define BANDLIST_H

#include <QObject>
#include <QMap>

class BandDetials
{
public:
    quint32 getHighFreq(){return highfreq;}
    void setHighFreq(quint32 f){highfreq = f;}
    QString getHighFreqStr(){return highfreqStr;}
    void setHighFreqStr(QString f){highfreqStr = f;}
    quint32 getLowFreq(){return lowfreq;}
    void setLowFreq(quint32 f){lowfreq = f;}
    QString getLowFreqStr(){return lowfreqStr;}
    void setLowFreqStr(QString f){lowfreqStr = f;}
    QString getBand(){return band;}
    void setBand(QString b){band = b;}


private:
    QString band;
    quint32 highfreq;
    QString highfreqStr;
    quint32 lowfreq;
    QString lowfreqStr;
};


class Bandlist
{
public:
    Bandlist();


    quint32 getBandListHighf(QString band);
    quint32 getBandListLowf(QString band);


    bool findBand(QString& band, quint32 f);
    void setBandDetail(QString &band, BandDetials &bd);
    BandDetials getBandDetail(QString &band);
    void clear();
private:

    QStringList availBands;
    int numBands;
    QMap<QString, BandDetials> bandList;
};

#endif // BANDLIST_H
