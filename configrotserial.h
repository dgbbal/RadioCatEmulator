#ifndef CONFIGROTSERIAL_H
#define CONFIGROTSERIAL_H

#include <QObject>
#include <QComboBox>
#include <QSerialPort>
#include <QByteArray>
#include <QSerialPortInfo>
//#include "serialcomms.h"



static const char blankString[] = QT_TRANSLATE_NOOP("SettingsDialog", "N/A");

class ConfigRotSerial : public QObject
{
    Q_OBJECT
public:
    explicit ConfigRotSerial(QObject *parent = nullptr);

    void sendCatMessage(const QByteArray &msg);
    void closeComport();

    void fillPortsInfo(QComboBox* comportSel);
    bool openComport(QString comport, int baudRateIdx, QSerialPort::DataBits databits, QSerialPort::StopBits stopbits);
    QString error();


    bool getOpenFlag();

    bool ctsFlag = false;

    QString getReceivedMsg();
    QByteArray& getBuffer(){return buffer;}
    QString& getRxMessage(){return rxMsg;}


    QByteArray *getRxBuffer();

    void startPttTimer(int duration);
    void stopPttTimer();
    void setFlowControl(QSerialPort::FlowControl flowControl);
signals:
    void dataReceived();
    void messageReceived();
    void txError();
    void ctsOn(bool);

private slots:
    void onReadyRead();
    void onCtsTimeout();
private:

    QSerialPort *sComPort = nullptr;
    
    QTimer *ctsTimer;
    QByteArray buffer;

    QString inMsg;
    QString rxMsg;
    bool openFlag = false;
    QStringList comportErrMsgs = { "No Error", "Device Not Found", "Permission Error"
                                   ,"Open Error", "Parity Error", "Framing Error"
                                   ,"Break Condition", "Write Error", "Read Error"
                                   ,"Resource Error", "Unsupported Operation Error"
                                   ,"Unknown Error", "Timeout Error", "Not Open Error"
                                  };




};

#endif // CONFIGROTSERIAL_H
