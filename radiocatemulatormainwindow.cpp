#include "radiocatemulatormainwindow.h"
#include "ui_radiocatemulatormainwindow.h"
#include "utils.h"
#include "rigCommon.h"
#include "MTrace.h"
#include <QSettings>
#include <QMessageBox>





RadioCatEmulatorMainWindow::RadioCatEmulatorMainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::RadioCatEmulatorMainWindow)
{
    ui->setupUi(this);

    configSerial = new ConfigRotSerial();       // configSerial would be better named CatSerial
    configSerial->fillPortsInfo(ui->comportCombo);
    populateCombo(ui->baudrateCombo , baudStrList, true);
    populateCombo(ui->databitsCombo, databitsStrList, true);
    populateCombo(ui->stopbitsCombo, stopbitsStrList, true);
    
    pttSerialPort = new ConfigRotSerial();
    pttSerialPort->fillPortsInfo(ui->pttComportCombo);

    databits = "8";
    stopbits = "1";

    enableTrace( "./TraceLog", "radioEmulator_" );


    // allow capture of events from these widgets
    ui->MainFreqDial->installEventFilter(this);
    ui->ritDial->installEventFilter(this);

    //ui->modeLabel->setText(" ");
    ui->civTitleLabel->setVisible(false);
    ui->civAddressLabel->setVisible(false);

    // main freq tuning
    //connect(ui->freqInput, SIGNAL(lostFocus()), this, SLOT(exitFreqEdit()));
    connect(ui->MainFreqDial, SIGNAL(freqEditReturn()), this, SLOT(onRadioFreqEditReturn()));
    connect(ui->MainFreqDial, SIGNAL(newFreq()), this, SLOT(onNewDialFreq()));

    connect(ui->bandCombo, SIGNAL(activated(int)), this, SLOT(onRadioBandFreq(int)));
    connect(ui->modeCombo, SIGNAL(activated(int)), this, SLOT(onRadioModeChanged(int)));
    connect(ui->comportCombo, SIGNAL(activated(int)), this, SLOT(onComportChanged(int)));
    connect(ui->baudrateCombo, SIGNAL(activated(int)), this, SLOT(onbaudRateChanged(int)));
    connect(ui->databitsCombo, SIGNAL(activated(int)), this, SLOT(onDataBitsChanged(int)));
    connect(ui->stopbitsCombo, SIGNAL(activated(int)), this, SLOT(onStopBitsChanged(int)));
    connect(ui->radioModelCombo, SIGNAL(activated(int)), this, SLOT(onRadioModelChanged(int)));
    connect(ui->saveRadioButton, SIGNAL(clicked()), this, SLOT(onSaveRadioButtonClicked()));
    connect(ui->txPushButton, SIGNAL(clicked()), this, SLOT(onTxPushButtonClicked()));


    // rit freq tuning
    connect(ui->ritDial, SIGNAL(newFreq(int)), this, SLOT(onNewRitFreq(int)));
    connect(ui->ritPushButton, SIGNAL(clicked()), this, SLOT(onRitButtonClicked()));
    connect(ui->clearRitPb, SIGNAL(clicked()), this, SLOT(onCearRitPbClicked()));

    // vfo
    connect(ui->vfoPushbutton, SIGNAL(clicked()), this, SLOT(onVfoButtonClicked()));

    // volume control updates to radio
    connect(ui->volumeSlider, SIGNAL(valueChanged(int)), this, SLOT(onVolumeChanged(int)));

    //connect(configSerial, SIGNAL(dataReceived()), this, SLOT(onSerialDataReceived()));
    connect(configSerial, SIGNAL(txError()), this, SLOT(onTxError()));

    connect(ui->pttComportCombo, SIGNAL(activated(int)), this, SLOT(onCtsComportChanged(int)));


    connect(ui->catRadioBut, SIGNAL(clicked()), this, SLOT(onCatTxButtonClicked()));
    connect(ui->ctsRadioBut, SIGNAL(clicked()), this, SLOT(onCtsButtonClicked()));

    radioStatus = new RadioStatus();

    radioStatus->setVfo(radioStatus->VFOA);
    setVfoLabel();
    //radioStatus->setBand(radioStatus->getBand());
    //radioStatus->setFreqA(144160000);
    //radioStatus->setFreqB(144200000);
    //radioStatus->setMode(RadioStatus::USB);
    //setModeLabel();
    radioStatus->setRitState(radioStatus->RIT_OFF);
    setRitLabel();


    //ui->freqDial->setText(convertFreqStrDisp(QString::number(radioStatus->getFreqA())));
    //ui->ritDial->setText(convertRitFreqToStr(radioStatus->getRitFreq()));
    //ui->modeCombo->setCurrentIndex(RadioStatus::USB + 1);

    radio = nullptr;
    rigFactory = new RigFactory(this);
    rigFactory->populateComboRigList(ui->radioModelCombo);



}



RadioCatEmulatorMainWindow::~RadioCatEmulatorMainWindow()
{
    delete ui;
}

void RadioCatEmulatorMainWindow::onRadioFreqEditReturn()
{

    changeMainRadioFreq();
    exitFreqEdit();


}



void RadioCatEmulatorMainWindow::onRitButtonClicked()
{
    if (radioStatus->getRitState())
    {
        radioStatus->setRitState(radioStatus->RIT_OFF);
        setRitLabel();
    }
    else
    {
        radioStatus->setRitState(radioStatus->RIT_ON);
        setRitLabel();
    }
}


void RadioCatEmulatorMainWindow::onCearRitPbClicked()
{

    radioStatus->setRitFreq(0);
    onRadioStatusUpdated();
}

void RadioCatEmulatorMainWindow::onVfoButtonClicked()
{
    if (radio != nullptr)
    {
        if (radioStatus->getVfo())
        {
            // set to VFOB
            radioStatus->setVfo(radioStatus->VFOB);
            setVfoLabel();
            if (radioStatus->getCurrentBandB().isEmpty())
            {
                ui->bandCombo->setCurrentIndex(0);

            }
            else
            {
                ui->bandCombo->setCurrentText(radioStatus->getCurrentBandB());

            }

            if (radioStatus->getModeB().isEmpty())
            {
                ui->modeCombo->setCurrentIndex(0);
                //setModeLabel("    ");
            }
            else
            {
                ui->modeCombo->setCurrentText(radioStatus->getModeB());
                //setModeLabel(radioStatus->getModeB());
            }

            ui->MainFreqDial->setText(convertFreqStrDisp(QString::number(radioStatus->getFreqB())));
        }
        else
        {
            // set to VFO A
            radioStatus->setVfo(radioStatus->VFOA);
            setVfoLabel();
            if (radioStatus->getCurrentBandA().isEmpty())
            {
                ui->bandCombo->setCurrentIndex(0);

            }
            else
            {
                ui->bandCombo->setCurrentText(radioStatus->getCurrentBandA());
            }

            if (radioStatus->getModeA().isEmpty())
            {
                ui->modeCombo->setCurrentIndex(0);
                setModeLabel("    ");
            }
            else
            {
                ui->modeCombo->setCurrentText(radioStatus->getModeA());
                setModeLabel(radioStatus->getModeA());
            }

            ui->MainFreqDial->setText(convertFreqStrDisp(QString::number(radioStatus->getFreqA())));

        }
    }

}


void RadioCatEmulatorMainWindow::changeMainRadioFreq()
{


    QString newFreqStr = ui->MainFreqDial->text();
    QString newFreq = newFreqStr.trimmed().remove('.');


    double f = convertStrToFreq(newFreq);


    if (f >= 0.0)
    {
        if (f > 0)
        {
            newFreq.remove( QRegularExpression("^[0]*")); //remove periods and leading zeros
        }

        if (newFreq != QString::number(radioStatus->getFreqA()))
        {
            radioStatus->setFreqA(newFreq.toULong());
            if (checkValidFreq(radioStatus->getFreqA()))
            {


            }

        }
    }
}

bool RadioCatEmulatorMainWindow::checkValidFreq(QString freq)
{

    bool bandOK = false;
    bool ok = false;
    QString band;
    QString sfreq = freq.trimmed().remove('.');

    quint32 freqUInt32 = sfreq.toULong(&ok);

    if (ok)
    {
        bandOK = bandlist->findBand(band, freqUInt32);
    }
    return bandOK;
}

bool RadioCatEmulatorMainWindow::checkValidFreq(quint32 freq)
{
    QString band;
    return bandlist->findBand(band, freq);

}

void RadioCatEmulatorMainWindow::onNewDialFreq()
{
    if (radio)
    {
        bool ok;
        quint32 f = ui->MainFreqDial->text().remove('.').toULong(&ok);
        if (ok)
        {
            if(radioStatus->getVfo())
            {
                if (f != radioStatus->getFreqA())
                {

                    radioStatus->setFreqA(f);
                }
            }
            else
            {
                if (f != radioStatus->getFreqB())
                {
                    radioStatus->setFreqB(f);
                }
            }
        }


    }
}

void RadioCatEmulatorMainWindow::onRadioBandFreq(int idx)
{
    Q_UNUSED(idx)
    if (radio != nullptr)
    {
        if (radioStatus->getVfo())
        {
            if (ui->bandCombo->currentText() != radioStatus->getCurrentBandA())
            {
                radioStatus->setCurrentBandA(ui->bandCombo->currentText());
                int idx = supportedBandList.indexOf(radioStatus->getCurrentBandA());
                //radioStatus->setCurrentFreqA(supportedBandStartFreq[idx]);
                radioStatus->setFreqA(supportedBandStartFreq[idx].toULong());
                ui->MainFreqDial->setText(convertFreqStrDisp(supportedBandStartFreq[idx]));

            }
        }
        else
        {
            if (ui->bandCombo->currentText() != radioStatus->getCurrentBandB())
            {
                radioStatus->setCurrentBandB(ui->bandCombo->currentText());
                int idx = supportedBandList.indexOf(radioStatus->getCurrentBandB());
                //radioStatus->setCurrentFreqB(supportedBandStartFreq[idx]);
                radioStatus->setFreqB(supportedBandStartFreq[idx].toULong());
                ui->MainFreqDial->setText(convertFreqStrDisp(supportedBandStartFreq[idx]));

            }
        }

    }



}

void RadioCatEmulatorMainWindow::onRadioModeChanged(int idx)
{
    Q_UNUSED(idx)
    if (ui->modeCombo->currentText() == "")
    {
        return;
    }

    if(radioStatus->getVfo())
    {
        if (ui->modeCombo->currentText() != radioStatus->getModeA())
        {

            radioStatus->setModeA(ui->modeCombo->currentText());
            setModeLabel(radioStatus->getModeA());

        }
    }
    else
    {
        if (ui->modeCombo->currentText() != radioStatus->getModeB())
        {
            radioStatus->setModeB(ui->modeCombo->currentText());
            setModeLabel(radioStatus->getModeB());

        }
    }

}

void RadioCatEmulatorMainWindow::onComportChanged(int idx)
{
    Q_UNUSED(idx)

    configSerial->closeComport();

    if (ui->comportCombo->currentText() != comport)
    {
        comport = ui->comportCombo->currentText();

    }

    openComport(configSerial, "Cat", comport, baudRateIndex, databits, stopbits);


}


bool RadioCatEmulatorMainWindow::validateComParams()
{
    if (ui->comportCombo->count() == 0 && ui->baudrateCombo->count() == 0
            && ui->stopbitsCombo->count() == 0 && ui->databitsCombo->count() == 0
            && ui->comportCombo->currentIndex() == 0 && ui->baudrateCombo->currentIndex() == 0
            && ui->stopbitsCombo->currentIndex() == 0 && ui->databitsCombo->currentIndex() == 0)
    {
        return false;
    }

    return true;

}

void RadioCatEmulatorMainWindow::onDataBitsChanged(int idx)
{
    Q_UNUSED(idx)
    configSerial->closeComport();
    if (ui->databitsCombo->currentText() != databits)
    {
        databits = ui->databitsCombo->currentText();
    }
}

void RadioCatEmulatorMainWindow::onStopBitsChanged(int idx)
{
    Q_UNUSED(idx)
    configSerial->closeComport();
    if (ui->stopbitsCombo->currentText() != stopbits)
    {
        stopbits = ui->stopbitsCombo->currentText();
    }
}

void RadioCatEmulatorMainWindow::openComport(ConfigRotSerial *serialObj, const QString name, const QString comport, const int baudRateIndex, const QString databits, const QString stopbits)
{
    if (validateComParams())
    {
        if (serialObj->openComport(comport, baudRateIndex - 1, static_cast<QSerialPort::DataBits>(databits.toInt()) , static_cast<QSerialPort::StopBits>(stopbits.toInt())))
        {
            sendToTextWindow(QString("[Comport] "), QString("%1 Serial port %2 open OK").arg(name, comport));
            trace(QString("%1 Serial port %2 open ok").arg(name, comport));

        }
        else
        {
            sendToTextWindow(QString("[Comport] "), QString("%1 Serial port %2 failed to open").arg(name, comport));
            trace(QString("%1 Serial port %2 failed to open").arg(name, comport));

        }
    }
    else
    {
        trace(QString("Serial port params not configured"));

    }
}

void RadioCatEmulatorMainWindow::closeComport(ConfigRotSerial *serialObj, const QString name, const QString comport)
{
    if (!comport.isEmpty())
    {
        serialObj->closeComport();
        sendToTextWindow(QString("[Comport] "), QString("S%1 Serial port %2 closed OK").arg(name, comport));
    }



}


void RadioCatEmulatorMainWindow::onTxError()
{
    sendToTextWindow(QString("[TX Error]"), QString("Comport not open?"));
}

void RadioCatEmulatorMainWindow::sendToTextWindow(QString startTxt, QString msg)
{
    QString windowMsg = startTxt + " " + msg;
    ui->serialTextWindow->appendPlainText(windowMsg.remove('\r').remove('\n').toLatin1());
}

void RadioCatEmulatorMainWindow::onbaudRateChanged(int idx)
{

        configSerial->closeComport();
        if(idx  != baudRateIndex)
        {
            baudRateIndex = idx;
            baudRate = ui->baudrateCombo->currentText();
        }

        openComport(configSerial, "Cat", comport, baudRateIndex, databits, stopbits);


}

void RadioCatEmulatorMainWindow::onRadioModelChanged(int idx)
{
    Q_UNUSED(idx)
    if (ui->radioModelCombo->currentText() != selectedRadio)
    {
        selectedRadio = ui->radioModelCombo->currentText();
        if (selectedRadio.isEmpty() && radio != nullptr)
        {
            closeRadio();
        }
        else if (!selectedRadio.isEmpty())
        {

             openRadio(selectedRadio);


        }

    }
}

void RadioCatEmulatorMainWindow::onRadioStatusUpdated()
{   QString band;
    bool bandOk = false;

    setVfoLabel();

    if (radioStatus->getVfo())
    {

        int idx = ui->modeCombo->findText(radioStatus->getModeA());
        if (idx >= 0)
        {
            ui->modeCombo->setCurrentIndex(idx);

        }

        setModeLabel(radioStatus->getModeA());
        ui->MainFreqDial->setText(convertFreqStrDisp(QString::number(radioStatus->getFreqA())));
        bandOk = bandlist->findBand(band, radioStatus->getFreqA());
        if (bandOk)
        {
            ui->bandCombo->setCurrentText(band);
        }
    }
    else
    {
        int idx = ui->modeCombo->findText(radioStatus->getModeB());
        if (idx >= 0)
        {
            ui->modeCombo->setCurrentIndex(idx);

        }
        setModeLabel(radioStatus->getModeB());
        ui->MainFreqDial->setText(convertFreqStrDisp(QString::number(radioStatus->getFreqB())));
        bandOk = bandlist->findBand(band, radioStatus->getFreqB());
        if (bandOk)
        {
            ui->bandCombo->setCurrentText(band);
        }

    }

    setRitLabel();
    ui->ritDial->setText(convertRitFreqToStr(radioStatus->getRitFreq(), ritKHzFlag));

    ui->volumeSlider->setValue(radioStatus->getVolumeLevel(radioStatus->getRxNum()));

    setVoiceMemLabel(radioStatus->getVoiceMemNum());

    setCwMemLabel(radioStatus->getCwMemNum());

    if (radioStatus->getTxState())
    {
        setTXStateLabel(true);
    }
    else
    {
        setTXStateLabel(false);
    }

    qDebug() << "morse text = " << radioStatus->getMorseText();
    ui->sendMorseLineEdit->setText(radioStatus->getMorseText());

    setCatCtsRadioButton(radioStatus->getCatCtsFlag());

}


void RadioCatEmulatorMainWindow::onNewRitFreq(int f)
{
    Q_UNUSED(f)
    if (radio)
    {
        qDebug() << "ritfreq = " << QString::number(f);
        if (f != radioStatus->getRitFreq())
        {

            radioStatus->setRitFreq(f);

        }
    }
}

void RadioCatEmulatorMainWindow::onVolumeChanged(int value)
{
    if (radioStatus->getVolumeLevel(radioStatus->getRxNum()) != value)
    {
        radioStatus->setVolumeLevel(value, radioStatus->getRxNum());
    }
}

void RadioCatEmulatorMainWindow::openRadio(QString selectedRadio)
{
    if (radio)
    {
        closeRadio();
    }

    sendToTextWindow(QString("[Radio] "), QString("Open Radio %1 OK").arg(selectedRadio));

    configSerial->fillPortsInfo(ui->comportCombo);
    configSerial->fillPortsInfo(ui->pttComportCombo);


    RigCapabilities rigCap = rigFactory->supported_rigs()->value(selectedRadio);

    rigModelNumber = rigCap.rigModelNumber;
    rigModelName = rigCap.rigModelName;
    rigManufacturer = rigCap.rigManufacturer;
    QStringList rigDefaultSettings = rigCap.defaultSettings.split(':');
    QString defBandA;
    QString defBandB;
    QString defFreqA;
    QString defFreqB;
    QString defModeA;
    QString defModeB;

    if (rigDefaultSettings.size() == 6)
    {
        defBandA = rigDefaultSettings[0];
        defFreqA = rigDefaultSettings[1];
        defBandB = rigDefaultSettings[2];
        defFreqB = rigDefaultSettings[3];
        defModeA = rigDefaultSettings[4];
        defModeB = rigDefaultSettings[5];

    }


    radio = rigFactory->createRigs(rigModelNumber, rigModelName, radioStatus, configSerial);


    if (radio != nullptr)
    {



        connect (radio, SIGNAL(catRxMsg(QByteArray)), this, SLOT(onCatRxMsg(QByteArray)), Qt::QueuedConnection);

        connect (radio, SIGNAL(catTxMsg(QString)), this, SLOT(onCatTxMsg(QString)), Qt::QueuedConnection);

        connect(configSerial, SIGNAL(dataReceived()), this, SLOT(onSerialDataReceived()));

        if (rigCap.catCaptureMethod == CATCAPTURE::KENWOOD)
        {
            connect(configSerial, SIGNAL(dataReceived()), radio, SLOT(KenwoodWaitForCatMessage()), Qt::QueuedConnection);

        }
        else if (rigCap.catCaptureMethod == CATCAPTURE::YAESU)
        {
            connect(configSerial, SIGNAL(dataReceived()), radio, SLOT(oldYaesuWaitForCatMessage()), Qt::QueuedConnection);

        }
        else if (rigCap.catCaptureMethod == CATCAPTURE::ICOM)
        {
            connect(configSerial, SIGNAL(dataReceived()), radio, SLOT(IcomWaitForCatMessage()), Qt::QueuedConnection);

        }
        else
        {
            //error couldn't find capture method code
            trace(QString("Couldn't find capture method code when opening radio"));
        }


        connect (radio, SIGNAL(radioStatusUpdated()), this, SLOT(onRadioStatusUpdated()), Qt::QueuedConnection);
        bandlist = new Bandlist();

        QStringList supportedBands = rigCap.supportedBands.split(',');
        for (int i = 0; i <supportedBands.size(); i++)
        {
            QStringList sl = supportedBands[i].split(':');
            if (sl.size() == 3)
            {
                BandDetials bd;
                bd.setBand(sl[0]);
                supportedBandList.append(sl[0]);
                bd.setLowFreqStr(sl[1]);
                bd.setLowFreq(sl[1].toULong());
                bd.setHighFreqStr(sl[2]);
                bd.setHighFreq(sl[2].toULong());
                bandlist->setBandDetail(sl[0], bd);
            }
        }

        ui->MainFreqDial->setBandList(bandlist);
        supportedBandStartFreq = rigCap.supportedBandStartFreq.split(',');
        supportedModes = rigCap.supportedModes.split(',');
        supportedModeCodes = rigCap.supportedModeCodes.split(',');



        supportedBandList.removeAll(" ");
        populateCombo(ui->bandCombo, supportedBandList, true);


        supportedModes.removeAll(" ");
        populateCombo(ui->modeCombo, supportedModes, true);

        ui->volumeSlider->setRange(rigCap.volLevelMin, rigCap.volLevelMax);



        radio->initRadio(bandlist, &supportedModes, &supportedModeCodes, rigCap.terminator, rigCap.civAddress, rigCap.kenwoodYaesuId);

        if (rigCap.rigManufacturer == "Icom")
        {
            ui->civTitleLabel->setVisible(true);
            ui->civAddressLabel->setVisible(true);
            QByteArray civ;
            civ +=rigCap.civAddress.toLatin1();
            ui->civAddressLabel->setText(civ.toHex());
        }
        else
        {
            ui->civTitleLabel->setVisible(false);
            ui->civAddressLabel->setVisible(false);
        }

        ui->ritDial->setMaxMinRitFreq(rigCap.maxRitFreq);
        maxRitFreq = rigCap.maxRitFreq;
        if (maxRitFreq > MAX_RITFREQ)
        {
            ritKHzFlag = true;
        }
        else
        {
            ritKHzFlag = false;
        }

        // recall settings for this radio
        RadioStatus rs;

        readRadioSettings(rigModelName, rs);

        if (rs.getFreqA() == 0)
        {
            // set default band and freq
            radioStatus->setCurrentBandA(defBandA);
            radioStatus->setFreqA(defFreqA.toULong());
        }
        else
        {
            radioStatus->setFreqA(rs.getFreqA());
        }

        if (rs.getFreqB() == 0)
        {
            // set default band and freq
            radioStatus->setCurrentBandB(defBandB);
            radioStatus->setFreqB(defFreqB.toULong());
        }
        else
        {
            radioStatus->setFreqB(rs.getFreqB());
        }

        if (rs.getModeA() == "")
        {
            // set Default mode
            radioStatus->setModeA(defModeA);
        }
        else
        {

            radioStatus->setModeA(rs.getModeA());
        }

        if (rs.getModeB() == "")
        {
            // set Default mode
            radioStatus->setModeB(defModeB);
        }
        else
        {
            radioStatus->setModeB(rs.getModeB());
        }

        radioStatus->setRxNum(rs.getRxNum());

        radioStatus->setVfo(rs.getVfo());

        radioStatus->setRitState(rs.getRitState());
        radioStatus->setRitFreq(rs.getRitFreq());

        radioStatus->setCatCtsFlag(rs.getCatCtsFlag());

        radioStatus->setVoiceMemNum('0');
        radioStatus->setTxState(false);

        radioStatus->setCwMemNum('0');


        // now set the display

        onRadioStatusUpdated();

        // now set comports
        QString comport_;
        QString pttComport_;
        QString baudrate_;
        QString databits_;
        QString stopbits_;
        readCommsSetting(rigModelName, comport_, pttComport_, baudrate_, databits_, stopbits_);

        if (validateComParams())
        {

            bool comportOk = false;
            bool baudrateOk = false;
            bool databitsOk = false;
            bool stopbitsOk = false;

            int idx = ui->comportCombo->findText(comport_);
            if (idx != -1)
            {
                comport = comport_;
                comportOk = true;
                ui->comportCombo->setCurrentIndex(idx);
                trace(QString("Open Radio: setting comport %1 for radio %2").arg(ui->comportCombo->currentText()).arg(rigModelName));
            }
            else
            {
                //error
                QMessageBox::warning(this, "Open Radio", QString("Data Comport = %1 missing").arg(comport_), QMessageBox::Ok);
                trace(QString("Open Radio: Data Comport %1 missing").arg(comport_));
            }

            idx =ui->baudrateCombo->findText(baudrate_);
            if (idx != -1)
            {
                baudRateIndex = idx;
                ui->baudrateCombo->setCurrentIndex(idx);
                baudRate = baudrate_;
                baudrateOk = true;
                trace(QString("Open Radio: setting baudrate %1 for radio %2").arg(ui->baudrateCombo->currentText()).arg(rigModelName));

            }
            else
            {
                // error
                QMessageBox::warning(this, "Open Radio", QString("Baudrate %1 missing").arg(baudrate_), QMessageBox::Ok);
                trace(QString("Open Radio: baudrate is missing %1 for radio %2").arg(ui->baudrateCombo->currentText()).arg(rigModelName));
            }

            idx =ui->databitsCombo->findText(databits_);
            if (idx != -1)
            {

                ui->databitsCombo->setCurrentIndex(idx);
                databits = databits_;
                databitsOk = true;
                trace(QString("Open Radio: setting databits %1 for radio %2").arg(ui->databitsCombo->currentText()).arg(rigModelName));

            }
            else
            {
                // error
                QMessageBox::warning(this, "Open Radio", QString("Databits %1 missing").arg(databits_), QMessageBox::Ok);
                trace(QString("Open Radio: databits %1 missing for radio %2").arg(ui->databitsCombo->currentText()).arg(rigModelName));
            }

            idx =ui->stopbitsCombo->findText(stopbits_);
            if (idx != -1)
            {

                ui->stopbitsCombo->setCurrentIndex(idx);
                stopbits = stopbits_;
                stopbitsOk = true;
                trace(QString("Open Radio: setting stopbits %1 for radio %2").arg(ui->stopbitsCombo->currentText()).arg(rigModelName));

            }
            else
            {
                // error
                QMessageBox::warning(this, "Open Radio", QString("Stopbits %1 missing").arg(stopbits_), QMessageBox::Ok);
                trace(QString("Open Radio: stopbits %1 missing for radio %2").arg(ui->stopbitsCombo->currentText()).arg(rigModelName));
            }

            openComport(configSerial, "Cat", comport, baudRateIndex, databits, stopbits);

            if (!radioStatus->getCatCtsFlag() /*&& !pttComport_.isEmpty()*/)
            {

                int idx = ui->pttComportCombo->findText(pttComport_);
                if (idx != -1)
                {
                    ui->pttComportCombo->setCurrentIndex(idx);
                    if (comport_ == pttComport_)
                    {
                        //ports are the same, point to same comport objects
                        pttComport = pttComport_;
                        pttSerialPort = configSerial;
                        sendToTextWindow(QString("[PTT Comport] "), QString("%1 PTT Serial port is the same as the radio comport").arg(pttComport));

                    }
                    else
                    {
                        pttComport = pttComport_;
                        openComport(pttSerialPort, "Ptt", pttComport, baudRateIndex, databits, stopbits ); // don't care about datarate etc here

                    }

                    connect(pttSerialPort, SIGNAL(ctsOn(bool)), this, SLOT(onCtsOn(bool)));
                    pttSerialPort->startPttTimer(250);
                    sendToTextWindow(QString("[PTT Comport] "), QString("Started CTS Timer"));

                }
            }

        }




    }
}


void RadioCatEmulatorMainWindow::closeEvent(QCloseEvent *event)
{

    closeRadio();

    // and tidy up all loose ends

    //QSettings settings;
    //settings.setValue(geoStr, saveGeometry());

    QWidget::closeEvent(event);

}

void RadioCatEmulatorMainWindow::closeRadio()
{
    sendToTextWindow(QString("[Radio] "), QString("Radio %1 closed OK").arg(rigModelName));
    if (radio != nullptr)
    {
        disconnect (radio, SIGNAL(catRxMsg(QByteArray)), this, SLOT(onCatRxMsg(QByteArray)));
        disconnect (radio, SIGNAL(catTxMsg(QString)), this, SLOT(onCatTxMsg(QString)));
        disconnect (radio, SIGNAL(radioStatusUpdated()), this, SLOT(onRadioStatusUpdated()));

        RigCapabilities rigCap = rigFactory->supported_rigs()->value(selectedRadio);

        if (rigCap.catCaptureMethod == CATCAPTURE::KENWOOD)
        {
            disconnect(configSerial, SIGNAL(dataReceived()), radio, SLOT(KenwoodWaitForCatMessage()));

        }
        else if (rigCap.catCaptureMethod == CATCAPTURE::YAESU)
        {
            disconnect(configSerial, SIGNAL(dataReceived()), radio, SLOT(oldYaesuWaitForCatMessage()));

        }
        else if (rigCap.catCaptureMethod == CATCAPTURE::ICOM)
        {
            disconnect(configSerial, SIGNAL(dataReceived()), radio, SLOT(IcomWaitForCatMessage()));

        }
        else
        {
            //error couldn't find capture method code
            trace(QString("Couldn't find capture method code when closing radio"));
        }


        saveRadioStatus(rigModelName);
        //QString comport_;
        //QString baudRate_;
        //QString databits_;
        //QString stopbits_;
        //readCommsSetting(rigModelName, comport_, baudRate_, databits_, stopbits_);
        //if (comport_.isEmpty() && baudRate_.isEmpty() && !comport.isEmpty() && !baudRate.isEmpty())
        //{
        saveCommsSetting(rigModelName);
        //}

        closeComport(configSerial, "Cat", comport);
        if (!radioStatus->getCatCtsFlag())
        {
            if (pttComport != comport)
            {
                closeComport(pttSerialPort, "Ptt", pttComport);
            }

        }
        bandlist->clear();
        supportedBandList.clear();
        ui->bandCombo->clear();
        //ui->pttComportCombo->clear();
        ui->modeCombo->clear();
        rigManufacturer.clear();
        rigModelName.clear();
        rigModelNumber = 0;
        supportedModes.clear();
        supportedModeCodes.clear();
        supportedBandStartFreq.clear();
        radioStatus->clear();
        ui->MainFreqDial->clear();
        ui->ritDial->clear();
        delete radio;
        radio = nullptr;
        delete bandlist;
        bandlist = nullptr;

    }
}

void RadioCatEmulatorMainWindow::setRitLabel()
{
    if (radioStatus->getRitState())
    {
        ui->ritLabel->setText("RIT - ON ");
    }
    else
    {
        ui->ritLabel->setText("RIT - OFF");
    }
}


void RadioCatEmulatorMainWindow::setVfoLabel()
{
    if (radioStatus->getVfo())
    {
        ui->vfoLabel->setText("VFO - A");
    }
    else
    {
        ui->vfoLabel->setText("VFO - B");
    }
}

void RadioCatEmulatorMainWindow::setModeLabel(QString m)
{
    ui->modeLabel->setText(m);
}
void RadioCatEmulatorMainWindow::populateCombo(QComboBox* comboBox, QStringList& textList, bool firstListBlank)
{
    comboBox->clear();
    if (firstListBlank)
    {
        comboBox->addItem(" ");
    }

    foreach(QString str, textList)
    {
        comboBox->addItem(str);
    }
}

bool RadioCatEmulatorMainWindow::isFirstItemBlank(QComboBox* comboBox)
{
    if (comboBox->itemText(0) == " ")
    {
        return true;
    }
    else
    {
        return false;
    }
}


void RadioCatEmulatorMainWindow::exitFreqEdit()
{
    //traceMsg(QString("Exit Edit Freq"));
    freqEditOn = false;

    freqLineEditFrameColour(false);
    QString freq = ui->MainFreqDial->text();
    if (radioStatus->getFreqA())
    {
        if (freq.remove('.') != QString::number(radioStatus->getFreqA()))
        {
            // up date display to current radio freq
            ui->MainFreqDial->setInputMask(maskData::freqMask[QString::number(radioStatus->getFreqA()).size() - 4]);
            ui->MainFreqDial->setText(QString::number(radioStatus->getFreqA()));
            //emit setFreqDisplay(curFreq, legalFreq);
        }


        ui->MainFreqDial->clearFocus();
        //setFreqTextLegalColour(curFreq, curMode);

    }

}


void RadioCatEmulatorMainWindow::freqEditSelected()
{
    //traceMsg(QString("Freq Edit Selected"));
    ui->MainFreqDial->setFocus();

    int len = ui->MainFreqDial->text().length();
    if (len > 5)
    {
       ui->MainFreqDial->setCursorPosition(len - 5);
    }

}

void RadioCatEmulatorMainWindow::freqLineEditInFocus()
{
    //traceMsg(QString("Freq LineEdit in Focus"));
    freqEditOn = true;
    ui->MainFreqDial->setReadOnly(false);
    freqLineEditFrameColour(true);
    //setFreqTextLegalColour(curFreq, curMode);
}

bool RadioCatEmulatorMainWindow::eventFilter(QObject *obj, QEvent *event)
{
   if (obj == ui->MainFreqDial)
   {
       if (event->type() == QEvent::FocusIn)
          freqLineEditInFocus();
       else if (event->type() == QEvent::FocusOut)
          exitFreqEdit();
   }
   else if (obj == ui->ritDial)
   {
       if (event->type() == QEvent::FocusIn)
          ritLineEditInFocus();
       else if (event->type() == QEvent::FocusOut)
          exitRitFreqEdit();
   }


   return false;
}

void RadioCatEmulatorMainWindow::freqLineEditFrameColour(bool status)
{
    int curPos = ui->MainFreqDial->cursorPosition();
    if (status)
    {
        ui->MainFreqDial->setStyleSheet("border: 1px solid magenta");
        // restore cursor selection
        ui->MainFreqDial->setSelection(curPos, 1);
    }
    else
    {
        ui->MainFreqDial->setStyleSheet("border: 1px solid black");

    }

    //QString freq = ui->freqInput->text();
    //setFreqTextLegalColour(freq, curMode);

}


void RadioCatEmulatorMainWindow::ritLineEditInFocus()
{
    //traceMsg(QString("Rit LineEdit in Focus"));
    if (radioStatus->getRitState())
    {
        ui->ritDial->setReadOnly(false);
        ui->ritDial ->setRitOnFlag(true);        // prevent updates from rigcontrol
        ritFreqLineEditFrameColour(true);
    }
}


void RadioCatEmulatorMainWindow::exitRitFreqEdit()
{
    //traceMsg(QString("Exit Rit Edit Freq"));
    ui->ritDial->setRitOnFlag(false);        // set flag to allow prevent editing of RIT freq
    //setFreq(curFreq);
    ritFreqLineEditFrameColour(false);
    ui->ritDial->clearFocus();
}

void RadioCatEmulatorMainWindow::ritFreqLineEditFrameColour(bool status)
{
    int curPos = ui->ritDial->cursorPosition();
    if (status)
    {
        ui->ritDial->setStyleSheet("border: 1px solid red");
        // restore cursor selection
        ui->ritDial->setSelection(curPos, 1);
    }
    else
    {
        ui->ritDial->setStyleSheet("border: 1px solid black");

    }

}


void RadioCatEmulatorMainWindow::onSerialDataReceived()
{
    static int numChars = 0;

    if (rigManufacturer == "Icom")
    {
       ui->rxWindow->appendPlainText(configSerial->getBuffer().toHex(':'));
    }
    else
    {
       ui->rxWindow->appendPlainText(configSerial->getBuffer());
    }


    numChars++;
    if (numChars > 40)
    {
        ui->rxWindow->setPlainText(QChar('\n'));
        numChars = 0;
    }


}

void RadioCatEmulatorMainWindow::onCatRxMsg(QByteArray msg)
{

    RigCapabilities rigCap = rigFactory->supported_rigs()->value(selectedRadio);
    if (rigCap.catCaptureMethod == CATCAPTURE::ICOM || rigCap.catCaptureMethod == CATCAPTURE::YAESU)
    {
        sendToTextWindow(QString("[RXMSG]"), QString(msg.toHex(':')));
    }
    else
    {
        sendToTextWindow(QString("[RXMSG]"), QString(msg));
    }



    if (radio)
    {
        radio->handleCatMessage(msg);
    }

}


void RadioCatEmulatorMainWindow::onCatTxMsg(QString msg)
{
    sendToTextWindow(QString("[TXMSG]"), msg);
}


void RadioCatEmulatorMainWindow::onSaveRadioButtonClicked()
{
    saveRadioStatus(rigModelName);
}


void RadioCatEmulatorMainWindow::saveRadioStatus(QString& rigModelName)
{

    if (rigModelName.isEmpty())
    {
        return;
    }

    QSettings settings("./Configuration/radioSettings.ini", QSettings::IniFormat);
    settings.beginGroup(rigModelName);

    settings.setValue("freqA", radioStatus->getFreqA());
    settings.setValue("freqB", radioStatus->getFreqB());

    settings.setValue("currentBandA", radioStatus->getCurrentBandA());
    settings.setValue("currentBandB", radioStatus->getCurrentBandB());

    settings.setValue("modeA", radioStatus->getModeA());
    settings.setValue("modeB", radioStatus->getModeB());

    settings.setValue("mainSubRx", radioStatus->getRxNum());

    settings.setValue("vfo", radioStatus->getVfo());

    settings.setValue("ritState", radioStatus->getRitState());
    settings.setValue("ritFreq", radioStatus->getRitFreq());

    settings.setValue("cat_cts_tx_flag", radioStatus->getCatCtsFlag());


    settings.endGroup();

}


void RadioCatEmulatorMainWindow::readRadioSettings(QString& rigModelName, RadioStatus& radioStatus)
{
    QSettings settings("./Configuration/radioSettings.ini", QSettings::IniFormat);
    settings.beginGroup(rigModelName);

    radioStatus.setFreqA(settings.value("freqA", 0).toString().toLongLong());
    radioStatus.setFreqB(settings.value("freqB", 0).toString().toLongLong());

    radioStatus.setCurrentBandA(settings.value("currentBandA", "").toString());
    radioStatus.setCurrentBandB(settings.value("currentBandB", "").toString());

    radioStatus.setModeA(settings.value("modeA", "").toString());
    radioStatus.setModeB(settings.value("modeB", "").toString());

    radioStatus.setRxNum(settings.value("mainSubRx", 0).toInt());

    radioStatus.setVfo(settings.value("vfo", true).toBool());

    radioStatus.setRitState(settings.value("ritState", false).toBool());
    radioStatus.setRitFreq(settings.value("ritFreq", 0).toInt());

    radioStatus.setCatCtsFlag(settings.value("cat_cts_tx_flag", true).toBool());  //true = cat, false = cts

    settings.endGroup();

}


void RadioCatEmulatorMainWindow::saveCommsSetting(QString& rigModelName)
{
    QSettings settings("./Configuration/radioSettings.ini", QSettings::IniFormat);
    settings.beginGroup(rigModelName);

    settings.setValue("comport", ui->comportCombo->currentText());
    settings.setValue("pttComport", ui->pttComportCombo->currentText());
    settings.setValue("baudrate", ui->baudrateCombo->currentText());
    settings.setValue("databits", ui->databitsCombo->currentText());
    settings.setValue("stopbits", ui->stopbitsCombo->currentText());
    settings.endGroup();


}


void RadioCatEmulatorMainWindow::readCommsSetting(QString &rigModelName, QString &comport, QString &pttComport, QString &baudrate, QString &databits, QString &stopbits)
{
    QSettings settings("./Configuration/radioSettings.ini", QSettings::IniFormat);
    settings.beginGroup(rigModelName);

    comport = settings.value("comport", "").toString();
    pttComport = settings.value("pttComport", "").toString();
    baudrate = settings.value("baudrate", "").toString();
    databits = settings.value("databits", "").toString();
    stopbits = settings.value("stopbits", "").toString();

    settings.endGroup();


}


void RadioCatEmulatorMainWindow::setVoiceMemLabel(QChar voiceMemNum)
{

    if (voiceMemNum >= '0' && voiceMemNum <= '8')
    {
        ui->voiceMemDisplay->setText(voiceMemNum);
    }
    else
    {
        QList<QChar> memNums = {'0' , '1',  '2', '3', '4', '5','6', '7', '8'};
        int i = static_cast<int>(voiceMemNum.toLatin1());

        if (i >= 0 && i < memNums.size())
        {
           ui->voiceMemDisplay->setText(memNums[i]);
        }
    }


}

void RadioCatEmulatorMainWindow::setCwMemLabel(QChar cwMemNum)
{
    if (cwMemNum >= '0' && cwMemNum <= '8')
    {
        ui->sendMorseMemNumLabel->setText(cwMemNum);
    }
    else
    {
        QList<QChar> memNums = {'0' , '1',  '2', '3', '4', '5','6', '7', '8'};
        int i = static_cast<int>(cwMemNum.toLatin1());

        if (i >= 0 && i < memNums.size())
        {
           ui->sendMorseMemNumLabel->setText(memNums[i]);
        }
    }
}





void RadioCatEmulatorMainWindow::onTxPushButtonClicked()
{
    if (radioStatus->getTxState())
    {
        radioStatus->setTxState(false);
        setTXStateLabel(false);
    }
    else
    {
        radioStatus->setTxState(true);
        setTXStateLabel(true);
    }
}



void RadioCatEmulatorMainWindow::setTXStateLabel(bool state)
{
    if (state)
    {
        ui->txPushButton->setText("TX: On");
        ui->txPushButton->setStyleSheet(TX_BUTTON_ON);
    }
    else
    {
        ui->txPushButton->setText("TX: Off");
        ui->txPushButton->setStyleSheet(TX_BUTTON_OFF);
    }
}

void RadioCatEmulatorMainWindow::onCatTxButtonClicked()
{
    radioStatus->setCatCtsFlag(!radioStatus->getCatCtsFlag());
    if (radioStatus->getCatCtsFlag())
    {
        ui->pttComportCombo->setEnabled(false);
    }
}

void RadioCatEmulatorMainWindow::onCtsButtonClicked()
{
    radioStatus->setCatCtsFlag(!radioStatus->getCatCtsFlag());
    if (!radioStatus->getCatCtsFlag())
    {
        ui->pttComportCombo->setEnabled(true);
    }
}

void RadioCatEmulatorMainWindow::onCtsPollTimerTimeout()
{

}

void RadioCatEmulatorMainWindow::setCatCtsRadioButton(bool catCtsTxFlag)
{
    if (catCtsTxFlag)
    {
        ui->catRadioBut->setChecked(catCtsTxFlag);
        pttSerialPort->stopPttTimer();
    }
    else
    {
       ui->ctsRadioBut->setChecked(!catCtsTxFlag);
       pttSerialPort->startPttTimer(250);
    }
}


void RadioCatEmulatorMainWindow::onCtsComportChanged(int idx)
{
    Q_UNUSED(idx)
    if (pttComport != comport)
    {
        pttSerialPort->closeComport(); // close if the pttcomport is not the same as comport
    }


    if (ui->pttComportCombo->currentText() != pttComport)
    {
        pttComport = ui->pttComportCombo->currentText();
    }

    if (!pttComport.isEmpty())
    {
        if (pttComport != comport)
        {
            openComport(pttSerialPort, "Ptt", pttComport, baudRateIndex, databits, stopbits);
        }
        else
        {
            //ports are the same, point to same comport objects

            pttSerialPort = configSerial;
            sendToTextWindow(QString("[PTT Comport] "), QString("%1 PTT Serial port is the same as the radio comport").arg(pttComport));

        }



    }

}


void RadioCatEmulatorMainWindow::onCtsOn(bool state)
{
    if (state)
    {
        radioStatus->setTxState(true);
        setTXStateLabel(true);
    }
    else
    {
        radioStatus->setTxState(false);
        setTXStateLabel(false);
    }

}
