#ifndef KENWOOD_H
#define KENWOOD_H

#include <QObject>
#include <QTimer>
#include "rigbase.h"
#include "rigcapabilities.h"
#include "rigfactory.h"
#include "radiostatus.h"
#include "configrotserial.h"


const QString TS590S = "Kenwood TS590S";
const QString TS890S = "Kenwood TS890S";
const QString TS2000 = "Kenwood TS2000";
const QString TS480 = "Kenwood TS480";
const QString QRPLABS = "QRPLABS OCX_QDX";
const QString THETIS = "Flexradio Thetis";

class Kenwood : public RigBase
{
    Q_OBJECT

public:
    explicit Kenwood(int id, QString modelName, RadioStatus *radioStatus_, ConfigRotSerial *configSerial_, QObject *parent = nullptr);
    virtual ~Kenwood() override;


    static void register_rigs(RigFactory::Rigs*);
    void initRadio( Bandlist* bandList_, QStringList *supportedModes_, QStringList *supportedModeCodes_, QChar terminator_, QChar civAddress_ = ' ', QString kenwoodYaesuId_ = "") override;


    void setMaxRitFreq(int f) override;
    void setMinRitFreq(int f) override;
signals:
    //void catRxMsg(QString);
    //void catTxMsg(QString);

private slots:
    void onVmsgTimeout();
    void onCwMsgTimeout();

private:

    RadioStatus* radioStatus;


    QString rxMessage;
    QString catTxMessage;
    QString modelName;
    int id;
    QChar terminator;

    Bandlist* bandList;
    //QList<int> supportedBandStartFreq;

    QString kenwoodYaesuId;

    QStringList *supportedModes;
    QStringList *supportedModeCodes;
    int maxRitFreq = +9999;
    int minRitFreq = -9999;

    QMap<QString, QString> modeStrToModeTable;

    QTimer* voiceMsgTimer;
    QTimer* cwMsgTimer;


    void handleCatMessage(QByteArray) override;

    void readCatFA_Response();
    void readCatFB_Response();
    void assembleMsgToSendToCat(QString &preamble, QString &msgBody, QString &terminator);
    void getFreqToSendToCat(QString &f, bool vfo);
    void getRitFreqToSendToCat(QString &f);
    void readCatAI_Response();
    void readCatDA_Response();
    void readCatMD_Response();
    void readCatRX_Response();
    void readCatSM_Response();
    void readCatAG_Response(int rxNum);
    void readCatPS_Response();
    void readCatID_Response();
    void readCatFV_Response();
    void readCatFW_Response();
    void readCatPB_Response();
    void readCatEX_Response();
    void readCatKS_Response();
    void readCatZZMD_Response();
    void setCatZZMD_Command();
    void readCatZZAG_Response();
    void setCatZZAG_Command();
    void readCatZZFL_Response();
    void setCatZZFL_Command();
    void readCatZZFH_Response();
    void setCatZZFH_Command();
    void readCatZZTX_Response();
    void setCatZZTX_Command();
    void readCatZZRM_Response();
    void setCatZZRM_Command();


    void readCatPB1_TS890S_Response();
    void readCatPB_TS590_Response();
    void setCatPB1_TS890S_Command(QString subCmd);
    void setCatPB_TS590_Command(QChar subCmd);
    void voiceMemoryStop(QChar subCommandNum);
    void voiceMemoryStart(QChar subCommandNum);

    void readCatIF_Response();
    void TS480_IF_Response();
    void TS590S_IF_Response();
    void TS2000_IF_Response();
    void THETIS_IF_Response();


    QString getStatusOfMode();
    void setFACommand();
    void setFBCommand();
    void setAGCommand(int rxNum);
    void setMDCommand();





    QString convertModetoQString(QString mode);

    QString convertQStringtoModeCode(QString m);



    void setRCCommand();
    void readCatRT_Response();
    void setRTCommand();
    void setRIT_Up();
    void setRIT_Down();
    void sendCatTxMsg();

    void readCatKY_Response();

    void catSetSendMorse(QByteArray msg);
    void catSetStopMorse();
    void stopMorse();
};

#endif // KENWOOD_H
