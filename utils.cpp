#include "utils.h"
#include <QByteArray>


// borrowed from hamlib library

/**
 * \brief Convert from binary to 4-bit BCD digits, little-endian
 * \param bcd_data
 * \param freq
 * \param bcd_len
 * \return bcd_data
 *
 * Convert a long long (e.g. frequency in Hz) to 4-bit BCD digits,
 * packed two digits per octet, in little-endian order
 * (e.g. byte order 90 78 56 34 12 for 1234567890 Hz).
 *
 * bcd_len is the number of BCD digits, usually 10 or 8 in 1-Hz units,
 * and 6 digits in 100-Hz units for Tx offset data.
 *
 * Hope the compiler will do a good job optimizing it (esp. w/the 64bit freq)
 *
 * Returns a pointer to (unsigned char *)bcd_data.
 *
 * \sa to_bcd_be()
 */
void to_bcd(QByteArray& bcd_data, quint32 freq, int bcd_len)
{
    int i;


    /* '450'/4-> 5,0;0,4 */
    /* '450'/3-> 5,0;x,4 */

    //char* bcdData = bcd_data.data();
    char bcdData[bcd_len];

    for (i = 0; i < bcd_len / 2; i++)
    {
        unsigned char a = freq % 10;
        freq /= 10;
        a |= (freq % 10) << 4;
        freq /= 10;
        bcdData[i] = a;
    }

    if (bcd_len & 1)
    {
        bcdData[i] &= 0xf0;
        bcdData[i] |= freq % 10; /* NB: high nibble is left uncleared */
    }

    bcd_data.clear();
    for (int x = 0; x < i; x++)
    {
        bcd_data += bcdData[x];
    }

}


/**
 * \brief Convert BCD digits, little-endian, to a long long (e.g. frequency in Hz)
 * \param bcd_data
 * \param bcd_len
 * \return binary result (e.g. frequency)
 *
 * Convert BCD digits, little-endian, (byte order 90 78 56 34 12
 * for 1234567890 Hz) to a long long (e.g. frequency in Hz)
 *
 * bcd_len is the number of BCD digits.
 *
 * Hope the compiler will do a good job optimizing it (esp. w/ the 64bit freq)
 *
 * Returns frequency in Hz an unsigned long long integer.
 *
 * \sa from_bcd_be()
 */
quint32 from_bcd(QByteArray& bcd_data, int bcd_len)
{
    int i;
    quint32 f = 0;


    if (bcd_len & 1)
    {
        f = bcd_data.at(bcd_len / 2) & 0x0f;
    }

    for (i = (bcd_len / 2) - 1; i >= 0; i--)
    {
        f *= 10;
        f += bcd_data.at(i) >> 4;
        f *= 10;
        f += bcd_data.at(i) & 0x0f;
    }

    return f;
}


/**
 * \brief Convert from binary to 4-bit BCD digits, big-endian
 * \param bcd_data
 * \param freq
 * \param bcd_len
 * \return bcd_data
 *
 * Same as to_bcd, but in big-endian order
 * (e.g. byte order 12 34 56 78 90 for 1234567890 Hz)
 *
 * \sa to_bcd()
 */
unsigned char *to_bcd_be(unsigned char bcd_data[],
                                    unsigned long long freq,
                                    unsigned bcd_len)
{
    int i;

    /* '450'/4 -> 0,4;5,0 */
    /* '450'/3 -> 4,5;0,x */


    if (bcd_len & 1)
    {
        bcd_data[bcd_len / 2] &= 0x0f;
        bcd_data[bcd_len / 2] |= (freq % 10) <<
                                 4; /* NB: low nibble is left uncleared */
        freq /= 10;
    }

    for (i = (bcd_len / 2) - 1; i >= 0; i--)
    {
        unsigned char a = freq % 10;
        freq /= 10;
        a |= (freq % 10) << 4;
        freq /= 10;
        bcd_data[i] = a;
    }

    return bcd_data;
}


/**
 * \brief Convert 4-bit BCD digits to binary, big-endian
 * \param bcd_data
 * \param bcd_len
 * \return binary result
 *
 * Same as from_bcd, but in big-endian order
 * (e.g. byte order 12 34 56 78 90 for 1234567890 Hz)
 *
 * \sa from_bcd()
 */
unsigned long long from_bcd_be(const unsigned char bcd_data[],
        unsigned bcd_len)
{
    int i;
    double f = 0;



    for (i = 0; i < bcd_len / 2; i++)
    {
        f *= 10;
        f += bcd_data[i] >> 4;
        f *= 10;
        f += bcd_data[i] & 0x0f;
    }

    if (bcd_len & 1)
    {
        f *= 10;
        f += bcd_data[bcd_len / 2] >> 4;
    }

    return f;
}


