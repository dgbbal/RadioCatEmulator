#include "yaesu.h"
#include "utils.h"
#include "MTrace.h"

#include <QDebug>
#include <QSettings>
#include <QTimer>

Yaesu::Yaesu(int id_, QString modelName_, RadioStatus *radioStatus_, ConfigRotSerial *configSerial_, QObject *parent)  : RigBase(configSerial_, parent),
             radioStatus(radioStatus_),
             modelName(modelName_),
             id(id_)
{

}

Yaesu::~Yaesu()
{
    txQueueTimer->stop();
    disconnect(txQueueTimer, SIGNAL(timeout()), this, SLOT(onTXQueueTimeout()));
    delete txQueueTimer;
}

void Yaesu::register_rigs(RigFactory::Rigs* rigsList)
{

    QSettings settings("./Radios/yaesu.ini", QSettings::IniFormat);

    QStringList availRadios = settings.childGroups();
    int numRadios = availRadios.count();

    for (int i = 0; i <numRadios; i++)
    {
        settings.beginGroup(availRadios[i]);
        QString rigManufacturer = settings.value("rigManufacturer", "").toString();
        QString rigName = settings.value("rigName", "").toString();
        QString rigModelName = rigManufacturer + " " + rigName;

        int rigModelNumber = settings.value("rigModelNumber", 0).toInt();

        QString kenwoodYaesuId = "";

        int volLevelMax = 0;
        int volLevelMin = 0;

        int maxRitFreq = settings.value("maxRit", 9990).toInt();

        QChar terminator = settings.value("terminator", ';').toChar();
        CATCAPTURE catCapMethodCode = RigCapabilities::getCatCaptureCode(settings.value("catCaptureMethod", "").toString().trimmed());
        //QByteArray civArray = settings.value("civ", ' ').toByteArray();
        //hex = civArray.toInt(&ok, 16);
        QChar civAddress = QChar('\x0');

        QString supportedBands = settings.value("supportedBands", "").toString();
        QString supportedBandStartFreq = settings.value("supportedBandStartFreq", "0").toString();
        QString supportedModes = settings.value("supportedModes", "").toString();
        QString supportedModeCodes = settings.value("supportedModeCodes", "-1").toString();
        QString defaultSettings = settings.value("defaultSettings", "").toString();
        settings.endGroup();

        (*rigsList)[rigModelName] = RigCapabilities(rigManufacturer,
                                                    rigName,
                                                    rigModelName,
                                                    rigModelNumber,
                                                    civAddress,
                                                    kenwoodYaesuId,
                                                    volLevelMax,
                                                    volLevelMin,
                                                    maxRitFreq,
                                                    terminator,
                                                    supportedBands,
                                                    supportedBandStartFreq,
                                                    supportedModes,
                                                    supportedModeCodes,
                                                    defaultSettings,
                                                    catCapMethodCode);

    }

}

void Yaesu::initRadio( Bandlist* bandList_, QStringList *supportedModes_, QStringList *supportedModeCodes_, QChar terminator_, QChar civAddress_, QString kenwoodYaesuId_ )
{

    Q_UNUSED(civAddress_)
    bandList = bandList_;
    supportedModes = supportedModes_;
    supportedModeCodes = supportedModeCodes_;
    terminator = terminator_.toLatin1();

    txQueueTimer = new QTimer(this);
    connect(txQueueTimer, SIGNAL(timeout()), this, SLOT(onTXQueueTimeout()));
    txQueueTimer->start(200);

    if ((*supportedModes).count() == (*supportedModeCodes).count())
    {
        for (int i = 0; i < (*supportedModes).count(); i++)
        {
            modeStrToModeTable.insert((*supportedModes)[i], (*supportedModeCodes)[i]);

        }
    }
    else
    {
        trace("Error - supportedModes length != supportedModeCodes length");
    }

}

void Yaesu::onTXQueueTimeout()
{

    if (!txQueue.isEmpty())
    {

        sendCatMsg(txQueue.first());
        txQueue.pop_front();
    }
}



void Yaesu::setMaxRitFreq(int f)
{
    maxRitFreq = f;
}

void Yaesu::setMinRitFreq(int f)
{
    minRitFreq = f;
}



void Yaesu::handleCatMessage(QByteArray msg)
{
    if (modelName == FT1000MP)
    {
        handleCatMessage_FT1000D(msg);
    }
    else if (modelName == FT857 )
    {
        handleCatMessage_FT857(msg);
    }

}


/************************* FT857 ***********************************************/

void Yaesu::handleCatMessage_FT857(QByteArray msg)
{
    rxMessage = msg;
    QChar cmd = rxMessage.at(4);
    //QChar subCmd = rxMessage.at(3);

    if (cmd == '\x01')
    {
        catSetFreq_FT857(msg);
    }
    else if (cmd == '\x03')
    {
        catReadFreqMode_FT857(cmd);
    }
    else if (cmd == '\x07')
    {
        catSetMode_FT857();
    }
    else if (cmd == '\x05' || cmd == '\x85')
    {
        catSetRit_FT857(cmd);
    }
    else if ( cmd == '\x08')
    {
        catSetTx_FT857(true);
    }
    else if (cmd == '\x88')
    {
        catSetTx_FT857(false);
    }

    else if (cmd == '\xbb')
    {
        if (rxMessage.at(0) == '\x00' && rxMessage.at(1) == '\x68')
        {
            catReadMemLoc68(cmd);
        }
        else if (rxMessage.at(0) == '\x00' && rxMessage.at(1) == '\x78')
        {
            catReadMemLoc78(cmd);
        }
    }
    else if (cmd == '\xe7')
    {
        catReadRxStatus_FT857(cmd);
    }
    else if (cmd == '\xf7')
    {
        catReadTxStatus_FT857(cmd);
    }
    else if (cmd == '\xf5')
    {
        catSetRitFreq_FT857(msg);
    }


}


void Yaesu::catSetFreq(QByteArray msg)
{
    QByteArray bcdFreq;
    bcdFreq += msg.at(0);
    bcdFreq += msg.at(1);
    bcdFreq += msg.at(2);
    bcdFreq += msg.at(3);
    quint32 freq = from_bcd(bcdFreq, 4);

    if (radioStatus->getVfo())
    {
        radioStatus->setFreqA(freq);
    }
    else
    {
        radioStatus->setFreqB(freq);

    }

    emit radioStatusUpdated();


}


void Yaesu::catSetFreq_FT857(QByteArray msg)
{
    QByteArray bcdFreq;
    bcdFreq += msg.at(3);
    bcdFreq += msg.at(2);
    bcdFreq += msg.at(1);
    bcdFreq += msg.at(0);
    quint32 freq = from_bcd(bcdFreq, 8) * 10;

    if (radioStatus->getVfo())
    {
        radioStatus->setFreqA(freq);
    }
    else
    {
        radioStatus->setFreqB(freq);

    }

    emit radioStatusUpdated();

    sendAck();


}


void Yaesu::catSetRitFreq_FT857(QByteArray msg)
{
     QByteArray bcdFreq;
     bcdFreq += msg.at(3);
     bcdFreq += msg.at(2);
     quint32 freq = from_bcd(bcdFreq, 4);
     int ritFreq = static_cast<int>(freq);
     if (msg.at(0) != 0)
     {
         ritFreq *= -1;
     }

     radioStatus->setRitFreq(ritFreq);

     emit radioStatusUpdated();

     sendAck();

}

void Yaesu::catSetRit_FT857(QChar cmd)
{
    if (cmd == '\05')
    {
        radioStatus->setRitState(true);
    }
    else if (cmd == '\x85')
    {
        radioStatus->setRitState(false);
    }

    emit radioStatusUpdated();

    sendAck();
}


void Yaesu::catReadRxStatus_FT857(QChar /*cmd*/)
{

    QByteArray data;
    data += '\x00';  // need to set status rather than 00??

    txQueue += data;


}

void Yaesu::catReadTxStatus_FT857(QChar cmd)
{
    Q_UNUSED(cmd)

    QByteArray data;
    QChar responseChar;
    if (radioStatus->getTxState())
    {
        responseChar = '\x00';
    }
    else
    {
        responseChar = '\x80';
    }


    data += responseChar.cell();
    txQueue += data;

}


void Yaesu::catSetTx_FT857(bool state)
{
    radioStatus->setTxState(state);
    emit radioStatusUpdated();
    sendAck();
}

void Yaesu::catReadMemLoc68(QChar cmd)
{
    Q_UNUSED(cmd)

    QByteArray data;
    /*if (radioStatus->getVfo())
    {
        //append has been deprecated replace with +=
        data.append('\x00').append('\x00');
    }
    else
    {
        data.append('\x01').append('\x00');
    }
    */

    data += '\x80';
    data += '\x83';

    txQueue += data;
}

void Yaesu::catReadMemLoc78(QChar cmd)
{
    Q_UNUSED(cmd)
    QByteArray data;

    data += '\x60';
    data += '\x00';

    txQueue += data;
}


void Yaesu::catReadFreqMode_FT857(QChar cmd)
{
    Q_UNUSED(cmd)

    QByteArray bcd_data;
    quint32 freq;
    QString modeStr;
    if (radioStatus->getVfo())
    {
        freq = radioStatus->getFreqA();
        modeStr = radioStatus->getModeA();
    }
    else
    {
        freq = radioStatus->getFreqB();
        modeStr = radioStatus->getModeB();
    }

    to_bcd(bcd_data, freq / 10, 8);

    QByteArray data;
    data += bcd_data.at(3);
    data += bcd_data.at(2);
    data += bcd_data.at(1);
    data += bcd_data.at(0);

    data.append(QString(convertQStringtoModeCode_FT897(modeStr)).toUtf8());

    txQueue += data;

}


/************************* FT1000 *************************************************/

void Yaesu::handleCatMessage_FT1000D(QByteArray msg)
{


    rxMessage = msg;
    QChar cmd = rxMessage.at(4);
    QChar subCmd = rxMessage.at(3);
    if (cmd == '\x05')
    {
        catSetVfo(subCmd);
    }
    else if (cmd == '\x09')
    {
       catSetRitFreq_FT1000();
    }

    else if (cmd == '\x0a')
    {
        catSetOperatingFreqVFOA();
    }
    else if (cmd == '\x10')
    {
        if (subCmd == '\x02')
        {
            catReadOperatingData();
        }
        else if (subCmd == '\x03')
        {
            catReadVfoFreqData();

        }
    }
    else if (cmd == '\x8a')
    {
        catSetOperatingFreqVFOB();
    }
    else if (cmd == '\xF7')
    {
        QChar subCmd = rxMessage.at(0);
        if (subCmd == '\x00' || subCmd == '\x01')
        {
            catReadSmeter(cmd, subCmd);
        }
    }
    else if (cmd == '\xfa')
    {
        // set number status bytes sent
        if (subCmd == '\x00')
        {
            catReadFiveStatusBytes();
        }
        else if (subCmd == '\x01')
        {
            catReadSixStatusBytes();
        }

    }
    else if (cmd == '\x0c')
    {
        catSetMode_FT1000(subCmd);
    }
    else
    {
        traceMsg(QString("Cmd = %1 not found").arg(cmd));
    }






}




void Yaesu::catReadOperatingData()
{

    QByteArray data;

    get16ByteOperatingData(data, radioStatus->getVfo());
    qDebug() << "read op data";

    txQueue += data;


}

void Yaesu::catReadVfoFreqData()
{

    qDebug() << "******";

    QByteArray data1;
    get16ByteOperatingData(data1, true); // VFOA

    QByteArray data2;
    get16ByteOperatingData(data2, false); //VFOB

    qDebug() << "read vfo freq data";

    QByteArray data;
    data += data1;
    data += data2;
    //sendCatMsg(data);
    txQueue += data;

}


void Yaesu::get16ByteOperatingData(QByteArray &data, bool vfo)
{
    quint32 freq;

    
    qDebug() << "Enter vfo =" << (vfo ? "VFOA" : "VFOB");

    // band selection
    
    data += '\x1b';    // band selection

    // operating freq
    if (vfo)
    {
        freq = radioStatus->getFreqA();
    }
    else
    {
       freq = radioStatus->getFreqB();
    }

    qDebug() << "freq = " << QString::number(freq);
    
    QByteArray hexFreq;
    freqtoHex(hexFreq, freq);
    data += hexFreq;
    
    // clarifier offset
    QByteArray offset;

    getRitFreq_FT1000(offset);

    data += offset;
    
    
    // operating mode
    QString modeStr;            // operating mode
    
    if (vfo)
    {
        modeStr = radioStatus->getModeA();
    }
    else
    {
        modeStr = radioStatus->getModeB();
    }
    
    data.append(QString(convertQStringtoModeCode_FT1000(modeStr)).toUtf8());

    // if filter

    data += '\x22';
    
    // VFO Mem Operating flags
    char vfoMem = '\x00';
    
    if (radioStatus->getRitState())
    {
       vfoMem = vfoMem |  '\x22';
    }
    else
    {
        vfoMem = vfoMem | '\x20';
    }
    
    
    data += vfoMem;
    
    data += '\x22';
    data += '\x91';
    data += '\x11';
    data += '\x91';
    data += '\x11';
    data += '\x00';

    qDebug() << "data = " << data.toHex(':');
    
    
}



void Yaesu::freqtoHex(QByteArray& hexFreq, const quint32 freq)
{

    quint32 f = freq / 10;

    unsigned char byte1 = (f & 0x0ff00000) >> 20;
    unsigned char byte2 = (f & 0x000ff000) >> 12;
    unsigned char byte3 = (f & 0x00000ff0) >> 4;
    unsigned char byte4 = (f & 0x0000000f) << 4;



    hexFreq.append(byte1);
    hexFreq.append(byte2);
    hexFreq.append(byte3);
    hexFreq.append(byte4);



}

void Yaesu::getRitFreq_FT1000(QByteArray &clarOffset)
{
    bool neg = false;
    int rit_i = radioStatus->getRitFreq();

    if (rit_i < 0)
    {
        neg = true;
        rit_i = rit_i * -1;
    }

    double rit_f = rit_i / 0.625;

    rit_i = qRound(rit_f);

    if (neg)
    {
        rit_i = rit_i | 0x8000;
    }

    unsigned char byte1 = (rit_i & 0xff00) >> 8;
    unsigned char byte2 = (rit_i & 0x00ff);

    clarOffset.append(byte1);
    clarOffset.append(byte2);


}


void Yaesu::catReadFiveStatusBytes()
{
    QByteArray data;

    //data += getStatusFlagByte1();
    //data += getStatusFlagByte2();
    //data += getStatusFlagByte3();
    data.append(QString(getStatusFlagByte1()).toUtf8());
    data.append(QString(getStatusFlagByte2()).toUtf8());
    data.append(QString(getStatusFlagByte3()).toUtf8());

    // ID bytes
    data += '\x03';
    data += '\x93';

    qDebug() << "read five status bytes";

    //sendCatMsg(data);
    txQueue += data;

}

void Yaesu::catReadSixStatusBytes()
{
    QByteArray data;

    data.append(QString(getStatusFlagByte1()).toUtf8());
    data.append(QString(getStatusFlagByte2()).toUtf8());
    data.append(QString(getStatusFlagByte3()).toUtf8());
    data.append(QString(getStatusFlagByte4()).toUtf8());
    data.append(QString(getStatusFlagByte5()).toUtf8());
    data.append(QString(getStatusFlagByte6()).toUtf8());

    qDebug() << "read six status bytes data";


    //sendCatMsg(data);
    txQueue += data;


}




void Yaesu::catReadSmeter(QChar cmd, QChar subCmd)
{

    Q_UNUSED(cmd)
    Q_UNUSED(subCmd)  // ignore whether it is main or sub smeter
    QByteArray data;
    QChar smeterData = '\x60';      // dummy for now

    for (int i = 0; i < 4; i++)
    {
        data.append(QString(smeterData).toUtf8());
    }

    data += '\xf7';
    qDebug() << "read s meter";

    //sendCatMsg(data);
    txQueue += data;

}


void Yaesu::catSetNumStatusBytes(QChar data)
{
    if (data == '\x00')
    {
        numStatusBytesFlag = false;
    }
    else if (data == '\x01')
    {
        numStatusBytesFlag = true;
    }

}


QChar Yaesu::getStatusFlagByte1()
{
    quint8 status1 = 0x08; // Initial value is 0x08

    if (radioStatus->getVfo())  // Only send sub VFO status
    {
        status1 = 0x08;       // 8 is CAT on
    }
    else
    {
        status1 = status1 | 0x18; // OR with 0x18 for the alternate status
    }

    return QChar(status1); // Return as QChar with the byte value
}

QChar Yaesu::getStatusFlagByte2()
{
    QChar status2 = '\x00';

    status2 = '\x20'; // only send VFO operation

    return status2;
}


QChar Yaesu::getStatusFlagByte3()
{
    QChar status3 = '\x30';

    return status3;
}

QChar Yaesu::getStatusFlagByte4()
{
    QChar status4 = '\x00';

    return status4;
}


QChar Yaesu::getStatusFlagByte5()
{
    QChar status5 = '\x00';
    return status5;
}

QChar Yaesu::getStatusFlagByte6()
{
    QChar status6 = '\x00';
    return status6;
}



void Yaesu::catSetOperatingFreqVFOA()
{
    QByteArray bcdFreq;
    bcdFreq += rxMessage.at(0);
    bcdFreq += rxMessage.at(1);
    bcdFreq += rxMessage.at(2);
    bcdFreq += rxMessage.at(3);

    quint32 freq = from_bcd(bcdFreq, 8) * 10;
    QString band;
    if (bandList->findBand(band, freq))
    {
        radioStatus->setFreqA(freq);
        emit radioStatusUpdated();
    }
    else
    {
        traceMsg(QString("Freq = %1 not supported on this radio").arg(QString::number(freq)));
    }
}


void Yaesu::catSetOperatingFreqVFOB()
{
    QByteArray bcdFreq;
    bcdFreq += rxMessage.at(0);
    bcdFreq += rxMessage.at(1);
    bcdFreq += rxMessage.at(2);
    bcdFreq += rxMessage.at(3);

    quint32 freq = from_bcd(bcdFreq, 8) * 10;

    QString band;
    if (bandList->findBand(band, freq))
    {

        radioStatus->setFreqB(freq);
        emit radioStatusUpdated();

    }
    else
    {
        traceMsg(QString("Freq = %1 not supported on this radio").arg(QString::number(freq)));
    }

}


void Yaesu::catSetVfo(QChar subCmd)
{
    if (subCmd == '\x00')
    {
        radioStatus->setVfo(true);
        emit radioStatusUpdated();
    }
    else
    {
        radioStatus->setVfo(false);
        emit radioStatusUpdated();
    }

}

void Yaesu::catSetRitFreq_FT1000()
{

    QByteArray bcdFreq;
    bcdFreq += rxMessage.at(0);
    bcdFreq += rxMessage.at(1);
    int ritFreq = static_cast<int>(from_bcd(bcdFreq, 4));

    if (rxMessage.at(2) == '\xff')
    {
        ritFreq = ritFreq * -1;
    }

    radioStatus->setRitFreq(ritFreq * 10);
    emit radioStatusUpdated();

}


void Yaesu::catSetMode_FT857()
{
    QChar m = rxMessage.at(0);
    QString mode = convertModetoQString_FT897(m);
    traceMsg(QString("Set mode = %1").arg(mode));
    if (radioStatus->getVfo())
    {
        radioStatus->setModeA(mode);
    }
    else
    {
        radioStatus->setModeB(mode);
    }

    emit radioStatusUpdated();

    sendAck();



}


void Yaesu::sendAck()
{
    QByteArray data;
    data += '\x00';
    txQueue +=  data;
}


void Yaesu::catSetMode_FT1000(QChar subCmd)
{

    QString m = convertModetoQString_FT1000(subCmd);
    traceMsg(QString("Set Mode = %1").arg(m));
    if (radioStatus->getVfo())
    {
        radioStatus->setModeA(m);
    }
    else
    {
        radioStatus->setModeB(m);
    }

    emit radioStatusUpdated();

}




QString Yaesu::convertModetoQString_FT897(QChar mode)
{
    switch(mode.digitValue())
    {
        case '\x00':
            return (*supportedModes)[0];    // LSB

        case '\x01':
            return (*supportedModes)[1];   // USB

        case '\x02':
            return (*supportedModes)[3];  // CW

        case '\x03':
            return (*supportedModes)[4];    // CW-R

        case '\x04':
            return (*supportedModes)[5];   // AM

        case '\x08':
            return (*supportedModes)[6];      // FM

        case '\x88':
            return (*supportedModes)[7];      // FM-N

        case '\x0a':
            return (*supportedModes)[8];     // DIG

        case '\x0c':
            return (*supportedModes)[9];    // PKT
        default:
            return "";


    }

}


QChar Yaesu::convertQStringtoModeCode_FT897(QString m)
{
    qDebug() << "mode = " << m;

    if ((*supportedModes)[0] == m)  // LSB
    {

        return '\x00';
    }
    else if ((*supportedModes)[1] == m)  // USB
    {
        qDebug() << m << " = 2";
        return '\x01';
    }
    else if ((*supportedModes)[2] == m)  // CW
    {

        return '\x02';
    }
    else if ((*supportedModes)[3] == m)  // CW-R
    {
        return '\x03';
    }
    else if ((*supportedModes)[4] == m)  // AM
    {
        return '\x04';
    }
    else if ((*supportedModes)[5] == m)  // FM
    {
        return '\x08';
    }
    else if ((*supportedModes)[6] == m)  // FM-N
    {
        return '\x88';
    }
    else if ((*supportedModes)[7] == m)  // DIG
    {
        return '\x0a';
    }
    else if ((*supportedModes)[8] == m)  // PKT
    {
        return '\x0c';
    }

    return '\x00';
}






QString Yaesu::convertModetoQString_FT1000(QChar mode)
{
    switch(mode.digitValue())
    {
        case '\x00':
            return (*supportedModes)[0];    // LSB

        case '\x01':
            return (*supportedModes)[1];   // USB

        case '\x02':
            return (*supportedModes)[2];  // CW

        case '\x03':
            return (*supportedModes)[4];    // AM

        case '\x04':
            return (*supportedModes)[6];   // FM

        case '\x5':
            return (*supportedModes)[8];  // RTTY

        case '\x6':
            return (*supportedModes)[11];  //PKT

        //case 8:
        //    return (*supportedModes)[7]; //RTTY-R

        default:
            return "";


    }

}


QChar Yaesu::convertQStringtoModeCode_FT1000(QString m)
{
    if (modeStrToModeTable.contains(m))
    {
        return modeStrToModeTable.value(m)[0];
    }
    else
    {
        return '0';
    }
}


void Yaesu::traceMsg(QString msg)
{
    trace(QString("Yaesu - %1").arg(msg));
}
