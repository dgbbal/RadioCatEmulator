#include "newyaesu.h"
#include "utils.h"
#include "MTrace.h"

#include <QDebug>
#include <QSettings>



NewYaesu::NewYaesu(int id_, QString modelName_, RadioStatus *radioStatus_, ConfigRotSerial *configSerial_, QObject *parent)  : RigBase(configSerial_, parent),
    radioStatus(radioStatus_),
    modelName(modelName_),
    id(id_)
{

}

NewYaesu::~NewYaesu()
{

}


void NewYaesu::register_rigs(RigFactory::Rigs* rigsList)
{

    QSettings settings("./Radios/newYaesu.ini", QSettings::IniFormat);

    QStringList availRadios = settings.childGroups();
    int numRadios = availRadios.count();

    for (int i = 0; i <numRadios; i++)
    {
        settings.beginGroup(availRadios[i]);
        QString rigManufacturer = settings.value("rigManufacturer", "").toString();
        QString rigName = settings.value("rigName", "").toString();
        QString rigModelName = rigManufacturer + " " + rigName;

        int rigModelNumber = settings.value("rigModelNumber", 0).toInt();

        int volLevelMax = 0;
        int volLevelMin = 0;

        int maxRitFreq = settings.value("maxRit", 9990).toInt();

        CATCAPTURE catCapMethodCode = RigCapabilities::getCatCaptureCode(settings.value("catCaptureMethod", "").toString().trimmed());

        QChar terminator = settings.value("terminator", ';').toChar();

        //QByteArray civArray = settings.value("civ", ' ').toByteArray();
        //hex = civArray.toInt(&ok, 16);
        QChar civAddress = QChar('\x0');

        QString kenwoodYaesuId = settings.value("kenwoodYaesuId", "").toString();
        QString supportedBands = settings.value("supportedBands", "").toString();
        QString supportedBandStartFreq = settings.value("supportedBandStartFreq", "0").toString();
        QString supportedModes = settings.value("supportedModes", "").toString();
        QString supportedModeCodes = settings.value("supportedModeCodes", "-1").toString();
        QString defaultSettings = settings.value("defaultSettings", "").toString();
        settings.endGroup();

        (*rigsList)[rigModelName] = RigCapabilities(rigManufacturer,
                                                    rigName,
                                                    rigModelName,
                                                    rigModelNumber,
                                                    civAddress,
                                                    kenwoodYaesuId,
                                                    volLevelMax,
                                                    volLevelMin,
                                                    maxRitFreq,
                                                    terminator,
                                                    supportedBands,
                                                    supportedBandStartFreq,
                                                    supportedModes,
                                                    supportedModeCodes,
                                                    defaultSettings,
                                                    catCapMethodCode);

    }

}

void NewYaesu::initRadio( Bandlist* bandList_, QStringList *supportedModes_, QStringList *supportedModeCodes_, QChar terminator_, QChar civAddress_, QString kenwoodYaesuId_ )
{

    Q_UNUSED(civAddress_)
    bandList = bandList_;
    supportedModes = supportedModes_;
    supportedModeCodes = supportedModeCodes_;
    terminator = terminator_.toLatin1();
    kenwoodYaesuId = kenwoodYaesuId_;

    if ((*supportedModes).count() == (*supportedModeCodes).count())
    {
        for (int i = 0; i < (*supportedModes).count(); i++)
        {
            modeStrToModeTable.insert((*supportedModes)[i], (*supportedModeCodes)[i]);

        }
    }
    else
    {
        trace("Error - supportedModes length != supportedModeCodes length");
    }

    voiceMsgTimer = new QTimer(this);
    connect(voiceMsgTimer, &QTimer::timeout,  this, [=](){onVmsgTimeout();});

    cwMsgTimer = new QTimer(this);
    connect(cwMsgTimer, &QTimer::timeout,  this, [=](){onCwMsgTimeout();});

    radioStatus->setVoiceMemNum('0');
    radioStatus->setCwMemNum('0');
    radioStatus->setTxState(false);


}

void NewYaesu::setMaxRitFreq(int f)
{
    maxRitFreq = f;
}

void NewYaesu::setMinRitFreq(int f)
{
   minRitFreq = f;
}



void NewYaesu::handleCatMessage(QByteArray msg)
{
    QString m(msg);
    rxMessage = m;

    qDebug() << "handle message = " << rxMessage;
    if (rxMessage.contains("AI"))
    {
        if (rxMessage.size() == 3)
        {
            readCatAI_Response();
        }
    }
    if (rxMessage.contains("AG"))
    {
        if (rxMessage[3] == ';')
        {
            readCatAG_Response();
        }
    }
    else if (rxMessage.contains("EX"))
    {
        readCatEX_Response();
    }
    else if (rxMessage.contains("ID"))
    {
        if (rxMessage.size() == 3)
        {
            readCatID_Response();
        }
    }
    else if (rxMessage.contains("IF"))
    {
        if (rxMessage.size() == 3)
        {
            readCatIF_Response();
        }
    }
    else if (rxMessage.contains("OI"))
    {
        if (rxMessage.size() == 3)
        {
            readCatOI_Response();
        }
    }
    else if (rxMessage.contains("FA"))
    {
        if (rxMessage.size() == 3)
        {
            readCatFA_Response();
        }
        else
        {
            setFACommand();

        }

    }
    else if (rxMessage.contains("FB"))
    {
        if (rxMessage.size() == 3)
        {
            readCatFB_Response();
        }
        else
        {
            setFBCommand();
        }

    }
    else if (rxMessage.contains("FT"))
    {
        if (rxMessage.size() == 3)
        {
            readCatFT_Response();
        }
    }
    else if (rxMessage.contains("KP"))
    {
        if (rxMessage.size() == 3)
        {
            readCatKP_Response();
        }
    }
    else if (rxMessage.contains("KM1"))
    {
        // extract morse message and add to morse memory channel
        QByteArray msg;
        for (int i = 3; rxMessage.at(i) != ';' && i < 24; i++)
        {
            msg.append(rxMessage.at(i).toLatin1());
        }

        catSetSendMorse(msg);

    }
    else if (rxMessage.contains("KS"))
    {
        if (rxMessage.size() == 3)
        {
            readCatKS_response();
        }
    }
    else if (rxMessage.contains("KY"))   // send Morse
    {
        // recalls the stored morse message
        QString subCmd = rxMessage.mid(2,1);
        setKYCommand(subCmd[0]);
    }
    else if (rxMessage.contains("MD"))
    {
        if (rxMessage[3] == ';')
        {
            readCatMD_Response();
        }
        else
        {
            QString subCmd = rxMessage.mid(2,2);
            setMDCommand(subCmd);

        }
    }
    else if (rxMessage.contains("NA"))
    {
        if (rxMessage.size() == 4)
        {
            readCatNA_Response();
        }
        //else
        //{
        //    //setNACommand(); // not for now
        //}
    }
    else if (rxMessage.contains("PB"))
    {
        if (rxMessage.size() == 4)
        {
            readCatPB_Response();
        }
        else
        {
            QString subCmd = rxMessage.mid(3,1);
            setCatPBCommand(subCmd[0]);
        }
    }
    else if (rxMessage.contains("PS"))
    {
        if (rxMessage.size() == 3)
        {
            readCatPS_Response();
        }
    }

    else if (rxMessage.contains("SH"))
    {
            if (rxMessage[3] == ';')
            {
                readCatSH_Response();
            }
            else
            {
                QString subCmd = rxMessage.mid(3,2);
                //setMDCommand(subCmd);

            }
    }
    else if (rxMessage.contains("SM"))
    {
        if (rxMessage[3] == ';')
        {
            readCatSM_Response();
        }
    }
    else if (rxMessage.contains("TX"))
    {
        if (rxMessage[2] == ';')
        {
            readCatTX_Response();
        }
        else
        {
            QString subCmd = rxMessage.mid(2,1);
            setCatTx(subCmd);
        }
    }
    else if (rxMessage.contains("RU"))
    {

            setRIT_Up();

    }
    else if (rxMessage.contains("RD"))
    {

            setRIT_Down();

    }
    else if (rxMessage.contains("RT"))
    {
          if (rxMessage.size() == 3)
          {
              readCatRT_Response();

          }
          else if (rxMessage.size() == 4)
          {
              setRTCommand();
          }
    }
    else if (rxMessage.contains("VS"))
    {
        if (rxMessage.size() == 3)
        {
            readCatVS_Response();

        }
        else if (rxMessage.size() == 4)
        {
            QString subCmd = rxMessage.mid(2,1);
            setVSCommand(subCmd);
        }
    }




}







void NewYaesu::readCatSH_Response()
{
    QString preamble = "SH";
    QString terminator = ";";
    QString msg = "012";

    assembleMsgToSendToCat(preamble, msg, terminator);
    sendCatTxMsg();
}

void NewYaesu::readCatSM_Response()
{
    QString preamble = "SM";
    QString terminator = ";";
    QString msg = "0127";

    assembleMsgToSendToCat(preamble, msg, terminator);
    sendCatTxMsg();
}

void NewYaesu::readCatRT_Response()
{
    QString preamble = "RT";
    QString terminator = ";";

    QString body;
    if (radioStatus->getRitState())
    {
        body = "1";
    }
    else
    {
        body = "0";
    }

    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}




void NewYaesu::setRTCommand()
{
    if (rxMessage.size() == 4)
    {
        QString state = rxMessage.mid(2,1);
        if (state == "1")
        {
            radioStatus->setRitState(true);
        }
        else
        {
            radioStatus->setRitState(false);
        }
        emit radioStatusUpdated();
    }
}



void NewYaesu::readCatID_Response()
{
    QString preamble = "ID";
    QString terminator = ";";

    assembleMsgToSendToCat(preamble, kenwoodYaesuId, terminator);
    sendCatTxMsg();

}

void NewYaesu::readCatVS_Response()
{
    QString preamble = "VS";
    QString terminator = ";";
    QString msg;
    if (radioStatus->getVfo())
    {
        msg = "0";
    }
    else
    {
        msg = "1";
    }

    assembleMsgToSendToCat(preamble, msg, terminator);
    sendCatTxMsg();
}

void NewYaesu::readCatAG_Response()
{
    QString preamble = "AG";
    QString terminator = ";";

    QString msg;
    msg = QString::number(radioStatus->getVolumeLevel(radioStatus->getRxNum()));   // volume mid poin

    if (msg.length() == 3)
    {
        msg.prepend("0");
    }
    else if (msg.length() == 2)
    {
        msg.prepend("00");
    }
    else if (msg.length() == 1)
    {
        msg.prepend("000");
    }

    assembleMsgToSendToCat(preamble, msg, terminator);
    sendCatTxMsg();
}

void NewYaesu::readCatEX_Response()
{
    QString preamble = "";
    QString terminator = "";

    // send back recieved message
    QString msg = rxMessage;

    assembleMsgToSendToCat(preamble, msg, terminator);
    sendCatTxMsg();
}


void NewYaesu::readCatAI_Response()
{
    QString preamble = "AI";
    QString terminator = ";";
    QString msg = "0";

    assembleMsgToSendToCat(preamble, msg, terminator);
    sendCatTxMsg();

}


void NewYaesu::readCatFA_Response()
{
    QString preamble = "FA";
    QString terminator = ";";
    QString msg;

    QString freq;

    getFreqToSendToCat(freq, radioStatus->VFOA);

    assembleMsgToSendToCat(preamble, freq, terminator);
    sendCatTxMsg();

}

void NewYaesu::readCatFB_Response()
{
    QString preamble = "FB";
    QString terminator = ";";
    QString msg;

    QString freq;

    getFreqToSendToCat(freq, radioStatus->VFOB);

    assembleMsgToSendToCat(preamble, freq, terminator);
    sendCatTxMsg();
}

void NewYaesu::readCatKP_Response()
{
    QString preamble = "KP";
    QString terminator = ";";
    QString Pitch = "50";
    assembleMsgToSendToCat(preamble, Pitch, terminator);
    sendCatTxMsg();
}

void NewYaesu::readCatKS_response()
{
    QString preamble = "KS";
    QString terminator = ";";
    QString cwSpeed= "012";
    assembleMsgToSendToCat(preamble, cwSpeed, terminator);
    sendCatTxMsg();
}

void NewYaesu::readCatNA_Response()
{
    QString preamble = "NA";
    QString terminator = ";";
    QString msg = "00";

    assembleMsgToSendToCat(preamble, msg, terminator);
    sendCatTxMsg();

}

void NewYaesu::readCatPB_Response()
{
    // not implemented in hamlib
}

void NewYaesu::readCatPS_Response()
{
    QString preamble = "PS";
    QString terminator = ";";
    QString msg = "1";

    assembleMsgToSendToCat(preamble, msg, terminator);
    sendCatTxMsg();

}



void NewYaesu::readCatTX_Response()
{
    QString preamble = "TX";
    QString terminator = ";";
    QString TXState;

    if (radioStatus->getTxState())
    {
        TXState = "1";
    }
    else
    {
        TXState = "0";
    }

    assembleMsgToSendToCat(preamble, TXState, terminator);
    sendCatTxMsg();
}

void NewYaesu::setCatTx(QString subCmd)
{
    if (subCmd == "1")
    {
        radioStatus->setTxState(true);  // tx on
    }
    else
    {
        radioStatus->setTxState(false);  // tx off

    }
    emit radioStatusUpdated();
}

void NewYaesu::readCatFT_Response()
{

    QString preamble = "FT";
    QString terminator = ";";
    QString P2;

    if (radioStatus->getVfo())
    {
        P2 = "0";
    }
    else
    {
        P2 = "1";
    }

    assembleMsgToSendToCat(preamble, P2, terminator);
    sendCatTxMsg();

}

void NewYaesu::readCatIF_Response()
{

    QString preamble = "IF";
    QString terminator = ";";

    QString memoryChannel = "000"; // P1

    QString freq;                   // P2 VF0-A Freq
    getFreqToSendToCat(freq, radioStatus->VFOA);

    QString ritFreq;        // P3
    getRitFreqToSendToCat(ritFreq);

    QString ritStatus;  //P4
    if (radioStatus->getRitState())
    {
        ritStatus = "1";
    }
    else
    {
        ritStatus = "0";
    }

    QString xitStatus = "0"; // P5

    QString mode = getStatusOfMode(); // P6


    QString vfoMemStatus = "0"; // P7

    QString ctcssStatus = "0";  // P8

    QString fixed = "00";  // P9


    QString shiftStatus = "0"; //P10

    QString msgBody = memoryChannel + freq + ritFreq + ritStatus
                      + xitStatus + mode + vfoMemStatus
                        + ctcssStatus + fixed + shiftStatus;

    assembleMsgToSendToCat(preamble, msgBody, terminator);
    sendCatTxMsg();

}


void NewYaesu::readCatOI_Response()
{

    QString preamble = "OI";
    QString terminator = ";";

    QString memoryChannel = "000"; // P1

    QString freq;                   // P2 VF0-B Freq
    getFreqToSendToCat(freq, radioStatus->VFOB);

    QString ritFreq;        // P3
    getRitFreqToSendToCat(ritFreq);

    QString ritStatus;  //P4
    if (radioStatus->getRitState())
    {
        ritStatus = "1";
    }
    else
    {
        ritStatus = "0";
    }

    QString xitStatus = "0"; // P5

    QString mode = getStatusOfMode(); // P6


    QString vfoMemStatus = "0"; // P7

    QString ctcssStatus = "0";  // P8

    QString fixed = "00";  // P9


    QString shiftStatus = "0"; //P10

    QString msgBody = memoryChannel + freq + ritFreq + ritStatus
                      + xitStatus + mode + vfoMemStatus
                        + ctcssStatus + fixed + shiftStatus;

    assembleMsgToSendToCat(preamble, msgBody, terminator);
    sendCatTxMsg();

}



void NewYaesu::getRitFreqToSendToCat(QString &f)
{
    f = QString::number(radioStatus->getRitFreq()).remove('-');

    qDebug() << "rit = " << f;

    int  lenRitFreq = 4;


    while (f.size() != lenRitFreq)
    {
        f.prepend("0");
    }

    if (radioStatus->getRitFreq() < 0)
    {
        f.prepend("-");
    }
    else
    {
        f.prepend("+");
    }
}

QString NewYaesu::getStatusOfMode()
{
    QString m;
    if (radioStatus->getVfo())
    {
        m = radioStatus->getModeA();

    }
    else
    {
        m = radioStatus->getModeB();
    }

    m = convertQStringtoModeCode(m);


    qDebug() << "m = " << m;
    return m;
}

void NewYaesu::assembleMsgToSendToCat( QString &preamble, QString &msgBody, QString &terminator)
{

    catTxMessage = preamble + msgBody + terminator;



}


void NewYaesu::readCatMD_Response()
{
    QString preamble = "MD";
    QString terminator = ";";

    QString modeCode;
    if (radioStatus->getVfo())
    {
        modeCode = convertQStringtoModeCode(radioStatus->getModeA());
    }
    else
    {
         modeCode = convertQStringtoModeCode(radioStatus->getModeB());
    }

    QString msgBody = "0" + modeCode;

    assembleMsgToSendToCat(preamble, msgBody, terminator);
    sendCatTxMsg();
}

void NewYaesu::setMDCommand(QString subCmd)
{
    if (subCmd.size() == 2)
    {
       if (radioStatus->getVfo())
       {
           radioStatus->setModeA(convertModetoQString(subCmd.mid(1,1)));
       }
       else
       {
           radioStatus->setModeB(convertModetoQString(subCmd.mid(1,1)));
       }

       emit radioStatusUpdated();
    }
}



void NewYaesu::getFreqToSendToCat(QString &f, bool vfo)
{
    if (vfo)
    {
        f = QString::number(radioStatus->getFreqA());
    }
    else
    {
        f = QString::number(radioStatus->getFreqB());
    }
    while (f.size() != 9)
    {
        f.prepend("0");
    }
}


void NewYaesu::setCatPBCommand(QChar subCmd)
{
    if (subCmd == '0')
    {
        // stop command
        radioStatus->setVoiceMemNum(subCmd);
        voiceMemoryStop(subCmd);

    }
    else if (subCmd >= '1' && subCmd <= '8')
    {
         //radioStatus->setVoiceMemNum(subCmd);
         voiceMemoryStart(subCmd);
    }
}


void NewYaesu::voiceMemoryStop(QChar subCommandNum)
{
    voiceMsgTimer->stop();
    radioStatus->setVoiceMemNum(subCommandNum);
    radioStatus->setTxState(false);

    emit radioStatusUpdated();
}

void NewYaesu::voiceMemoryStart(QChar subCommandNum)
{
    radioStatus->setVoiceMemNum(subCommandNum);
    radioStatus->setTxState(true);

    voiceMsgTimer->start(10 * 1000);
    emit radioStatusUpdated();
}


void NewYaesu::onVmsgTimeout()
{
    voiceMsgTimer->stop();
    radioStatus->setTxState(false);
    radioStatus->setVoiceMemNum('0');
    emit radioStatusUpdated();
}

void NewYaesu::catSetSendMorse(QByteArray msg)
{
    radioStatus->setMorseText(msg);
    //radioStatus->setTxState(true);
    //cwMsgTimer->start(25 * 1000);

    emit radioStatusUpdated();
}


void NewYaesu::setKYCommand(QChar subCommandNum)
{
    if (subCommandNum == '0' && subCommandNum > '5')
    {
        return; // stop not available?
    }

    radioStatus->setCwMemNum(subCommandNum);
    radioStatus->setTxState(true);
    cwMsgTimer->start(25 * 1000);
    emit radioStatusUpdated();

}

void NewYaesu::setVSCommand(QString subCommandNum)
{
    if (subCommandNum == "0")
    {
        radioStatus->setVfo(true);
    }
    else
    {

       radioStatus->setVfo(false);
    }

    emit radioStatusUpdated();
}

void NewYaesu::onCwMsgTimeout()
{
    cwMsgTimer->stop();
    radioStatus->setTxState(false);
    radioStatus->setCwMemNum('0');
    emit radioStatusUpdated();
}


void NewYaesu::setFACommand()
{

    QStringList sl = rxMessage.split('A');
    if (sl.size() == 2)
    {
        QString freq = sl[1].remove(';');
        if (freq.size() == 9)
        {
            freq.remove( QRegularExpression("^[0]*") ); // remove leading zeros
            //radioStatus->setCurrentFreqA(freq);
            radioStatus->setFreqA(freq.toULong());
            emit radioStatusUpdated();
        }
    }
}

void NewYaesu::setFBCommand()
{

    QStringList sl = rxMessage.split('B');
    if (sl.size() == 2)
    {
        QString freq = sl[1].remove(';');
        if (freq.size() == 9)
        {
            freq.remove( QRegularExpression("^[0]*") ); // remove leading zeros
            //radioStatus->setCurrentFreqB(freq);
            radioStatus->setFreqB(freq.toULong());
            emit radioStatusUpdated();
        }
    }
}

void NewYaesu::sendCatTxMsg()
{
    QByteArray msg = catTxMessage.toLocal8Bit();
    sendCatMsg(msg);
}


void NewYaesu::setRIT_Up()
{

    if (rxMessage.size() == 3)
    {
        int f = radioStatus->getRitFreq() + 1;
        if (f > maxRitFreq)
        {
            return;
        }

        radioStatus->setRitFreq(f);
        emit radioStatusUpdated();

    }
    else if (rxMessage.size() == 7)
    {
        bool ok;
        QString ritFreqStr = rxMessage.mid(2,4);
        int f = ritFreqStr.toInt(&ok);
        if (ok)
        {

            if (f > maxRitFreq)
            {
                return;
            }
            radioStatus->setRitFreq(f);
            emit radioStatusUpdated();
        }


    }




}


void NewYaesu::setRIT_Down()
{
    if (rxMessage.size() == 3)
    {
            int f = radioStatus->getRitFreq() - 1;
            if (f < maxRitFreq)
            {
                return;
            }

            radioStatus->setRitFreq(f);
            emit radioStatusUpdated();
    }
    else if (rxMessage.size() == 7)
    {
        bool ok;
        QString ritFreqStr = rxMessage.mid(2,4);
        int f = ritFreqStr.toInt(&ok);
        if (ok)
        {

            if (f > maxRitFreq)   // value is positve at this point
            {
                return;
            }
            radioStatus->setRitFreq(f * -1);  // now make it negative
            emit radioStatusUpdated();
        }


    }

}

QString NewYaesu::convertModetoQString(QString mode)
{
    int lookup = 0;

    if (mode == "A")
    {
        lookup = 10;
    }
    else if (mode == "B")
    {
        lookup = 11;
    }
    else if (mode == "C")
    {
        lookup = 12;
    }
    else if (mode == "D")
    {
        lookup = 13;
    }
    else if (mode == "E")
    {
        lookup = 14;
    }
    else if (mode.toInt() >= 1 && mode.toInt() <= 9)
    {
        lookup = mode.toInt();
    }
    else
    {
        return "";
    }

    switch(lookup)
    {
        case 1:
            return (*supportedModes)[0];

        case 2:
            return (*supportedModes)[1];

        case 3:
            return (*supportedModes)[2];

        case 4:
            return (*supportedModes)[3];

        case 5:
            return (*supportedModes)[4];

        case 6:
            return (*supportedModes)[5];

        case 7:
            return (*supportedModes)[6];

        case 8:
            return (*supportedModes)[7];

        case 9:
            return (*supportedModes)[8];

        case 10:
            return (*supportedModes)[9];

        case 11:
            return (*supportedModes)[10];

        case 12:
            return (*supportedModes)[11];

        case 13:
            return (*supportedModes)[12];

        case 14:
            return (*supportedModes)[13];

        default:
            return "";


    }


}




QChar NewYaesu::convertQStringtoModeCode(QString m)
{
    if (modeStrToModeTable.contains(m))
    {
        return modeStrToModeTable.value(m)[0];
    }
    else
    {
        return '0';   // invalid mode
    }



}
