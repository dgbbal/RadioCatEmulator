#include "bandlist.h"
#include <QSettings>


Bandlist::Bandlist()
{
    // bandlist created per radio creation

}


quint32 Bandlist::getBandListHighf(QString band)
{
    return bandList[band].getHighFreq();
}
quint32 Bandlist::getBandListLowf(QString band)
{
    return bandList[band].getLowFreq();
}

void Bandlist::setBandDetail(QString &band, BandDetials &bd)
{
    bandList[band] = bd;
}

BandDetials Bandlist::getBandDetail(QString &band)
{
    return bandList[band];
}

void Bandlist::clear()
{
    bandList.clear();
}

bool Bandlist::findBand(QString& band, quint32 f)
{
    for(auto i : bandList.keys())
    {
         BandDetials bd = bandList.value(i);
         if (f >= bd.getLowFreq() && f <= bd.getHighFreq())
         {
             band = bd.getBand();
             return true;
         }
    }

    return false;


}
