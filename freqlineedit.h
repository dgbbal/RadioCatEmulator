/////////////////////////////////////////////////////////////////////////////
// $Id$
//
// PROJECT NAME 		Minos Amateur Radio Control and Logging System
//
// Copyright        (c) D. G. Balharrie M0DGB/G8FKH 2017
//
//
//
/////////////////////////////////////////////////////////////////////////////




#ifndef FREQLINEEDIT_H
#define FREQLINEEDIT_H

#include <QLineEdit>
#include "bandlist.h"

class FreqLineEdit : public QLineEdit
{
    Q_OBJECT

public:

    FreqLineEdit(QWidget *parent);
    ~FreqLineEdit() override;
    void changeFreq(bool direction);
    //QString convertFreqString(double frequency);

    void setBandList(Bandlist *bandlist_);
signals:

    void newFreq();
    void freqEditReturn();
private:

    Bandlist* bandlist;
    void wheelEvent(QWheelEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;



} ;

#endif // FREQLINEEDIT_H
