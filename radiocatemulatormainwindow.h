#ifndef RADIOCATEMULATORMAINWINDOW_H
#define RADIOCATEMULATORMAINWINDOW_H

#include <QMainWindow>
#include <QMap>
#include "configrotserial.h"
#include "rigutils.h"
#include "bandlist.h"
#include "radiostatus.h"
#include "rigbase.h"
#include "rigcapabilities.h"
#include "rigfactory.h"

namespace maskData {

    const QStringList freqMask = { "9.999",
                             "99.999",
                             "999.999",
                             "9.999.999",
                             "99.999.999",
                             "999.999.999",
                             "9.999.999.999",
                             "99.999.999.999"};
    enum freqMask_lookup {
                      KHZ = 0,              /* 9 Khz   */
                      KHZ99,        		/* 99 khz  */
                      KHZ999,        		/* 999 khz */
                      MHZ,                  /* 9 Mhz   */
                      MHZ99,        		/* 99 Mhz  */
                      MHZ999,       		/* 999 Mhz */
                      GHZ,                  /* Ghz     */
                      GHZ99                 /* Ghz 99  */
    };

}



QT_BEGIN_NAMESPACE
namespace Ui { class RadioCatEmulatorMainWindow; }
QT_END_NAMESPACE



const QString TX_BUTTON_ON = QString("background-color: red; color:black; border-style: outset; border-width: 1px; border-color: black;\n");
const QString TX_BUTTON_OFF = QString("background-color: Gainsboro ; color:black; border-style: outset; border-width: 1px; border-color: black;\n");


class RadioCatEmulatorMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    RadioCatEmulatorMainWindow(QWidget *parent = nullptr);
    ~RadioCatEmulatorMainWindow();

private slots:
    void onRadioFreqEditReturn();
    void onNewDialFreq();
    void onVolumeChanged(int value);
    void onNewRitFreq(int idx);
    void onRadioModelChanged(int idx);
    void onbaudRateChanged(int idx);
    void onComportChanged(int idx);
    void onRadioModeChanged(int idx);
    void onRadioBandFreq(int idx);
    void freqLineEditInFocus();
    void freqEditSelected();
    void changeMainRadioFreq();
    void onSerialDataReceived();
    void onRitButtonClicked();
    void onVfoButtonClicked();
    void onCatTxMsg(QString msg);
    void onCatRxMsg(QByteArray msg);
    void onTxError();
    void onRadioStatusUpdated();
    void onDataBitsChanged(int idx);
    void onStopBitsChanged(int idx);

    void onCearRitPbClicked();
    void onTxPushButtonClicked();
    void onCatTxButtonClicked();
    void onCtsButtonClicked();
    void onCtsPollTimerTimeout();
    void onCtsComportChanged(int);
    void onCtsOn(bool state);
    void onSaveRadioButtonClicked();
private:
    Ui::RadioCatEmulatorMainWindow *ui;

    // Cat port
    ConfigRotSerial* configSerial;
    QString comport;
    QString baudRate;
    int baudRateIndex = 0;
    QString databits;
    QString stopbits;
    
    // Ptt port
    ConfigRotSerial* pttSerialPort;
    QString pttComport;

    QTimer *ctsPollTimer;
    
    

    RigBase  *radio;
    RigFactory* rigFactory;

    int rigModelNumber = 0;
    QString rigModelName;
    QString rigManufacturer;

    int maxRitFreq;
    bool ritKHzFlag;


    RadioStatus *radioStatus;

    QString selectedRadio;

    bool freqEditOn = false;

    Bandlist* bandlist;



    QStringList supportedBandList;
    //QMap<QString, BandDetials> bandList;

    QStringList supportedBandStartFreq;
    QStringList supportedModes;
    QStringList supportedModeCodes;


    //QStringList modelStrList = {"Kenwood TS2000"};
    QStringList baudStrList = {"4800", "9600", "19200", "38400"};
    QStringList databitsStrList = {"7", "8"};
    QStringList stopbitsStrList = {"1", "2"};
    //QStringList bandStrList = {"1.8 MHz", "3.5 MHz", "7 MHz", "14 MHz", "28 MHz", "50 MHz", "70 MHz", "144 MHz", "432 MHz", "1296 MHz"};
    //QStringList modeStrList = {"USB", "LSB", "CW", "CW-R", "FM", "AM"};
    void populateCombo(QComboBox *comboBox, QStringList &textList, bool firstListBlank);
    bool isFirstItemBlank(QComboBox *comboBox);
    void setRitLabel();
    bool eventFilter(QObject *obj, QEvent *event);
    void freqLineEditFrameColour(bool status);
    void exitFreqEdit();
    void exitRitFreqEdit();
    void ritLineEditInFocus();
    void ritFreqLineEditFrameColour(bool status);
    bool checkValidFreq(QString freq);
    bool checkValidFreq(quint32 freq);
    void sendToTextWindow(QString startTxt, QString msg);
    void handleCATMessage();
    void getRitFreqToSendToCat(QString &f);
    void assembleMsgToSendToCat(QString &preamble, QString &msgBody, QString &terminator);
    void sendCatIDResponse();
    void sendCatIFResponse();
    void sendCatAIResponse();
    void sendCatPSResponse();
    void sendCatMDResponse();
    void sendCatAGResponse();
    void sendCatSMResponse();
    void sendCatFAResponse();
    void getFreqToSendToCat(QString &f, bool vfo);
    void sendCatFBResponse();
    void sendCatRXResponse();
    void setVfoLabel();
    void closeRadio();
    void openRadio(QString selectedRadio);
    void setModeLabel(QString m);

    void saveRadioStatus(QString& rigModelName);
    void readRadioSettings(QString& rigModelName, RadioStatus &radioStatus);
    void saveCommsSetting(QString &rigModelName);
    void closeEvent(QCloseEvent *event);


    void setTXStateLabel(bool state);
    void setVoiceMemLabel(QChar voiceMemNum);
    bool validateComParams();



    void setCatCtsRadioButton(bool catCtsTxFlag);

    void closeComport(ConfigRotSerial *serialObj, const QString name, const QString comport);
    void openComport(ConfigRotSerial *serialObj, const QString name, const QString comport, const int baudRateIndex, const QString databits, const QString stopbits);
    void readCommsSetting(QString &rigModelName, QString &comport, QString &pttComport, QString &baudrate, QString &databits, QString &stopbits);
    void setCwMemLabel(QChar cwMemNum);
};
#endif // RADIOCATEMULATORMAINWINDOW_H
