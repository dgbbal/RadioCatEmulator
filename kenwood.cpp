#include "kenwood.h"

#include <QDebug>
#include <QSettings>

Kenwood::Kenwood(int id_, QString modelName_, RadioStatus *radioStatus_, ConfigRotSerial *configSerial_, QObject *parent) : RigBase(configSerial_, parent),
    radioStatus(radioStatus_),
    modelName(modelName_),
    id(id_)
{


}


Kenwood::~Kenwood()
{


}




void Kenwood::register_rigs(RigFactory::Rigs* rigsList)
{

    QSettings settings("./Radios/kenwood.ini", QSettings::IniFormat);

    QStringList availRadios = settings.childGroups();
    int numRadios = availRadios.size();

    for (int i = 0; i <numRadios; i++)
    {
        settings.beginGroup(availRadios[i]);
        QString rigManufacturer = settings.value("rigManufacturer", "").toString();
        QString rigName = settings.value("rigName", "").toString();
        QString rigModelName = rigManufacturer + " " + rigName;

        int rigModelNumber = settings.value("rigModelNumber", 0).toInt();

        QString kenwoodYaesuId = settings.value("kenwoodRigId", "000").toString();
        int volLevelMax = settings.value("maxVol", 0).toInt();
        int volLevelMin = settings.value("minVol", 255).toInt();
        int maxRitFreq = settings.value("maxRit", 9999).toInt();

        QChar terminator = settings.value("terminator", ';').toChar();

        CATCAPTURE catCapMethodCode = RigCapabilities::getCatCaptureCode(settings.value("catCaptureMethod", "").toString().trimmed());

        QString supportedBands = settings.value("supportedBands", "").toString();
        QString supportedBandStartFreq = settings.value("supportedBandStartFreq", "0").toString();
        QString supportedModes = settings.value("supportedModes", "").toString();
        QString supportedModeCodes = settings.value("supportedModeCodes", "-1").toString();
        QString defaultSettings = settings.value("defaultSettings", "").toString();

        QString supportedOpCodesStr = settings.value("supportedOPCodes", "").toString();
        QByteArray supportedOpCodes;
        supportedOpCodes += supportedOpCodesStr.toLatin1();
        settings.endGroup();

        QChar civAddress = ' ';

        (*rigsList)[rigModelName] = RigCapabilities(rigManufacturer,
                                                    rigName,
                                                    rigModelName,
                                                    rigModelNumber,
                                                    civAddress,
                                                    kenwoodYaesuId,
                                                    volLevelMax,
                                                    volLevelMin,
                                                    maxRitFreq,
                                                    terminator,
                                                    supportedBands,
                                                    supportedBandStartFreq,
                                                    supportedModes,
                                                    supportedModeCodes,
                                                    defaultSettings,
                                                    catCapMethodCode);

    }

}

void Kenwood::initRadio( Bandlist* bandList_, QStringList *supportedModes_, QStringList *supportedModeCodes_, QChar terminator_, QChar civAddress_, QString kenwoodYaesuId_)
{

    Q_UNUSED(civAddress_)

    bandList = bandList_;
    supportedModes = supportedModes_;
    supportedModeCodes = supportedModeCodes_;
    terminator = terminator_;
    kenwoodYaesuId = kenwoodYaesuId_;

    // we need to sort modes for none Kenwood radios like Flex Thetis

    if ((*supportedModes).size() == (*supportedModeCodes).size())
    {
        for (int i = 0; i < (*supportedModes).size(); i++)
        {
            modeStrToModeTable.insert((*supportedModes)[i], (*supportedModeCodes)[i]);

        }
    }
    else
    {
        trace("Error - supportedModes length != supportedModeCodes length");
    }

    voiceMsgTimer = new QTimer(this);
    connect(voiceMsgTimer, &QTimer::timeout,  this, [=](){onVmsgTimeout();});

    cwMsgTimer = new QTimer(this);
    connect(cwMsgTimer, &QTimer::timeout,  this, [=](){onCwMsgTimeout();});

    radioStatus->setVoiceMemNum('0');
    radioStatus->setCwMemNum('0');
    radioStatus->setTxState(false);

}

void Kenwood::setMaxRitFreq(int f)
{
    maxRitFreq = f;
}

void Kenwood::setMinRitFreq(int f)
{
   minRitFreq = f;
}


void Kenwood::handleCatMessage(QByteArray msg)
{
    QString m(msg);
    rxMessage = m;

    qDebug() << "handle message = " << rxMessage;

    // do these Thetis commands first
    if (rxMessage.contains("ZZMD"))
    {
        if (rxMessage.size() == 5)
        {
            readCatZZMD_Response();
        }
        else
        {
            setCatZZMD_Command();
        }
    }
    else if (rxMessage.contains("ZZAG"))
    {

        if (rxMessage.size() == 5)
        {
            readCatZZAG_Response();
        }
        else
        {
            setCatZZAG_Command();
        }


    }
    else if (rxMessage.contains("ZZFL"))
    {
        if (rxMessage.size() == 5)
        {
            readCatZZFL_Response();
        }
        else
        {
            setCatZZFL_Command();
        }
    }
    else if (rxMessage.contains("ZZFH"))
    {
        if (rxMessage.size() == 5)
        {
            readCatZZFH_Response();
        }
        else
        {
            setCatZZFH_Command();
        }
    }
    else if (rxMessage.contains("ZZTX"))
    {
        if (rxMessage.size() == 5)
        {
            readCatZZTX_Response();
        }
        else
        {
            setCatZZTX_Command();
        }
    }
    else if (rxMessage.contains("ZZRM"))
    {
        if (rxMessage.size() == 6)
        {
            readCatZZRM_Response();
        }
        else
        {
            setCatZZRM_Command();
        }
    }
    else if (rxMessage.contains("FA"))
    {
        if (rxMessage.size() == 3)
        {
            readCatFA_Response();
        }
        else
        {
            setFACommand();

        }

    }
    else if (rxMessage.contains("FB"))
    {
        if (rxMessage.size() == 3)
        {
            readCatFB_Response();
        }
        else
        {
            setFBCommand();
        }

    }
    else if (rxMessage.contains("DA"))
    {
        if (rxMessage.size() == 3)
        {
            readCatDA_Response();
        }
    }
    else if (rxMessage.contains("ID"))
    {
        if (rxMessage.size() == 3)
        {
            readCatID_Response();
        }
    }
    else if (rxMessage.contains("IF"))
    {

        if (rxMessage.size() == 3)
        {

            readCatIF_Response();
        }
    }
    else if (rxMessage.contains("AI"))
    {
        if (rxMessage.size() == 3)
        {
            readCatAI_Response();
        }
    }
    else if (rxMessage.contains("PS"))
    {
        if (rxMessage.size() == 3)
        {
            readCatPS_Response();
        }
    }



    else if (rxMessage.contains("MD"))
    {
        if (rxMessage.size() == 3)
        {
            readCatMD_Response();
        }
        else
        {
            setMDCommand();
        }
    }
    else if (rxMessage.contains("SM"))
    {
        if (rxMessage.size() == 4 || rxMessage.size() == 3)   // TS590 count = 4, TS890S count = 3
        {
            readCatSM_Response();
        }

    }
    else if (rxMessage.contains("AG"))
    {
        int rxNum = rxMessage[2].digitValue();
        if (rxNum == 0 || rxNum == 1)
        {
            if (rxMessage.size() == 4)
            {

                readCatAG_Response(rxNum);
            }
            else
            {
                setAGCommand(rxNum);
            }
        }


    }
    else if (rxMessage.contains("RX"))
    {
        if (rxMessage.size() == 3)
        {
            readCatRX_Response();
        }
    }
    else if (rxMessage.contains("FV"))
    {
        readCatFV_Response();
    }
    else if (rxMessage.contains("FW"))
    {
        readCatFW_Response();
    }
    else if (rxMessage.contains("RC"))
    {
        setRCCommand();
    }
    else if (rxMessage.contains("RT"))
    {
          if (rxMessage.size() == 3)
          {
              readCatRT_Response();

          }
          else if (rxMessage.size() == 4)
          {
              setRTCommand();
          }
    }
    else if (rxMessage.contains("RU"))
    {

            setRIT_Up();

    }
    else if (rxMessage.contains("RD"))
    {

            setRIT_Down();

    }
    else if (rxMessage.contains("PB"))
    {
        if (modelName == "Kenwood TS890S")
        {
            QString subCmd = rxMessage.mid(2,1);
            if (subCmd == "0")
            {
                // This is PB0 turning list display on we ignore this
            }
            else if (subCmd == "1")  // Voice Message Playback
            {
                if (rxMessage.size() == 4)
                {
                    readCatPB1_TS890S_Response();
                }
                else
                {
                    subCmd = rxMessage.mid(3,2);  // we want message number and operation
                    setCatPB1_TS890S_Command(subCmd);
                }
            }
        }
        else if (modelName.contains("Kenwood") && modelName.contains("590"))
        {
            if (rxMessage.size() == 3)
            {
                readCatPB_TS590_Response();
            }
            else
            {
                QString subCmd = rxMessage.mid(2,1);
                setCatPB_TS590_Command(subCmd[0]);
            }

        }

    }
    else if (rxMessage.contains("KY"))
    {
        if (rxMessage.size() == 3)
        {
            if (rxMessage.mid(2,1) == ';')
            {
                readCatKY_Response();
            }
        }
        else if (rxMessage == "KY0;")
        {
            // stop morse
            // These radios support stop morse
            // flex6xxx.c:    .stop_morse =  kenwood_stop_morse,
            // ts590.c:    .stop_morse =  kenwood_stop_morse,
            // ts890s.c:    .stop_morse =  kenwood_stop_morse,
            // ts990s.c:    .stop_morse =  kenwood_stop_morse

            catSetStopMorse();
        }
        else if (rxMessage.size() > 3 && rxMessage.contains(';'))
        {
            QByteArray msg;
            for (int i = 3; rxMessage.at(i) != ';' && i < 24; i++)
            {
                msg.append(rxMessage.at(i).toLatin1());
            }

            catSetSendMorse(msg);


        }

    }
    else if (rxMessage.contains("EX"))
    {
        readCatEX_Response();     // TS890S command
    }
    else if (rxMessage.contains("KS"))
    {
        readCatKS_Response();    // TS890S Command
    }


}




void Kenwood::readCatDA_Response()
{
    QString preamble = "DA";
    QString terminator = ";";
    QString msg = "0";




    assembleMsgToSendToCat(preamble, msg, terminator);
    sendCatTxMsg();

}




void Kenwood::readCatFA_Response()
{
    QString preamble = "FA";
    QString terminator = ";";
    QString msg;

    QString freq;

    getFreqToSendToCat(freq, radioStatus->VFOA);

    assembleMsgToSendToCat(preamble, freq, terminator);
    sendCatTxMsg();

}

void Kenwood::readCatFB_Response()
{
    QString preamble = "FB";
    QString terminator = ";";
    QString msg;

    QString freq;

    getFreqToSendToCat(freq, radioStatus->VFOB);

    assembleMsgToSendToCat(preamble, freq, terminator);
    sendCatTxMsg();
}


void Kenwood::assembleMsgToSendToCat( QString &preamble, QString &msgBody, QString &terminator)
{

    catTxMessage = preamble + msgBody + terminator;



}



void Kenwood::getFreqToSendToCat(QString &f, bool vfo)
{
    if (vfo)
    {
        f = QString::number(radioStatus->getFreqA());
    }
    else
    {
        f = QString::number(radioStatus->getFreqB());
    }
    while (f.size() != 11)
    {
        f.prepend("0");
    }
}


void Kenwood::getRitFreqToSendToCat(QString &f)
{
    f = QString::number(radioStatus->getRitFreq()).remove('-');

    int lenRitFreq = 0;
    if (modelName == TS2000 || modelName == TS480 || modelName == THETIS)
    {
        lenRitFreq = 5;
    }
    else if (modelName == TS590S || modelName == TS890S || modelName == TS480) // assume 890S is same as 590S
    {
        lenRitFreq = 4;
    }
    else
    {
        lenRitFreq = 4;  // default to stop crash!
    }

    while (f.size() != lenRitFreq)
    {
        f.prepend("0");
    }

    if (radioStatus->getRitFreq() < 0)
    {
        f.prepend("-");
    }
    else
    {
        f.prepend("+");
    }
    qDebug() << "read RIT = " << f;
}

void Kenwood::readCatAI_Response()
{
    QString preamble = "AI";
    QString terminator = ";";
    QString body = "1";

    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}

void Kenwood::readCatMD_Response()
{
    QString preamble = "MD";
    QString terminator = ";";
    QString msgBody = getStatusOfMode();

    if  (msgBody == "error")
    {
        msgBody = '0';  // couldn't find code, respond with single char code
    }

    assembleMsgToSendToCat(preamble, msgBody, terminator);
    sendCatTxMsg();
}


QString Kenwood::getStatusOfMode()
{
    QString m;
    if (radioStatus->getVfo())
    {
        m = radioStatus->getModeA();

    }
    else
    {
        m = radioStatus->getModeB();
    }
    m = convertQStringtoModeCode(m);
    qDebug() << "m = " << m;
    return m;
}

void Kenwood::readCatRX_Response()
{
    QString preamble = "RX";
    QString terminator = ";";
    QString body = "0";

    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}


void Kenwood::readCatEX_Response()
{
    QString preamble = "EX";
    QString terminator = ";";
    QString body = "00011 000";

    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();

}

void Kenwood::readCatKS_Response()
{
    QString preamble = "KS";
    QString terminator = ";";
    QString body = "020";

    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}



void Kenwood::readCatSM_Response()
{
    QString preamble = "SM";
    QString terminator = ";";
    QString body;
    if (modelName == TS590S || modelName == TS2000 || modelName == TS480 || modelName == THETIS)
    {
        body = "00010";
    }
    else if (modelName == TS890S || modelName == QRPLABS)
    {
       body = "0010";
    }


    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}

// Flexradio command
void Kenwood::readCatZZMD_Response()
{
    QString preamble = "ZZMD";
    QString terminator = ";";
    QString body = getStatusOfMode();
    if (body == "error")
    {
        trace("error readCatZZMD command ");
        body = "00";  // errror respond with two chars
    }

    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}

void Kenwood::setCatZZMD_Command()
{

}
// Flexradio command
void Kenwood::readCatZZAG_Response()
{
    QString preamble = "ZZAG";
    QString terminator = ";";
    int rxNum = 0;
    QString body = QString::number(radioStatus->getVolumeLevel(rxNum));
    QString rxNumStr = QString::number(radioStatus->getRxNum());
    while (body.size() < 3)
    {
        body.prepend('0');
    }

    body = rxNumStr + body;

    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}

void Kenwood::setCatZZAG_Command()
{
    QString volLevelStr = rxMessage.mid(4,6);
    bool ok = false;
    int volLevel = volLevelStr.toInt(&ok);
    if (ok && volLevel >= 0 && volLevel <= 100)
    {
        radioStatus->setVolumeLevel(volLevel, 0); // force main
        emit radioStatusUpdated();
    }
}

void Kenwood::readCatZZFL_Response()
{
    QString preamble = "ZZFL";
    QString terminator = ";";
    QString body = "00000";


    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}

void Kenwood::setCatZZFL_Command()
{

}

void Kenwood::readCatZZFH_Response()
{
    QString preamble = "ZZFH";
    QString terminator = ";";
    QString body = "00000";


    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}

void Kenwood::setCatZZFH_Command()
{

}



void Kenwood::readCatZZTX_Response()
{
    QString preamble = "ZZTX";
    QString terminator = ";";
    QString body = "0";
    if (radioStatus->getTxState())
    {
        body = "1";
    }

    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}

void Kenwood::setCatZZTX_Command()
{
    QString txStateStr = rxMessage.mid(4,1);
    if (txStateStr == "0")
    {
        radioStatus->setTxState(false);
    }
    else
    {
        radioStatus->setTxState(true);
    }

    emit radioStatusUpdated();
}


void Kenwood::readCatZZRM_Response()
{
    QString preamble = "ZZRM";
    QString terminator = ";";
    QString body;
    QString measurement = rxMessage.mid(4,1);
    if (measurement == "0")
    {
        // requesting signal strength


        //for (int i = 0; i < 17; i++)
        //{
        //    body.append(" ");
       // }

        body.append("0");  // command number
        body.append("-123.45 dbm");

        assembleMsgToSendToCat(preamble, body, terminator);
        sendCatTxMsg();

    }
    else
    {
        // just send spaces for other measurements


        for (int i = 0; i < 17; i++)
        {
            body.append(" ");
        }



        assembleMsgToSendToCat(preamble, body, terminator);
        sendCatTxMsg();
    }
}

void Kenwood::setCatZZRM_Command()
{

}

void Kenwood::readCatAG_Response(int rxNum)
{

    QString preamble = "AG";
    QString terminator = ";";
    QString body = QString::number(radioStatus->getVolumeLevel(rxNum));
    QString rxNumStr = QString::number(radioStatus->getRxNum());
    while (body.size() < 3)
    {
        body.prepend('0');
    }

    body = rxNumStr + body;

    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}

void Kenwood::readCatPS_Response()
{

    QString preamble = "PS";
    QString terminator = ";";
    QString body = "1"; //power on

    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}


void Kenwood::readCatFW_Response()
{
    QString preamble = "FW";
    QString terminator = ";";
    QString body = "0000"; //USB Normal

    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}


void Kenwood::readCatFV_Response()
{
    QString preamble = "FV";
    QString terminator = ";";
    QString body = "1.00";

    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}

void Kenwood::readCatID_Response()
{
    QString preamble = "ID";
    QString terminator = ";";


    assembleMsgToSendToCat(preamble, kenwoodYaesuId, terminator);
    sendCatTxMsg();
}

void Kenwood::readCatRT_Response()
{
    QString preamble = "RT";
    QString terminator = ";";

    QString body;
    if (radioStatus->getRitState())
    {
        body = "1";
    }
    else
    {
        body = "0";
    }

    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}

void Kenwood::readCatIF_Response()
{
    if (modelName == TS2000)
    {
        TS2000_IF_Response();

    }
    else if (modelName == TS590S)
    {
        TS590S_IF_Response();
    }
    else if (modelName == TS890S)
    {
        // use TS590S response for now
        TS590S_IF_Response();
    }
    else if (modelName == TS480)
    {

        TS480_IF_Response();
    }
    else if (modelName == QRPLABS)
    {
        // use TS480 respons for now
        TS480_IF_Response();
    }
    else if (modelName == THETIS)
    {
        // use TS2000 respons for now
        THETIS_IF_Response();
    }

}


void Kenwood::TS2000_IF_Response()
{

    QString preamble = "IF";
    QString terminator = ";";

    QString freq;                   // P1
    getFreqToSendToCat(freq, radioStatus->VFOA);

    QString stepSize = "0000"; // P2

    QString ritFreq;        // P3
    getRitFreqToSendToCat(ritFreq);

    QString ritStatus;  //P4
    if (radioStatus->getRitState())
    {
        ritStatus = "1";
    }
    else
    {
        ritStatus = "0";
    }

    QString xitStatus = "0"; // P5

    QString memBankNum = " "; //P6

    QString channelNum = "00"; // P7

    QString TxStatus = "0"; // P8
    if (radioStatus->getTxState())
    {
        TxStatus = "1";
    }

    QString mode = getStatusOfMode(); // P9


    QString vfo;

    if (radioStatus->getVfo())
    {
        vfo = "0"; // VFOA P10
    }
    else
    {
        vfo = "1";
    }

    QString scan = "0"; // Off P11

    QString simplex = "0"; // P12

    QString toneOnOff = "0"; // P13

    QString toneNum = "00"; // P14

    QString shiftStatus = "0"; //P15

    QString msgBody = freq + stepSize + ritFreq + ritStatus
                      + xitStatus + memBankNum + channelNum
                      + TxStatus + mode + vfo + scan + simplex
                     + toneOnOff + toneNum + shiftStatus;

    assembleMsgToSendToCat(preamble, msgBody, terminator);
    sendCatTxMsg();

}


void Kenwood::TS590S_IF_Response()
{

    QString preamble = "IF";
    QString terminator = ";";

    QString freq;                   // P1
    getFreqToSendToCat(freq, radioStatus->VFOA);

    QString stepSize = "     "; // P2

    QString ritFreq;        // P3
    getRitFreqToSendToCat(ritFreq);

    QString ritStatus;  //P4
    if (radioStatus->getRitState())
    {
        ritStatus = "1";
    }
    else
    {
        ritStatus = "0";
    }

    QString xitStatus = "0"; // P5

    QString memBankNum = " "; //P6

    QString channelNum = "00"; // P7

    QString TxStatus;  // P8
    if (radioStatus->getTxState())
    {
        TxStatus = "1";
    }
    else
    {
       TxStatus = "0";
    }

    QString mode = getStatusOfMode(); // P9

    QString vfo = "0"; // VFOA P10

    QString scan = "0"; // Off P11

    QString simplex = "0"; // P12

    QString toneOnOff = "0"; // P13

    QString toneNum = "00"; // P14

    QString shiftStatus = "0"; //P15

    QString msgBody = freq + stepSize + ritFreq + ritStatus
                      + xitStatus + memBankNum + channelNum
                      + TxStatus + mode + vfo + scan + simplex
                     + toneOnOff + toneNum + shiftStatus;

    assembleMsgToSendToCat(preamble, msgBody, terminator);
    sendCatTxMsg();

}


void Kenwood::TS480_IF_Response()
{
    QString preamble = "IF";
    QString terminator = ";";

    QString freq;                   // P1
    getFreqToSendToCat(freq, radioStatus->VFOA);

    QString stepSize = "     "; // P2

    QString ritFreq;        // P3
    getRitFreqToSendToCat(ritFreq);

    QString ritStatus;  //P4
    if (radioStatus->getRitState())
    {
        ritStatus = "1";
    }
    else
    {
        ritStatus = "0";
    }

    QString xitStatus = "0"; // P5

    QString memBankNum = " "; //P6

    QString channelNum = "00"; // P7

    QString TxStatus;  // P8
    if (radioStatus->getTxState())
    {
        TxStatus = "1";
    }
    else
    {
        TxStatus = "0";
    }

    QString mode = getStatusOfMode(); // P9

    QString vfo = "0"; // VFOA P10

    QString scan = "0"; // Off P11

    QString simplex = "0"; // P12

    QString toneOnOff = "0"; // P13

    QString toneNum = "00"; // P14

    QString shiftStatus = "0"; //P15

    QString msgBody = freq + stepSize + ritFreq + ritStatus
                      + xitStatus + memBankNum + channelNum
                      + TxStatus + mode + vfo + scan + simplex
                      + toneOnOff + toneNum + shiftStatus;

    assembleMsgToSendToCat(preamble, msgBody, terminator);
    sendCatTxMsg();


}


void Kenwood::THETIS_IF_Response()
{

    QString preamble = "IF";
    QString terminator = ";";

    QString freq;                   // P1
    getFreqToSendToCat(freq, radioStatus->VFOA);

    QString stepSize = "0000"; // P2

    QString ritFreq;        // P3
    getRitFreqToSendToCat(ritFreq);

    QString ritStatus;  //P4
    if (radioStatus->getRitState())
    {
        ritStatus = "1";
    }
    else
    {
        ritStatus = "0";
    }

    QString xitStatus = "0"; // P5

    QString channelBankNum1 = "0"; //P6 Not used

    QString channelNum = "00"; // P7 not used

    QString TxStatus = "0"; // P8
    if (radioStatus->getTxState())
    {
        TxStatus = "1";
    }

    QString mode = getStatusOfMode(); // P9

    QString splitStatus = "0";   // P10 always 0

    QString scanStatus = "0";   // P11 not implemented defaults to 0

    QString vfo;                // P12

    if (radioStatus->getVfo())
    {
        vfo = "0"; // VFOA P10
    }
    else
    {
        vfo = "1";
    }

    QString ctcssTone = "0"; // P13 not used

    QString moreTone = "00"; // P14 not used

    QString shiftStatus = "0"; // P15 not used




    QString msgBody = freq + stepSize + ritFreq + ritStatus
                      + xitStatus + channelBankNum1 + channelNum
                      + TxStatus + mode + splitStatus + scanStatus
                      + vfo + ctcssTone + moreTone + shiftStatus;

    assembleMsgToSendToCat(preamble, msgBody, terminator);
    sendCatTxMsg();

}


void Kenwood::setFACommand()
{

    QStringList sl = rxMessage.split('A');
    if (sl.size() == 2)
    {
        QString freq = sl[1].remove(';');
        if (freq.size() == 11)
        {
            freq.remove( QRegularExpression("^[0]*") ); // remove leading zeros
            //radioStatus->setCurrentFreqA(freq);
            radioStatus->setFreqA(freq.toULong());
            emit radioStatusUpdated();
        }
    }
}

void Kenwood::setFBCommand()
{

    QStringList sl = rxMessage.split('B');
    if (sl.size() == 2)
    {
        QString freq = sl[1].remove(';');
        if (freq.size() == 11)
        {
            freq.remove( QRegularExpression("^[0]*") ); // remove leading zeros
            //radioStatus->setCurrentFreqB(freq);
            radioStatus->setFreqB(freq.toULong());
            emit radioStatusUpdated();
        }
    }
}

void Kenwood::setAGCommand(int rxNum)
{
     Q_UNUSED(rxNum)

    // hamlib doesn't send the main/sub rx number!
    // that has now been fixed, so need to restore supporting rxNum!!!!

    if (rxMessage.size() == 6 )
    {
        bool ok = false;
        int volVal = rxMessage.mid(2, 3).toInt(&ok);
        if (ok)
        {
           radioStatus->setVolumeLevel(volVal, 0); // force main
           emit radioStatusUpdated();
        }

    }

}


void Kenwood::setRTCommand()
{
    if (rxMessage.size() == 4)
    {
        QString state = rxMessage.mid(2,1);
        if (state == "1")
        {
            radioStatus->setRitState(true);
        }
        else
        {
            radioStatus->setRitState(false);
        }
        emit radioStatusUpdated();
    }
}

void Kenwood::setRIT_Up()
{

    if (rxMessage.size() == 3)
    {
        int f = radioStatus->getRitFreq() + 1;
        if (f > maxRitFreq)
        {
            return;
        }

        radioStatus->setRitFreq(f);
        emit radioStatusUpdated();

    }
    else if (rxMessage.size() == 8)
    {
        bool ok;
        QString ritFreqStr = rxMessage.mid(2,5);
        int f = ritFreqStr.toInt(&ok);
        if (ok)
        {

            if (f > maxRitFreq)
            {
                return;
            }
            radioStatus->setRitFreq(f);
            emit radioStatusUpdated();
        }


    }




}


void Kenwood::setRIT_Down()
{
    if (rxMessage.size() == 3)
    {
            int f = radioStatus->getRitFreq() - 1;
            if (f < maxRitFreq)
            {
                return;
            }

            radioStatus->setRitFreq(f);
            emit radioStatusUpdated();
    }
    else if (rxMessage.size() == 8)
    {
        bool ok;
        QString ritFreqStr = rxMessage.mid(2,5);
        int f = ritFreqStr.toInt(&ok);
        if (ok)
        {

            if (f > maxRitFreq)   // value is positve at this point
            {
                return;
            }
            radioStatus->setRitFreq(f * -1);  // now make it negative
            emit radioStatusUpdated();
        }


    }

}


void Kenwood::readCatPB1_TS890S_Response()
{

}

void Kenwood::readCatPB_TS590_Response()
{

}

void Kenwood::setCatPB1_TS890S_Command(QString subCmd)
{
    QChar chanNum = subCmd[0];
    QChar voiceCmd = subCmd[1];

    if (voiceCmd  == '0')
    {
        // stop
        voiceMemoryStop('0');   // should this be '0'?
    }
    else if (voiceCmd  == '1')
    {
         // Being playback
        if (chanNum  >= '1' && chanNum  <= '6')   // TS890S has 6 messages
        {
            voiceMemoryStart(chanNum );
        }
    }
}


void Kenwood::setCatPB_TS590_Command(QChar subCmd)
{
    if (subCmd == '0')
    {
        // stop command
        //radioStatus->setVoiceMemNum(subCmd);
        voiceMemoryStop(subCmd);

    }
    else if (subCmd >= '1' && subCmd <= '4')  // TS590 only has 4 channels
    {
         //radioStatus->setVoiceMemNum(subCmd);
         voiceMemoryStart(subCmd);
    }
}

void Kenwood::voiceMemoryStop(QChar subCommandNum)
{
    voiceMsgTimer->stop();
    radioStatus->setVoiceMemNum(subCommandNum);
    radioStatus->setTxState(false);

    emit radioStatusUpdated();
}

void Kenwood::voiceMemoryStart(QChar subCommandNum)
{
    radioStatus->setVoiceMemNum(subCommandNum);
    radioStatus->setTxState(true);

    voiceMsgTimer->start(10 * 1000);
    emit radioStatusUpdated();
}

void Kenwood::onVmsgTimeout()
{
    voiceMsgTimer->stop();
    radioStatus->setTxState(false);
    radioStatus->setVoiceMemNum('0');
    emit radioStatusUpdated();
}

void Kenwood::catSetSendMorse(QByteArray msg)
{
    radioStatus->setMorseText(msg);
    radioStatus->setTxState(true);
    cwMsgTimer->start(25 * 1000);
    readCatKY_Response();
    emit radioStatusUpdated();
}

void Kenwood::catSetStopMorse()
{

    stopMorse();
    readCatKY_Response();
}

void Kenwood::onCwMsgTimeout()
{
    stopMorse();

}




void Kenwood::stopMorse()
{
    cwMsgTimer->stop();
    radioStatus->setTxState(false);
    radioStatus->clearMorseText();
    emit radioStatusUpdated();
}

void Kenwood::readCatKY_Response()
{
    QString preamble = "KY";
    QString terminator = ";";
    QString msg = "0";      // we are not handling a buffer...

    assembleMsgToSendToCat(preamble, msg, terminator);
    sendCatTxMsg();
}

void Kenwood::setRCCommand()
{
    if (rxMessage.size() == 3)
    {
        radioStatus->setRitFreq(0);
    }
    emit radioStatusUpdated();
}

void Kenwood::setMDCommand()
{
    if (rxMessage.size() == 4)
    {
        if (radioStatus->getVfo())
        {

            radioStatus->setModeA(convertModetoQString(rxMessage.mid(2,1)));
        }
        else
        {

            radioStatus->setModeB(convertModetoQString(rxMessage.mid(2,1)));
        }
        emit radioStatusUpdated();
    }
}

void Kenwood::sendCatTxMsg()
{
    QByteArray msg = catTxMessage.toLocal8Bit();
    sendCatMsg(msg);
}





QString Kenwood::convertModetoQString(QString mode)
{
    switch(mode.toInt())
    {
        case 1:
            return (*supportedModes)[0];

        case 2:
            return (*supportedModes)[1];

        case 3:
            return (*supportedModes)[2];

        case 4:
            return (*supportedModes)[3];

        case 5:
            return (*supportedModes)[4];

        case 6:
            return (*supportedModes)[5];

        case 7:
            return (*supportedModes)[6];

        case 9:
            return (*supportedModes)[7];

        default:
            return "";


    }


}


QString Kenwood::convertQStringtoModeCode(QString m)
{
    if (modeStrToModeTable.contains(m))
    {

        return modeStrToModeTable.value(m);
    }
    else
    {
        return "error";
    }
}
