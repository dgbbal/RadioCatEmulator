#ifndef UTILS_H
#define UTILS_H

#include <QByteArray>

void to_bcd(QByteArray& bcd_data, quint32 freq, int bcd_len);

quint32 from_bcd(QByteArray& bcd_data, int bcd_len);

unsigned char *to_bcd_be(unsigned char bcd_data[],
                                    unsigned long long freq,
                                    unsigned bcd_len);


#endif // UTILS_H
