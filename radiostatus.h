#ifndef RADIOSTATUS_H
#define RADIOSTATUS_H

#include <QObject>

class RadioStatus : public QObject
{
     Q_OBJECT

public:

    explicit RadioStatus(QObject *parent = nullptr);
    //enum modeCode {USB = 0, LSB, CW, CW_R, FM, AM, };
    //enum bandCode {_1_8MHz = 0, _3_5MHz, _7MHz, _10MHz, _14MHz, _21MHz, _24MHz, _28MHz, _50MHz, _70MHz, _144MHz, _432MHz, _1296MHz};
    enum modelCode {TS2000 = 0};





    const bool VFOA = true;
    const bool VFOB = false;
    const bool RIT_ON = true;
    const bool RIT_OFF = false;
    const bool DATAMODE_ON = true;
    const bool DATAMODE_OFF = false;
    quint32 getFreqA(){return freqA;}
    void setFreqA(quint32 f){freqA = f;}

    //QString getCurrentFreqA(){return currentFreqA;}
    //void setCurrentFreqA(QString f){currentFreqA = f;}

    QString getCurrentBandA(){return currentBandA;}
    void setCurrentBandA(QString f){currentBandA = f;}

    QString getModeA(){return modeA;}
    void setModeA(QString  m){modeA = m;}

    bool getDataModeA(){return dataModeA;}
    void setDataModeA(bool state){dataModeA = state; }

    quint32  getFreqB(){return freqB;}
    void setFreqB(quint32  f){freqB = f;}

    //QString getCurrentFreqB(){return currentFreqB;}
    //void setCurrentFreqB(QString f){currentFreqB = f;}

    QString getCurrentBandB(){return currentBandB;}
    void setCurrentBandB(QString f){currentBandB = f;}

    QString getModeB(){return modeB;}
    void setModeB(QString  m){modeB = m;}

    bool getDataModeB(){return dataModeB;}
    void setDataModeB(bool state){dataModeB = state; }


    bool getVfo(){return vfo;}
    void setVfo(bool vfo_){vfo = vfo_;}

    bool getSplit(){return split;}
    void setSplit(bool split_){split = split_;}

    int getRitFreq(){return ritFreq;}
    void setRitFreq(int f){ritFreq = f;}
    bool getRitState(){return ritStatus;}
    void setRitState(bool state){ritStatus = state;}

    //QString getMode(){return mode;}
    //void setMode(QString m){mode = m;}

    //QString getBand(){return band;}
    //void setBand(QString b){band = b;}

    int getRxNum(){return rxNum;}
    void setRxNum(int num){rxNum = num;}

    int getVolumeLevel(int rxNum){return volumeLevel[rxNum];}
    void setVolumeLevel(int volume, int rxNum){volumeLevel[rxNum] = volume;}

    bool getTxState(){return txState;}
    void setTxState(bool state){txState = state;}

    QChar getVoiceMemNum(){return voiceMemNum;}
    void setVoiceMemNum(QChar voiceMemNum_){voiceMemNum = voiceMemNum_;}

    QChar getCwMemNum(){return cwMemNum;}
    void setCwMemNum(QChar memNum){cwMemNum = memNum;}

    QByteArray& getMorseText(){return morseText;}
    void setMorseText(const QByteArray& morseText_){morseText = morseText_;}
    void clearMorseText(){morseText.clear();}

    bool getCatCtsFlag(){return catCtsTxFlag;}
    void setCatCtsFlag(bool catCtsTxFlag_){catCtsTxFlag = catCtsTxFlag_;}

    void clear();
private:

    quint32 freqA = 0;
    QString currentBandA;
    //QString currentFreqA = "0";
    QString modeA;       // 0 = none

    bool dataModeA = DATAMODE_OFF;

    quint32 freqB = 0;
    QString currentBandB;
    //QString currentFreqB = "0";
    QString modeB;
    bool dataModeB = DATAMODE_OFF;

    int rxNum = 0;      // 0 = main, 1 = sub

    bool vfo = true;    // true = vfoA, false = vfoB
    bool split = false; // true = split, false = no split
    int ritFreq = 0;
    bool ritStatus = false; // true = on, false = off

    int volumeLevel[2] = {0, 0}; //main rx/sub rx

    bool catCtsTxFlag = true;   // true CAT Tx control , false CTS Tx Control

    QChar voiceMemNum;
    QChar cwMemNum;

    bool txState = false;

    QByteArray morseText;

};;

#endif // RADIOSTATUS_H
