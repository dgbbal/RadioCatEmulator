#ifndef NEWYAESU_H
#define NEWYAESU_H

#include <QObject>
#include <QTimer>
#include "rigbase.h"
#include "rigcapabilities.h"
#include "rigfactory.h"
#include "radiostatus.h"
#include "configrotserial.h"

const QString FT991 = "Yaesu FT991";
const QString FTDX101 = "Yaesu FTDX101";

class NewYaesu : public RigBase
{
public:
    explicit NewYaesu(int id, QString modelName, RadioStatus *radioStatus_, ConfigRotSerial *configSerial_, QObject *parent = nullptr);
    virtual ~NewYaesu() override;
    //void waitForCatMessage() override;
    void handleCatMessage(QByteArray) override;
    static void register_rigs(RigFactory::Rigs*);
    void initRadio(Bandlist *bandList_, QStringList *supportedModes, QStringList *supportedModeCodes, QChar terminator_, QChar civAddress_, QString kenwoodYaesuId_ = "") override;

    void setMaxRitFreq(int f) override;
    void setMinRitFreq(int f) override;

private slots:
    void onVmsgTimeout();
    void onCwMsgTimeout();

private:

    RadioStatus* radioStatus;

    QString rxMessage;
    QString catTxMessage;
    QString modelName;
    int id;
    QChar terminator;
    QString kenwoodYaesuId;

    Bandlist* bandList;
    //QList<int> supportedBandStartFreq;

    QStringList *supportedModes;
    QStringList *supportedModeCodes;
    int maxRitFreq = +9999;
    int minRitFreq = -9999;

    QTimer* voiceMsgTimer;
    QTimer* cwMsgTimer;

    QMap<QString, QString> modeStrToModeTable;


    QString convertModetoQString(QString mode);
    QChar convertQStringtoModeCode(QString m);








    void getFreqToSendToCat(QString &f, bool vfo);
    void assembleMsgToSendToCat(QString &preamble, QString &msgBody, QString &terminator);
    void readCatFB_Response();
    void readCatFA_Response();
    void setFBCommand();
    void setFACommand();

    void getRitFreqToSendToCat(QString &f);
    QString getStatusOfMode();
    void readCatKP_Response();
    void readCatTX_Response();
    void readCatFT_Response();
    void readCatAI_Response();
    void readCatMD_Response();
    void setMDCommand(QString subCmd);
    void readCatSH_Response();
    void readCatAG_Response();
    void readCatSM_Response();
    void sendCatTxMsg();
    void readCatVS_Response();
    void readCatIF_Response();
    void setRIT_Up();
    void setRIT_Down();
    void readCatRT_Response();
    void setRTCommand();
    void readCatNA_Response();

    void setCatTx(QString subCmd);
    void readCatPB_Response();
    void setCatPBCommand(QString subCmd);
    void voiceMemoryStart(QChar subCommandNum);
    void voiceMemoryStop(QChar subCommandNum);
    void setCatPBCommand(QChar subCmd);
    void setKYCommand(QChar subCommandNum);

    void readCatID_Response();
    void setVSCommand(QString subCommandNum);
    void readCatOI_Response();
    void catSetSendMorse(QByteArray msg);
    void readCatPS_Response();
    void readCatEX_Response();
    void readCatKS_response();
};

#endif // NEWYAESU_H
