#ifndef RIGBASE_H
#define RIGBASE_H

#include <QObject>
#include "bandlist.h"
#include "configrotserial.h"
#include "MTrace.h"

class RigBase : public QObject
{
    Q_OBJECT
public:
    explicit RigBase(ConfigRotSerial *configSerial_, QObject *parent = nullptr);

    void waitForCatMessage();
    virtual void setMaxRitFreq(int f) = 0;
    virtual void setMinRitFreq(int f) = 0;

    virtual void initRadio( Bandlist* bandList_, QStringList *supportedModes_, QStringList *supportedModeCodes_, QChar terminator_, QChar civAddress_ = ' ', QString kenwoodYaesuId_ = "") = 0;
    virtual void handleCatMessage(QByteArray) = 0;


    void sendCatMsg(QByteArray &msg);
    QList<QByteArray> txQueue;
    QTimer *txQueueTimer;

public slots:

    void KenwoodWaitForCatMessage();
    void oldYaesuWaitForCatMessage();
    void IcomWaitForCatMessage();

signals:
    void catRxMsg(QByteArray);
    void catMessageReceived();

    void catTxMsg(QString);
    void radioStatusUpdated();

private:

    ConfigRotSerial *configSerial;
    QByteArray inMsg;
    QByteArray rxMessage;




};

#endif // RIGBASE_H
