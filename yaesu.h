#ifndef YAESU_H
#define YAESU_H

#include <QObject>

#include "rigbase.h"
#include "rigcapabilities.h"
#include "rigfactory.h"
#include "radiostatus.h"
#include "configrotserial.h"

const QString FT1000MP = "Yaesu FT1000MP";
const QString FT897 = "Yaesu FT897";
const QString FT857 = "Yaesu FT857";

class Yaesu : public RigBase
{
    Q_OBJECT
public:
    explicit Yaesu(int id, QString modelName, RadioStatus *radioStatus_, ConfigRotSerial *configSerial_, QObject *parent = nullptr);
    virtual ~Yaesu() override;
    //void waitForCatMessage() override;

    static void register_rigs(RigFactory::Rigs*);
    void initRadio(Bandlist *bandList_, QStringList *supportedModes_, QStringList *supportedModeCodes_, QChar terminator_, QChar civAddress_, QString kenwoodYaesuId_ = "") override;

    void setMaxRitFreq(int f) override;
    void setMinRitFreq(int f) override;

private slots:
    void onTXQueueTimeout();
private:

    RadioStatus* radioStatus;

    QByteArray rxMessage;
    QString catTxMessage;
    QString modelName;
    int id;

    char terminator;
    //QChar civAddress;

    Bandlist* bandList;
    //QList<int> supportedBandStartFreq;

    QStringList *supportedModes;
    QStringList *supportedModeCodes;

    QMap<QString, QString> modeStrToModeTable;

    int maxRitFreq;
    int minRitFreq;

    bool numStatusBytesFlag = false;   // false = 5 byte, true 6 byte


    void handleCatMessage(QByteArray msg) override;





    void catReadOperatingData();
    void catReadVfoFreqData();
    QString convertModetoQString_FT1000(QChar mode);
    QChar convertQStringtoModeCode_FT1000(QString m);


    QChar getStatusFlagByte1();
    QChar getStatusFlagByte2();
    QChar getStatusFlagByte3();
    QChar getStatusFlagByte4();
    QChar getStatusFlagByte5();
    QChar getStatusFlagByte6();


    void catReadFiveStatusBytes();
    void catReadSixStatusBytes();
    void catSetNumStatusBytes(QChar data);
    void convertYaesuBcd(QByteArray &bcdFreq, quint32 freq);
    void freqToByteStream(QByteArray &hexFreq, quint32 freq);
    void freqtoHex(QByteArray &hexFreq, const quint32 freq);
    void traceMsg(QString msg);
    void catSetOperatingFreqVFOA();
    void catSetOperatingFreqVFOB();
    void get16ByteOperatingData(QByteArray &data, bool vfo);
    void catReadSmeter(QChar cmd, QChar subCmd);
    void catSetVfo(QChar subCmd);
    void handleCatMessage_FT1000D(QByteArray msg);

    QChar convertQStringtoModeCode_FT897(QString m);
    QString convertModetoQString_FT897(QChar mode);
    void catReadMemLoc64();
    void catSetFreq(QByteArray msg);
    void handleCatMessage_FT857(QByteArray msg);
    void catReadMemLoc68(QChar cmd);
    void catReadMemLoc78(QChar cmd);
    void catReadFreqMode_FT857(QChar cmd);
    void catSetFreq_FT857(QByteArray msg);
    void catSetRit_FT857(QChar cmd);
    void catSetRitFreq_FT857(QByteArray msg);
    void catReadRxStatus_FT857(QChar cmd);
    void catSetRitFreq_FT1000();

    void getRitFreq_FT1000(QByteArray &clarOffset);
    void catReadTxStatus_FT857(QChar cmd);
    void catSetTx_FT857(bool state);
    void catSetMode_FT1000(QChar subCmd);
    void catSetMode_FT857();
    void sendAck();
};

#endif // YAESU_H
