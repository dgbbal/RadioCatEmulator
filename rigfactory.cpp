#include <QComboBox>
#include "rigfactory.h"
#include "kenwood.h"
#include "icom.h"
#include "yaesu.h"
#include "newyaesu.h"
#include "elecraft.h"

RigFactory::RigFactory(QObject *parent) : QObject(parent)
{
    Kenwood::register_rigs(&rigsList);
    Icom::register_rigs(&rigsList);
    Yaesu::register_rigs(&rigsList);
    NewYaesu::register_rigs(&rigsList);
    Elecraft::register_rigs(&rigsList);
}

RigFactory::~RigFactory()
{

}

RigFactory::Rigs* RigFactory::supported_rigs()
{
    return &rigsList;
}

RigBase* RigFactory::createRigs(int rigId, QString modelName, RadioStatus *radioStatus, ConfigRotSerial *configSerial)
{



    if (rigId >= KenwoodStartId && rigId <= KenwoodEndId)
    {
        return new Kenwood(rigId, modelName, radioStatus, configSerial, this);
    }
    else if (rigId >= IcomStartId && rigId <= IcomEndId)
    {
        return new Icom(rigId, modelName, radioStatus, configSerial, this);
    }
    else if (rigId >= YaesuStartId && rigId <= YaesuEndId)
    {
        return new Yaesu(rigId, modelName, radioStatus, configSerial, this);
    }
    else if (rigId >= NewYaesuStartId && rigId <= NewYaesuEndId)
    {
        return new NewYaesu(rigId, modelName, radioStatus, configSerial, this);
    }
    else if (rigId >= ElecraftStartId && rigId <= ElecraftEndId)
    {
        return new Elecraft(rigId, modelName, radioStatus, configSerial, this);
    }

    return nullptr;



}


void RigFactory::populateComboRigList(QComboBox* comBox )
{
    comBox->clear();
    comBox->addItem("");
    for (auto r = supported_rigs()->cbegin(); r != supported_rigs()->cend(); ++r)
    {
        QString rigText = r.key();
        comBox->addItem(rigText);


    }
}
