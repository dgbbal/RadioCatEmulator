/////////////////////////////////////////////////////////////////////////////
// $Id$
//
// PROJECT NAME 		VHF Contest Adjudication
//
// COPYRIGHT         (c) M. J. Goodey G0GJV, 2005 - 2008
//
/////////////////////////////////////////////////////////////////////////////

#ifndef MLogFileH
#define MLogFileH 
#include <QString>
#include <QMutexLocker>
#include <QTextStream>
//---------------------------------------------------------------------------

class CsGuard:public QMutexLocker
        #if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
        <QRecursiveMutex>
        #endif
{
   public:
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
    CsGuard(
        QRecursiveMutex *m_mutex
    ):QMutexLocker<QRecursiveMutex>(m_mutex)
#else
    CsGuard(
        QMutex *m_mutex
    ):QMutexLocker(m_mutex)
#endif


      {
      }

      ~CsGuard()
      {
      }
      static void ClearDown()
      {
      }
};


//---------------------------------------------------------------------------
class MLogFile
{
   private:
      QString fLogFileName;
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
    QRecursiveMutex m_mutex;
#else
    QMutex m_mutex;
#endif
   public:

#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
    MLogFile():m_mutex()
#else
    MLogFile():m_mutex(QMutex::Recursive)
#endif
      { }
      // CreateLogFile is called by the boot form at startup
      void createLogFile(const QString &path, const QString filePrefix, int keepDays );

      // Log and LogT are used to log data without and with a timestamp;
      QTextStream &log( );
      QTextStream & logT( );
      // Log logs a string with a time prefix hh:mm:ss
      QTextStream &log( const QString &s );
      void close( );

      // Generates a suffix of the form yyyyddmmhhmmss
      static QString generateLogFileName( const QString &prefix );
      static void tidyFiles( const QString &prefix, int keepDays );
      QString getTraceFileName()
      {
         return fLogFileName;
      }
};
//---------------------------------------------------------------------------
#endif
