#include "icom.h"
#include "utils.h"
#include "MTrace.h"

#include <QDebug>
#include <QSettings>
#include <QTimer>


Icom::Icom(int id_, QString modelName_, RadioStatus *radioStatus_, ConfigRotSerial *configSerial_, QObject *parent)  : RigBase(configSerial_, parent),
    radioStatus(radioStatus_),
    modelName(modelName_),
    id(id_)
{

}

Icom::~Icom()
{

}

void Icom::register_rigs(RigFactory::Rigs* rigsList)
{

    QSettings settings("./Radios/icom.ini", QSettings::IniFormat);

    QStringList availRadios = settings.childGroups();
    int numRadios = availRadios.count();

    for (int i = 0; i <numRadios; i++)
    {
        settings.beginGroup(availRadios[i]);
        QString rigManufacturer = settings.value("rigManufacturer", "").toString();
        QString rigName = settings.value("rigName", "").toString();
        QString rigModelName = rigManufacturer + " " + rigName;

        QString kenwoodYaesuId = "";

        int rigModelNumber = settings.value("rigModelNumber", 0).toInt();

        int volLevelMax = settings.value("maxVol", 0).toInt();
        int volLevelMin = settings.value("minVol", 255).toInt();

        int maxRitFreq = settings.value("maxRit", 9999).toInt();

        QByteArray termArray = settings.value("terminator", ' ').toByteArray();
        bool ok;
        int hex = termArray.toInt(&ok, 16);
        QChar terminator = QChar(hex);

        QByteArray civArray = settings.value("civ", ' ').toByteArray();
        hex = civArray.toInt(&ok, 16);
        QChar civAddress = QChar(hex);

        CATCAPTURE catCapMethodCode = RigCapabilities::getCatCaptureCode(settings.value("catCaptureMethod", "").toString().trimmed());

        QString supportedBands = settings.value("supportedBands", "").toString();
        QString supportedBandStartFreq = settings.value("supportedBandStartFreq", "0").toString();
        QString supportedModes = settings.value("supportedModes", "").toString();
        QString supportedModeCodes = settings.value("supportedModeCodes", "-1").toString();
        QString defaultSettings = settings.value("defaultSettings", "").toString();
        settings.endGroup();

        (*rigsList)[rigModelName] = RigCapabilities(rigManufacturer,
                                                    rigName,
                                                    rigModelName,
                                                    rigModelNumber,
                                                    civAddress,
                                                    kenwoodYaesuId,
                                                    volLevelMax,
                                                    volLevelMin,
                                                    maxRitFreq,
                                                    terminator,
                                                    supportedBands,
                                                    supportedBandStartFreq,
                                                    supportedModes,
                                                    supportedModeCodes,
                                                    defaultSettings,
                                                    catCapMethodCode);

    }

}


void Icom::initRadio( Bandlist* bandList_, QStringList *supportedModes_, QStringList *supportedModeCodes_, QChar terminator_, QChar civAddress_, QString kenwoodYaesuId_)
{

    Q_UNUSED(kenwoodYaesuId_)

    bandList = bandList_;
    supportedModes = supportedModes_;
    supportedModeCodes = supportedModeCodes_;





    terminator = terminator_.toLatin1();
    civAddress = civAddress_;
    voiceMsgTimer = new QTimer(this);
    connect(voiceMsgTimer, SIGNAL(timeout()), this, SLOT(onVmsgTimeout()));

    cwMsgTimer = new QTimer(this);
    connect(cwMsgTimer, SIGNAL(timeout()), this, SLOT(onCwMsgTimeout()));


    if ((*supportedModes).count() == (*supportedModeCodes).count())
    {
        for (int i = 0; i < (*supportedModes).count(); i++)
        {
            QByteArray modeCode;
            int m = (*supportedModeCodes)[i].toInt();
            modeCode += char(m);
            modeStrToModeTable.insert((*supportedModes)[i], modeCode);

        }
    }
    else
    {
        trace("Error - supportedModes length != supportedModeCodes length");
    }


}

void Icom::setMaxRitFreq(int f)
{
    maxRitFreq = f;
}

void Icom::setMinRitFreq(int f)
{
    minRitFreq = f;
}






void Icom::handleCatMessage(QByteArray msg)
{

    rxMessage = msg;
    qDebug() << "rxmesage = " << rxMessage;

    // get commands
    char commandNum = rxMessage.at(4);
    char subCommandNum = rxMessage.at(5);

    char subCommandNum1 = '\xff';
    controllerDefaultAddress = rxMessage.at(3);


    QByteArray cmn;
    cmn.append(commandNum);
    QByteArray scmn;
    scmn.append(subCommandNum);

    if (rxMessage.size() >= 7)
    {
        subCommandNum1 = rxMessage.at(6);
        QByteArray scmn1;
        scmn1 += subCommandNum1;
        trace("[handlCatMessage] cmdnum = " + cmn.toHex() + " subcmd = " + scmn.toHex() +  + " subcmd1 = " + scmn1.toHex());
    }
    else
    {
        trace("[handlCatMessage] cmdnum = " + cmn.toHex() + " subcmd = " + scmn.toHex());
    }



    qDebug() << "cmdnum = " << cmn.toHex() << "subcmd = " << scmn.toHex();

    if (commandNum == '\x03')        // command \x03
    {
        catReadFreqResponse(commandNum );
    }


    else if (commandNum == '\x04')
    {
        catReadMode(commandNum);
    }


    else if (commandNum == '\x05')          // command \x05
    {
        catSetFreq();
    }
    else if (commandNum == '\x06')
    {
        catSetMode(subCommandNum);
    }


    else if (commandNum == '\x07')          // command \x07
    {
        if (subCommandNum == '\x00' || subCommandNum == '\x01')
        {
            catSetVfo(commandNum);
        }


        else if (subCommandNum == '\xd0')
        {
            catSetMainBand();
        }
        else if (subCommandNum == '\xd2')
        {
            catReadMainBand(commandNum, subCommandNum);
        }
        else if (subCommandNum == '\xb0')
        {
            catExchangeVFOAandVFOB();
        }
    }


    else if (commandNum == '\x0f')      // command \x0f
    {
        if (subCommandNum == '\xfd')
        {
            catReadSplitState(commandNum);
        }

    }

    else if (commandNum == '\x14')          // command \x14
    {
        if (subCommandNum == '\01')
        {
            if (rxMessage.at(6) == '\xfd')
            {
                catReadVolume(commandNum, subCommandNum);

            }
            else
            {
                catSetVolume();
            }
        }
        if (subCommandNum == '\x09')
        {
            if (rxMessage.at(6) == '\xfd')
            {
                catReadCwPitch(commandNum, subCommandNum);
            }
        }
        if (subCommandNum == '\x0c')
        {
            if (rxMessage.at(6) == '\xfd')
            {
                catReadKeySpeed(commandNum, subCommandNum);
            }
        }
    }


    else if (commandNum == '\x15')          // command \x15
    {
        if (subCommandNum == '\x02')
        {
            catReadSMeter(commandNum, subCommandNum);
        }
    }


    else if (commandNum == '\x16')          // command \x16
    {
        if (subCommandNum == '\x5a')
        {
            if (rxMessage.at(6) == '\xfd')
            {
               catReadSatMode(commandNum);
            }
            else
            {
                //catSetSatMode()
                // ack for now
                sendCatOK();
            }


        }
    }
    else if (commandNum == '\x17')
    {
        if (subCommandNum == '\xff')
        {
            catSetStopMorse();
        }
        else
        {
           QByteArray msg;

           for (int i = 5; rxMessage.at(i) != '\xfd' && i < MAX_CW_CHAR_COUNT + 1; i++)
           {
               msg += rxMessage.at(i);
           }

           catSetSendMorse(msg);
        }
    }


    else if (commandNum == '\x19')          // command \x19
    {
        if (subCommandNum == '\x00')
        {
            catReadTxRxID(commandNum);
        }
    }


    else if (commandNum == '\x1a')          // command \x1a
    {
        qDebug() << "cmd 1a" << "sub cmdNum = " << subCommandNum;
        if (subCommandNum == '\x03')
        {
            if (rxMessage.at(6) == '\xfd')
            {
                catReadFilterWidth(commandNum);
            }
        }
        else if (subCommandNum == '\x05')
        {
            if (rxMessage.at(7) == '\x67')
            {
                // set Function CW Normal side
                // ack for now
                //sendCatOK();

            }
            // ack for now
            sendCatOK();
        }
        else if (subCommandNum == '\x06')
        {
            if (rxMessage.at(6) == '\x00')
            {
                qDebug() << "cmd 1a 06";
                catSetDataMode();
            }
            if (rxMessage.at(6) == '\xfd')
            {
                catReadDataMode(commandNum);
            }
        }

    }


    else if (commandNum == '\x1c')          // command \x1c
    {
        if (subCommandNum == '\x00' && rxMessage.at(6) == '\xfd')
        {
            catReadTXStatus(commandNum, subCommandNum);
        }
        else if (subCommandNum == '\x00' && rxMessage.at(7) == '\xfd')
        {
            catSetTxStatus(commandNum, rxMessage.at(6));
        }
    }


    else if (commandNum == '\x21')      // command \x21
    {
        if (rxMessage.size() >= 7)
        {
            if (subCommandNum == '\00' && rxMessage.at(6) == '\xfd')
            {
                catReadRitFreq(commandNum, subCommandNum);
            }
            else if (subCommandNum == '\00' && rxMessage.at(6) != '\xfd')
            {
                catSetRitFreq(commandNum, subCommandNum);
            }
            else if (subCommandNum == '\x01' && rxMessage.at(6) == '\xfd')
            {
                catReadRitStatus(commandNum, subCommandNum);
            }
            else if (subCommandNum == '\x01' && rxMessage.at(6) != '\xfd')
            {
                catSetRitStatus(commandNum, subCommandNum);
            }
            else if (subCommandNum == '\x02' && rxMessage.at(6) == '\xfd')
            {
                catReadXitStatus(commandNum, subCommandNum);
            }
            else if (subCommandNum == '\x02' && rxMessage.at(6) != '\xfd')
            {
                catSetXitStatus(commandNum, subCommandNum);
            }
        }

    }


    else if (commandNum == '\x25')          // command \x25
    {
        if (rxMessage.at(6) == '\xfd')
        {
            catReadFreq_SelectedUnselectedVfo(commandNum, subCommandNum);

        }
        else
        {
            catSetFreq_SelectedUnselectedVfo(subCommandNum);
        }


    }
    else if (commandNum == '\x26')
    {
        if (rxMessage.at(6) == '\xfd')
        {
            catReadModeAndFilterWidth_SelectedUnselectedVfo(commandNum, subCommandNum);

        }
        else
        {
            catSetModeAndFilterWidth_SelectedUnselectedVfo(subCommandNum);
        }



    }
    else if (commandNum == '\x28')          // command \x28
    {
            //transmits TX Voice Memory
           catSetTXVoiceMem(commandNum, subCommandNum,  subCommandNum1);
    }



}



void Icom::catReadFreqResponse(QChar cmd)
{
   quint32 freq;
   QByteArray preAmble;
   getPreAmble(preAmble, cmd);

   QByteArray body;

   if (radioStatus->getVfo())
   {
       freq = radioStatus->getFreqA();
   }
   else
   {
      freq = radioStatus->getFreqB();
   }

   to_bcd(body, freq, 10);

   QByteArray msg;
   createMsg(msg, preAmble, body);
   sendCatMsg(msg);
}

void Icom::catReadFreq_SelectedUnselectedVfo(QChar cmd, QChar subCommandNum)
{
       quint32 freq;
       QByteArray preAmble;
       getPreAmble(preAmble, cmd);



       QByteArray body;


       if (subCommandNum == SELECTED_VFO)
       {
           preAmble.append(SELECTED_VFO.toLatin1());   // add selected byte

           if (radioStatus->getVfo())
           {
               freq = radioStatus->getFreqA();
           }
           else
           {
                freq = radioStatus->getFreqB();

           }
       }
       else
       {
           preAmble.append(UNSELECTED_VFO.toLatin1());    //add unselected byte
           if (!radioStatus->getVfo())
           {
               freq = radioStatus->getFreqB();
           }
           else
           {
                freq = radioStatus->getFreqA();
           }
       }

       to_bcd(body, freq, 10);

       QByteArray msg;
       createMsg(msg, preAmble, body);
       sendCatMsg(msg);
}

void Icom::catReadRitFreq(QChar commandNum, QChar subCommandNum)
{

    int f = radioStatus->getRitFreq();
    QByteArray bcdfreq = convertFreqToIcomRitData(f);

    QByteArray preAmble;
    getPreAmble(preAmble, commandNum);
    preAmble.append(subCommandNum.toLatin1());

    QByteArray msg;
    createMsg(msg, preAmble, bcdfreq);
    sendCatMsg(msg);


}


QByteArray Icom::convertFreqToIcomRitData(int freq)
{
    QByteArray ritData;

    // Ensure freq is within the allowed range (-9999 to +9999)
    if (freq < -9999 || freq > 9999)
    {
        qWarning() << "Frequency out of range!";
        return QByteArray();
    }

    // Handle the absolute value of the frequency for encoding
    int absFreq = qAbs(freq);

    // Extract the digits
    int onesHz = absFreq % 10;          // 1 Hz digit
    int tensHz = (absFreq / 10) % 10;   // 10 Hz digit
    int hundredsHz = (absFreq / 100) % 10; // 100 Hz digit
    int onesKHz = (absFreq / 1000) % 10;  // 1 kHz digit

    // First byte: 10 Hz (high nibble) and 1 Hz (low nibble)
    char firstByte = (tensHz << 4) | onesHz;

    // Second byte: 1 kHz (high nibble) and 100 Hz (low nibble)
    char secondByte = (onesKHz << 4) | hundredsHz;

    // Third byte: 00 for positive, 01 for negative
    char thirdByte = (freq >= 0) ? 0x00 : 0x01;

    // Append the bytes to the QByteArray
    ritData += firstByte;
    ritData += secondByte;
    ritData += thirdByte;

    return ritData;
}


void Icom::catSetRitFreq(QChar cmd, QChar subCommandNum)
{
    Q_UNUSED(cmd)
    Q_UNUSED(subCommandNum)
    QByteArray bcdFreq;
    bcdFreq += rxMessage.at(6);
    bcdFreq += rxMessage.at(7);

    quint32 ritFreq = from_bcd(bcdFreq, 4);

    int ritFreqInt = static_cast<int>(ritFreq);
    if (rxMessage.at(8) == '\x01')
    {
        ritFreqInt = ritFreqInt * -1;
    }

    radioStatus->setRitFreq(ritFreqInt);
    sendCatOK();
    emit radioStatusUpdated();

}

void Icom::catReadVfoFreq(QChar cmd, QChar subCommandNum)
{


    quint32 freq;
    QByteArray preAmble;
    getPreAmble(preAmble, cmd);
    preAmble.append(subCommandNum.toLatin1());

    QByteArray body;

    if( subCommandNum == '\x00')
    {
        freq = radioStatus->getFreqA();
    }
    else
    {
        freq = radioStatus->getFreqB();
    }

    to_bcd(body, freq, 10);
    QByteArray msg;
    createMsg(msg, preAmble, body);
    sendCatMsg(msg);


}

void Icom::catReadMode(QChar cmd)
{
    QByteArray preAmble;
    getPreAmble(preAmble, cmd);

    QByteArray body;
    if (radioStatus->getVfo())
    {
       body += convertQStringtoModeCode(radioStatus->getModeA());
    }
    else
    {
       body += convertQStringtoModeCode(radioStatus->getModeB());
    }

    // add filter
    body += '\x02';

    QByteArray msg;
    createMsg(msg, preAmble, body);
    sendCatMsg(msg);

}

void Icom::catReadModeAndFilterWidth_SelectedUnselectedVfo(QChar cmd, QChar subCommandNum)
{
    QByteArray preAmble;
    getPreAmble(preAmble, cmd);
    QByteArray body;

    QString mode;

    // subcommand here is the selected/unselected VFO command

    // append mode and digital on/off
    if( subCommandNum == SELECTED_VFO)
    {
        preAmble.append(SELECTED_VFO.toLatin1());
        // read selected VFO
        if (radioStatus->getVfo())
        {
            body += convertQStringtoModeCode(radioStatus->getModeA());
            // is digital mode on/off for this vfo
            if (radioStatus->getDataModeA())
            {
                body += DATAMODE_ON;
            }
            else
            {
                body += DATAMODE_OFF;
            }

        }
        else
        {
            body += convertQStringtoModeCode(radioStatus->getModeB());
            if (radioStatus->getDataModeB())
            {
                body += DATAMODE_ON;
            }
            else
            {
                body += DATAMODE_OFF;
            }
        }
    }
    else
    {
        preAmble.append(UNSELECTED_VFO.toLatin1());

        // read unselected VFO
        if (!radioStatus->getVfo())
        {
            body += convertQStringtoModeCode(radioStatus->getModeB());
            if (radioStatus->getDataModeB())
            {
                body += DATAMODE_ON;
            }
            else
            {
                body += DATAMODE_OFF;
            }

        }
        else
        {

            body += convertQStringtoModeCode(radioStatus->getModeA());
            if (radioStatus->getDataModeA())
            {
                body += DATAMODE_ON;
            }
            else
            {
                body += DATAMODE_OFF;
            }


        }
    }


    // append filter setting
    body += '\x02';

    QByteArray msg;
    createMsg(msg, preAmble, body);
    sendCatMsg(msg);


}


void Icom::catReadSplitState(QChar cmd)
{

    QByteArray preAmble;
    getPreAmble(preAmble, cmd);

    QByteArray body;
    // this might not be correct..
    if (radioStatus->getVfo())
    {
        body += '\x00';
    }
    else
    {
        body += '\x01';
    }


    QByteArray msg;
    createMsg(msg, preAmble, body);
    sendCatMsg(msg);


}


void Icom::catReadCwPitch(QChar cmd, QChar subCommandNum)
{
    QByteArray preAmble;
    getPreAmble(preAmble, cmd);
    preAmble.append(subCommandNum.toLatin1());

    QByteArray body;
    body += '\x28';
    body += '\x01';

    QByteArray msg;
    createMsg(msg, preAmble, body);
    sendCatMsg(msg);
}

void Icom::catReadKeySpeed(QChar cmd, QChar subCommandNum)
{

    QByteArray preAmble;
    getPreAmble(preAmble, cmd);
    preAmble.append(subCommandNum.toLatin1());

    QByteArray body;
    body += '\27';
    body += '\x01';

    QByteArray msg;
    createMsg(msg, preAmble, body);
    sendCatMsg(msg);
}


void Icom::catSetMainBand()
{
    sendCatOK();
}



void Icom::catSetMode(QChar subCommandNum)
{
    if (radioStatus->getVfo())
    {
        radioStatus->setModeA(convertModetoQString(subCommandNum));
    }
    else
    {
        radioStatus->setModeA(convertModetoQString(subCommandNum));
    }

    sendCatOK();
    emit radioStatusUpdated();
}

void Icom::catSetModeAndFilterWidth_SelectedUnselectedVfo(QChar subCommandNum)
{
        char m = rxMessage.at(6);
        if (subCommandNum == SELECTED_VFO)
        {
            if (radioStatus->getVfo())
            {
                radioStatus->setModeA(convertModetoQString(m));
            }
            else
            {
                radioStatus->setModeB(convertModetoQString(m));
            }

        }
        else
        {
            if (!radioStatus->getVfo())
            {
                radioStatus->setModeB(convertModetoQString(m));
            }
            else
            {
                radioStatus->setModeA(convertModetoQString(m));
            }
        }

        sendCatOK();
        emit radioStatusUpdated();
}


void Icom::catSetRitStatus(QChar commandNum, QChar subCommandNum)
{
    Q_UNUSED(commandNum)
    Q_UNUSED(subCommandNum)

    if (rxMessage.at(6) == '\x00')
    {
        radioStatus->setRitState(radioStatus->RIT_OFF);
    }
    else
    {
        radioStatus->setRitState(radioStatus->RIT_ON);
    }
    sendCatOK();
    emit radioStatusUpdated();
}



void Icom::catSetVfo(QChar commandNum)
{
    if (commandNum == '\x00')
    {
        radioStatus->setVfo(true);
    }
    else
    {
        radioStatus->setVfo(false);
    }
    sendCatOK();
    emit radioStatusUpdated();
}

void Icom::catExchangeVFOAandVFOB()
{
    if (radioStatus->getVfo())
    {
        radioStatus->setVfo(false);
    }
    else
    {
       radioStatus->setVfo(false);
    }

    sendCatOK();

    emit radioStatusUpdated();

}


void Icom::catSetFreq()
{
    // extract the bcd freq
    QByteArray bcdFreq;
    for (int i = 5; rxMessage[i] != '\xfd'; i++)
    {
        bcdFreq += rxMessage[i];
    }

    quint32 f = from_bcd(bcdFreq, bcdFreq.size() *2);

    if (radioStatus->getVfo())
    {
        radioStatus->setFreqA(f);
    }
    else
    {
        radioStatus->setFreqB(f);
    }

    sendCatOK();
    emit radioStatusUpdated();
}



void Icom::catSetFreq_SelectedUnselectedVfo(QChar subCommandNum)
{
    Q_UNUSED(subCommandNum)

    // extract the bcd freq
    QByteArray bcdFreq;
    for (int i = 6; rxMessage[i] != '\xfd'; i++)
    {
        bcdFreq += rxMessage[i];
    }

    quint32 f = from_bcd(bcdFreq, bcdFreq.size() *2);

    if (rxMessage[5] == '\x00') // check set selected VFO
    {
        if (radioStatus->getVfo())
        {
            radioStatus->setFreqA(f);
        }
        else
        {
            radioStatus->setFreqB(f);
        }
    }
    else
    {
        if (!radioStatus->getVfo())
        {
            radioStatus->setFreqB(f);
        }
        else
        {
            radioStatus->setFreqA(f);
        }
    }

    sendCatOK();
    emit radioStatusUpdated();
}

void Icom::catSetTXVoiceMem(QChar cmd, QChar subCommandNum, QChar subCommandNum1)
{
    // Data for voice mem seems to be in second data byte..
    Q_UNUSED(cmd)
    Q_UNUSED(subCommandNum)
    if (subCommandNum1 == '\x00')
    {
        // stop command
        radioStatus->setVoiceMemNum(subCommandNum1);
        voiceMemoryStop(subCommandNum1);

    }
    else if (subCommandNum1.toLatin1() >= '\x01' && subCommandNum1.toLatin1() <= '\x08')
    {
         radioStatus->setVoiceMemNum(subCommandNum1);
         voiceMemoryStart(subCommandNum1);
    }
}


void Icom::catSetStopMorse()
{

    cwMsgTimer->stop();
    radioStatus->clearMorseText();
    radioStatus->setTxState(false);
    sendCatOK();
    emit radioStatusUpdated();
}

void Icom::onCwMsgTimeout()
{
    voiceMsgTimer->stop();
    radioStatus->setTxState(false);
    radioStatus->clearMorseText();
    emit radioStatusUpdated();
}


void Icom::catSetSendMorse(QByteArray msg)
{
    radioStatus->setMorseText(msg);
    radioStatus->setTxState(true);
    cwMsgTimer->start(10 * 1000);
    sendCatOK();
    emit radioStatusUpdated();
}

void Icom::catReadMainBand(QChar cmd, QChar subCommandNum)
{
     Q_UNUSED(subCommandNum)

    QByteArray preAmble;
    getPreAmble(preAmble, cmd);
    preAmble += '\xd2';

    QByteArray body;
    if (radioStatus->getVfo())
    {
       body += '\x00';
    }
    else
    {
       body += '\x01';
    }

    QByteArray msg;
    createMsg(msg, preAmble, body);
    sendCatMsg(msg);


}


void Icom::catReadFilterWidth(QChar cmd)
{

    QByteArray preAmble;
    getPreAmble(preAmble, cmd);

    QByteArray body;
    body += '\20';
    QByteArray msg;
    createMsg(msg, preAmble, body);
    sendCatMsg(msg);

}

void Icom::catReadSMeter(QChar cmd, QChar subCmd)
{
    QByteArray preAmble;
    getPreAmble(preAmble, cmd);
    preAmble.append(subCmd.toLatin1());
    QByteArray body;


    QByteArray msg;
    createMsg(msg, preAmble, body);
    sendCatMsg(msg);

}

void Icom::catReadVolume(QChar cmd, QChar subCmd)
{
    QByteArray preAmble;
    getPreAmble(preAmble, cmd);
    preAmble.append(subCmd.toLatin1());
    QByteArray body;
    body += '\x00';
    body += '\x0';

    //int v = radioStatus->getVolumeLevel(0);


    QByteArray msg;
    createMsg(msg, preAmble, body);
    sendCatMsg(msg);



}





void Icom::catReadSatMode(QChar cmd)
{
    QByteArray preAmble;
    getPreAmble(preAmble, cmd);

    QByteArray body;

    body += '\x00';


    QByteArray msg;
    createMsg(msg, preAmble, body);
    sendCatMsg(msg);
}


void Icom::catReadDataMode(QChar cmd)
{
    QByteArray preAmble;
    getPreAmble(preAmble, cmd);

    QByteArray body;

    body += '\x00';
    body += '\x01';
    QByteArray msg;
    createMsg(msg, preAmble, body);
    sendCatMsg(msg);

}

void Icom::catReadTxRxID(QChar cmd)
{
    QByteArray preAmble;
    getPreAmble(preAmble, cmd);

    QByteArray body;
    body += civAddress.toLatin1();

    QByteArray msg;
    createMsg(msg, preAmble, body);
    sendCatMsg(msg);
}


void Icom::catReadRitStatus(QChar cmd, QChar subCommandNum)
{
    QByteArray preAmble;
    getPreAmble(preAmble, cmd);
    preAmble.append(subCommandNum.toLatin1());

    QByteArray body;
    if (radioStatus->getRitState())
    {
        body += '\x01';
    }
    else
    {
        body += '\x00';

    }

    QByteArray msg;
    createMsg(msg, preAmble, body);
    sendCatMsg(msg);

}

void Icom::catReadXitStatus(QChar cmd, QChar subCommandNum)
{
    QByteArray preAmble;
    getPreAmble(preAmble, cmd);
    preAmble.append(subCommandNum.toLatin1());

    QByteArray body;
    // XIT status off
    body += '\x00';

    QByteArray msg;
    createMsg(msg, preAmble, body);
    sendCatMsg(msg);

}

void Icom::catSetXitStatus(QChar cmd, QChar subCommandNum)
{
     Q_UNUSED(subCommandNum)
      Q_UNUSED(cmd)


}

void Icom::catReadTXStatus(QChar cmd, QChar subCommandNum)
{
    QByteArray preAmble;
    getPreAmble(preAmble, cmd);
    preAmble.append(subCommandNum.toLatin1());

    QByteArray body;
    if (radioStatus->getTxState())
    {
        body += '\x01';
    }
    else
    {
       body += '\x00';
    }

    QByteArray msg;
    createMsg(msg, preAmble, body);
    sendCatMsg(msg);

}

void Icom::catSetTxStatus(QChar cmd, QChar state)
{
    Q_UNUSED(cmd)

    if (state == '\x00')
    {
        radioStatus->setTxState(false);
        qDebug() << "set TX Off";
    }
    else if (state == '\x01')
    {
        radioStatus->setTxState(true);
        qDebug() << "set TX On";
    }

    sendCatOK();
    emit radioStatusUpdated();
}

void Icom::catSetVolume()
{

}

void Icom::catSetDataMode()
{
    // dummy for now
    qDebug() << "in setDataMode";
    sendCatOK();
    qDebug() << "sent setData OK";
}



void Icom::getPreAmble(QByteArray& preAmble, QChar cmd)
{
    preAmble += '\xfe';
    preAmble += '\xfe';
    preAmble += controllerDefaultAddress.toLatin1();
    preAmble += civAddress.toLatin1();
    preAmble.append(cmd.toLatin1());
}

void Icom::createMsg(QByteArray& msg, QByteArray& preAmble, QByteArray& body)
{
    msg += preAmble;
    msg += body;
    msg += '\xfd';
}




void Icom::sendCatOK()
{
    QByteArray msg;
    msg += '\xfe';
    msg += '\xfe';
    msg += '\xe0';
    msg += civAddress.toLatin1();
    msg += '\xfb';
    msg += '\xfd';

    sendCatMsg(msg);
}

void Icom::getFreqToSendToCat(QByteArray &f)
{
    quint32 freq;
    if (radioStatus->getVfo())
    {
        freq = radioStatus->getFreqA();
    }
    else
    {
        freq = radioStatus->getFreqB();
    }

    to_bcd(f, freq, 10);

}

void Icom::voiceMemoryStop(QChar subCommandNum)
{
    voiceMsgTimer->stop();
    radioStatus->setVoiceMemNum(subCommandNum);
    radioStatus->setTxState(false);
    sendCatOK();
    emit radioStatusUpdated();
}

void Icom::voiceMemoryStart(QChar subCommandNum)
{
    radioStatus->setVoiceMemNum(subCommandNum);
    radioStatus->setTxState(true);
    sendCatOK();
    voiceMsgTimer->start(10 * 1000);
    emit radioStatusUpdated();
}

void Icom::onVmsgTimeout()
{
    voiceMsgTimer->stop();
    radioStatus->setTxState(false);
    radioStatus->setVoiceMemNum('0');
    emit radioStatusUpdated();
}





QString Icom::convertModetoQString(QChar mode)
{

    if (mode == '\x00')
    {
        return (*supportedModes)[0];    // LSB

    }
    else if (mode == '\x01')
    {
        return (*supportedModes)[1];   // USB
    }
    else if (mode == '\x02')
    {
        return (*supportedModes)[2];  // AM
    }
    else if (mode == '\x03')
    {
        return (*supportedModes)[3];    // CW
    }
    else if (mode == '\x04')
    {
        return (*supportedModes)[4];  // RTTY
    }
    else if (mode == '\x05')
    {
        return (*supportedModes)[5];  // FM
    }
    else if (mode == '\x07')
    {
        return (*supportedModes)[6]; //CW-R
    }
    else if (mode == '\x08')
    {
        return (*supportedModes)[7]; //RTTY-R
    }


     return "";


}


char Icom::convertQStringtoModeCode(QString m)
{
    char c = '\x01';

    if (modeStrToModeTable.contains(m))
    {

        c = modeStrToModeTable.value(m)[0];
        return c;
    }
    else
    {
        return '\xff'; // error
    }
}

