#ifndef RIGFACTORY_H
#define RIGFACTORY_H

#include <QObject>
#include <QComboBox>
#include "rigcapabilities.h"
#include "rigbase.h"
#include "radiostatus.h"
#include "configrotserial.h"

enum RigId
{
    KenwoodStartId = 1000,
    KenwoodEndId = 1100,
    IcomStartId = 2000,
    IcomEndId = 2100,
    YaesuStartId = 3000,
    YaesuEndId = 3100,
    NewYaesuStartId = 4000,
    NewYaesuEndId = 4100,
    ElecraftStartId = 5000,
    ElecraftEndId = 5050
};



class RigFactory : public QObject
{
    Q_OBJECT
public:
    typedef QMap<QString, RigCapabilities> Rigs;

    explicit RigFactory(QObject *parent = nullptr);
    ~RigFactory();

    RigBase* createRigs(int rigId, QString modelName, RadioStatus *radioStatus, ConfigRotSerial *configSerial);
    Rigs* supported_rigs();

    void populateComboRigList(QComboBox* comBox);

signals:

private:

    Rigs rigsList;

};

#endif // RIGFACTORY_H
