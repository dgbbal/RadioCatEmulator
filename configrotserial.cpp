#include "configrotserial.h"
#include <QtDebug>
#include <QTimer>



QSerialPort::BaudRate baudRateValue[] = {QSerialPort::Baud4800, QSerialPort::Baud9600, QSerialPort::Baud19200, QSerialPort::Baud38400};

ConfigRotSerial::ConfigRotSerial(QObject *parent) : QObject(parent)
{
    ctsTimer = new QTimer(this);
    connect(ctsTimer, SIGNAL(timeout()), this, SLOT(onCtsTimeout()));
}


void ConfigRotSerial::sendCatMessage(const QByteArray &msg)
{

    if (openFlag)
    {

        //qDebug() << "tx msg = " << msg.toHex(':');
        sComPort->write(msg);
    }
    else
    {
        //qDebug() << "tx error";
        emit txError();
    }

}

bool ConfigRotSerial::openComport(const QString comport, int baudRateIdx, QSerialPort::DataBits databits, QSerialPort::StopBits stopbits)
{
    sComPort = new QSerialPort;
    sComPort->setPortName(comport);
    sComPort->setBaudRate(baudRateValue[baudRateIdx]);
    sComPort->setDataBits(databits);
    sComPort->setStopBits(stopbits);
    connect(sComPort, SIGNAL(readyRead()), this, SLOT(onReadyRead()));

    openFlag = sComPort->open(QIODevice::ReadWrite);
    //if (openFlag)
    //{
    //    sComPort->setFlowControl(QSerialPort::HardwareControl);
    //}




    return openFlag;

}


void ConfigRotSerial::setFlowControl(QSerialPort::FlowControl flowControl)
{
    sComPort->setFlowControl(flowControl);
}


void ConfigRotSerial::startPttTimer(int duration)
{
    ctsTimer->start(duration);
}



void ConfigRotSerial::stopPttTimer()
{
    ctsTimer->stop();
}

void ConfigRotSerial::closeComport()
{

    if (openFlag)
    {
        if (ctsTimer->isActive())
        {
            ctsTimer->stop();
        }
        sComPort->close();
        delete sComPort;
        openFlag = false;
    }

}

void ConfigRotSerial::onReadyRead()
{

    buffer = sComPort->readAll();

    emit dataReceived();

}

QByteArray* ConfigRotSerial::getRxBuffer()
{
    return &buffer;
}

QString ConfigRotSerial::getReceivedMsg()
{
    return inMsg;
}

bool ConfigRotSerial::getOpenFlag()
{
    return openFlag;
}

QString ConfigRotSerial::error()
{
    QSerialPort::SerialPortError err = sComPort->error();
    return comportErrMsgs[err];
}


void ConfigRotSerial::fillPortsInfo(QComboBox* comportSel)
{

    comportSel->clear();

    QString description;
    QString manufacturer;
    QString serialNumber;

    comportSel->addItem("");

    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        QStringList list;
        description = info.description();
        manufacturer = info.manufacturer();
#if QT_VERSION > QT_VERSION_CHECK(5, 3, 0)
        serialNumber = info.serialNumber();
#endif
        list << info.portName()
             << (!description.isEmpty() ? description : blankString)
             << (!manufacturer.isEmpty() ? manufacturer : blankString)
             << (!serialNumber.isEmpty() ? serialNumber : blankString)
             << info.systemLocation()
             << (info.vendorIdentifier() ? QString::number(info.vendorIdentifier(), 16) : blankString)
             << (info.productIdentifier() ? QString::number(info.productIdentifier(), 16) : blankString);


        comportSel->addItem(list.first(), list);

    }


}

void ConfigRotSerial::onCtsTimeout()
{

    if (sComPort && sComPort->isOpen())
    {
        //QSerialPort::PinoutSignals s = sComPort->pinoutSignals();
        if (sComPort->pinoutSignals() & QSerialPort::ClearToSendSignal)
        {
            if (!ctsFlag)
            {
                ctsFlag = true;
                emit ctsOn(true);
            }

        }
        else
        {
            if (ctsFlag)
            {
                ctsFlag = false;
                emit ctsOn(false);
            }

        }
    }
}
