QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    LogEvents.cpp \
    MLogFile.cpp \
    MTrace.cpp \
    bandlist.cpp \
    configrotserial.cpp \
    cutils.cpp \
    elecraft.cpp \
    fileutils.cpp \
    freqlineedit.cpp \
    icom.cpp \
    kenwood.cpp \
    main.cpp \
    newyaesu.cpp \
    radiocatemulatormainwindow.cpp \
    radiostatus.cpp \
    rigbase.cpp \
    rigcapabilities.cpp \
    rigfactory.cpp \
    rigutils.cpp \
    ritlineedit.cpp \
    utils.cpp \
    yaesu.cpp

HEADERS += \
    LogEvents.h \
    MLogFile.h \
    MTrace.h \
    bandlist.h \
    base_pch.h \
    configrotserial.h \
    cutils.h \
    elecraft.h \
    fileutils.h \
    freqlineedit.h \
    icom.h \
    kenwood.h \
    mqtUtils_pch.h \
    newyaesu.h \
    radiocatemulatormainwindow.h \
    radiostatus.h \
    rigCommon.h \
    rigbase.h \
    rigcapabilities.h \
    rigfactory.h \
    rigutils.h \
    ritlineedit.h \
    utils.h \
    yaesu.h

FORMS += \
    radiocatemulatormainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
