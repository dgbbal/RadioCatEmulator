#include "radiostatus.h"

RadioStatus::RadioStatus(QObject *parent) : QObject(parent)
{

}


void RadioStatus::clear()
{
    freqA = 0;
    currentBandA = "";
    modeA = "";

    freqB = 0;
    currentBandB = "";
    modeB = "";

    rxNum = 0;

    vfo =true;

    ritFreq = 0;
    ritStatus = false;

    volumeLevel[0] = 50;
    volumeLevel[1] = 127;

    voiceMemNum = '0';

    morseText.clear();


}
