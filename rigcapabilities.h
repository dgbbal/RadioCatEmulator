#ifndef RIGCAPABILITIES_H
#define RIGCAPABILITIES_H

#include <QString>
#include <QStringList>
#include <QList>
#include <QVariant>


enum CATCAPTURE {NONE = 0, YAESU, ICOM, KENWOOD};

class RigCapabilities
{
public:
    RigCapabilities(QString rigManufacturer_ = "",
                    QString rigName_ = "",
                    QString rigModelName_ = "",
                    int rigModelNumber_ = 0,
                    QChar civAddress_ = ' ',
                    QString kenwoodYaesuId_ = "",
                    int volLevelMax_ = 0,
                    int volLeveMin_ = 100,
                    int maxRitFreq_ = 9999,
                    QChar terminator_ = ';',
                    QString supportedBands_ = "",
                    QString supportedBandStartFreq_ = "",
                    QString supportedModes_ = "",
                    QString supportedModeCodes_ = "",
                    QString defaultSettings_ = "",
                    CATCAPTURE catCaptureMethod_ = NONE);


    QString rigManufacturer;
    QString rigName;
    QString rigModelName;       // combined manufacturer and rig names
    int rigModelNumber;
    QChar civAddress;
    QString kenwoodYaesuId;
    int volLevelMax;
    int volLevelMin;
    int maxRitFreq;
    QChar terminator;
    QString supportedBands;
    QString supportedBandStartFreq;
    QString supportedModes;
    QString supportedModeCodes;
    QString defaultSettings;
    CATCAPTURE catCaptureMethod;



    RigCapabilities(const RigCapabilities &rigcap);
    static CATCAPTURE getCatCaptureCode(QString catCapture);
};

#endif // RIGCAPABILITIES_H
