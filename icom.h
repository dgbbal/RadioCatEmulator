#ifndef ICOM_H
#define ICOM_H

#include <QObject>


#include "rigbase.h"
#include "rigcapabilities.h"
#include "rigfactory.h"
#include "radiostatus.h"
#include "configrotserial.h"

const QString IC7300 = "Icom IC7300";
const QString IC9700 = "Icom IC9700";

const QChar SELECTED_VFO = '\x00';
const QChar UNSELECTED_VFO = '\x01';

const char DATAMODE_ON = '\x01';
const char DATAMODE_OFF = '\00';

const int MAX_CW_CHAR_COUNT = 30;

class Icom : public RigBase
{
    Q_OBJECT
public:
    explicit Icom(int id, QString modelName, RadioStatus *radioStatus_, ConfigRotSerial *configSerial_, QObject *parent = nullptr);
    virtual ~Icom() override;
    //void waitForCatMessage() override;

    static void register_rigs(RigFactory::Rigs*);
    void initRadio(Bandlist *bandList_, QStringList *supportedModes_, QStringList *supportedModeCodes_, QChar terminator_, QChar civAddress_, QString kenwoodYaesuId_ = "") override;

    void setMaxRitFreq(int f) override;
    void setMinRitFreq(int f) override;
signals:

private slots:
    void onVmsgTimeout();
    void onCwMsgTimeout();
private:

    RadioStatus* radioStatus;

    QByteArray rxMessage;
    QString catTxMessage;
    QString modelName;
    int id;


    char terminator;
    QChar civAddress;
    QChar controllerDefaultAddress;

    Bandlist* bandList;
    //QList<int> supportedBandStartFreq;

    QStringList* supportedModes = nullptr;
    QStringList* supportedModeCodes = nullptr;

    QMap<QString, QByteArray> modeStrToModeTable;


    int maxRitFreq;
    int minRitFreq;

    QTimer* voiceMsgTimer;
    QTimer* cwMsgTimer;

    void handleCatMessage(QByteArray) override;

    void getFreqToSendToCat(QByteArray &f);
    void catReadFreqResponse(QChar cmd);
    void getPreAmble(QByteArray &preAmble, QChar cmd);
    void createMsg(QByteArray &msg, QByteArray &preAmble, QByteArray &body);
    void catSetMode(QChar subCommandNum);
    char convertQStringtoModeCode(QString m);
    QString convertModetoQString(QChar mode);
    void catReadMode(QChar cmd);
    void catReadFilterWidth(QChar cmd);
    void catReadSMeter(QChar cmd, QChar subCmd);
    void catReadVolume(QChar cmd, QChar subCmd);
    void catSetVolume();
    void sendCatOK();
    void catSetDataMode();
    void catReadSatMode(QChar commandNum);
    void catReadTxRxID(QChar cmd);
    void catReadDataMode(QChar cmd);


    void catSetFreq();
    void catSetMainBand();
    void catReadVfoFreq(QChar cmd, QChar subCommandNum);
    void catReadMainBand(QChar cmd, QChar subCommandNum);
    void catReadTXStatus(QChar cmd, QChar subCommandNum);
    void catReadSplitState(QChar cmd);
    void catReadCwPitch(QChar cmd, QChar subCommandNum);
    void catReadRitStatus(QChar cmd, QChar subCommandNum);
    void catSetVfo(QChar commandNum);
    void catReadXitStatus(QChar cmd, QChar subCommandNum);

    void catSetTXVoiceMem(QChar cmd, QChar subCommandNum, QChar subCommandNum1);

    void voiceMemoryStop(QChar subCommandNum);
    void voiceMemoryStart(QChar subCommandNum);

    void catReadRitFreq(QChar commandNum, QChar subCommandNum);
    void catSetRitStatus(QChar commandNum, QChar subCommandNum);
    void catSetRitFreq(QChar cmd, QChar subCommandNum);
    void catSetXitStatus(QChar cmd, QChar subCommandNum);

    void catSetStopMorse();
    void catSetSendMorse(QByteArray msg);
    void catSetTxStatus(QChar cmdN, QChar state);
    void catReadModeAndFilterWidth_SelectedUnselectedVfo(QChar cmd, QChar subCommandNum);
    void catSetModeAndFilterWidth_SelectedUnselectedVfo(QChar subCommandNum);
    void catReadFreq_SelectedUnselectedVfo(QChar commandNum, QChar subCommandNum);
    void catSetFreq_SelectedUnselectedVfo(QChar subCommandNum);
    void catExchangeVFOAandVFOB();
    void catReadKeySpeed(QChar cmd, QChar subCommandNum);
    QByteArray convertFreqToIcomRitData(int freq);
};

#endif // ICOM_H
