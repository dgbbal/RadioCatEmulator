#include "rigbase.h"
#include <QDebug>
#include <QString>

RigBase::RigBase(ConfigRotSerial *configSerial_, QObject *parent) : QObject(parent),
    configSerial(configSerial_)

{

}


void RigBase::KenwoodWaitForCatMessage()
{
    inMsg += configSerial->getBuffer();
    //qDebug() << "inMsg" << inMsg;

    if (inMsg.contains(char(';')))
    {

        do
        {

            QByteArray msg;
            for (int i = 0; i < inMsg.size(); i++)
            {
                //QChar c = inMsg[i];
                msg += inMsg[i];
                //qDebug() << "msg = " << msg;

                if (inMsg[i] == ';')
                {
                    rxMessage = msg;
                    inMsg.remove(0, i + 1);
                    //qDebug() << "rxMessage" << rxMessage;

                    //handleCATMessage();
                    trace("[kenwood rxMessage] " + rxMessage.toHex(':'));

                    emit catRxMsg(rxMessage);

                    break;
                }

            }
        }
        while (inMsg.contains(char(';')));





    }

}


void RigBase::IcomWaitForCatMessage()
{

    inMsg += configSerial->getBuffer();
    qDebug() << "inMsg" << inMsg;

    if (inMsg.contains('\xfd'))
    {

        do
        {

            QByteArray msg;
            for (int i = 0; i < inMsg.size(); i++)
            {

                msg += inMsg[i];
                qDebug() << "msg = " << msg;

                if (inMsg[i] == '\xfd')
                {
                    qDebug() << "msg = " << msg;
                    rxMessage = msg;
                    inMsg.remove(0, i + 1);
                    //qDebug() << "rxMessage" << rxMessage.toHex(':');
                    trace("[rxMessage] " + rxMessage.toHex(':'));
                    if (rxMessage.at(0) == '\xfe' && rxMessage.at(1) == '\xfe')
                    {
                        // echo back message
                        configSerial->sendCatMessage(rxMessage);
                        //handleCATMessage();
                        //emit catRxMsg(QString(rxMessage.toHex(':')));
                        trace("[icom rxMessage] " + rxMessage.toHex(':'));

                        emit catRxMsg(rxMessage);
                    }



                    break;
                }

            }
        }
        while (inMsg.contains('\xfd'));


    }

}


void RigBase::oldYaesuWaitForCatMessage()
{
    inMsg +=configSerial->getBuffer();
    //qDebug() << "inMsg" << inMsg.toHex(':');

    if (inMsg.size() >= 5)
    {
        QByteArray msg;
        for (int i = 0; i < inMsg.size(); i++)
        {
            msg += inMsg.at(i);
            if (msg.size() == 5)
            {
                rxMessage = msg;
                inMsg.remove(0, i + 1);
                trace("[old yaesu rxMessage] " + rxMessage.toHex(':'));
                //handleCATMessage();
                emit catRxMsg(rxMessage);
            }
        }
    }


}


void RigBase::sendCatMsg(QByteArray& msg)
{
    configSerial->sendCatMessage(msg);
    trace("[txMessage] " + msg.toHex(':'));
    emit catTxMsg(QString(msg.toHex(':')));
}
