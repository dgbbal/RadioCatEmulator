#include "elecraft.h"

#include <QDebug>
#include <QSettings>

Elecraft::Elecraft(int id_, QString modelName_, RadioStatus *radioStatus_, ConfigRotSerial *configSerial_, QObject *parent) : RigBase(configSerial_, parent),
    radioStatus(radioStatus_),
    modelName(modelName_),
    id(id_)
{


}


Elecraft::~Elecraft()
{


}




void Elecraft::register_rigs(RigFactory::Rigs* rigsList)
{

    QSettings settings("./Radios/elecraft.ini", QSettings::IniFormat);

    QStringList availRadios = settings.childGroups();
    int numRadios = availRadios.size();

    for (int i = 0; i <numRadios; i++)
    {
        settings.beginGroup(availRadios[i]);
        QString rigManufacturer = settings.value("rigManufacturer", "").toString();
        QString rigName = settings.value("rigName", "").toString();
        QString rigModelName = rigManufacturer + " " + rigName;

        QString kenwoodYaesuId = "";

        int rigModelNumber = settings.value("rigModelNumber", 0).toInt();

        int volLevelMax = settings.value("maxVol", 0).toInt();
        int volLevelMin = settings.value("minVol", 255).toInt();
        int maxRitFreq = settings.value("maxRit", 9999).toInt();

        QChar terminator = settings.value("terminator", ';').toChar();

        CATCAPTURE catCapMethodCode = RigCapabilities::getCatCaptureCode(settings.value("catCaptureMethod", "").toString().trimmed());


        QString supportedBands = settings.value("supportedBands", "").toString();
        QString supportedBandStartFreq = settings.value("supportedBandStartFreq", "0").toString();
        QString supportedModes = settings.value("supportedModes", "").toString();
        QString supportedModeCodes = settings.value("supportedModeCodes", "-1").toString();
        QString defaultSettings = settings.value("defaultSettings", "").toString();

        QString supportedOpCodesStr = settings.value("supportedOPCodes", "").toString();
        QByteArray supportedOpCodes;
        supportedOpCodes += supportedOpCodesStr.toLatin1();
        settings.endGroup();

        QChar civAddress = ' ';

        (*rigsList)[rigModelName] = RigCapabilities(rigManufacturer,
                                                    rigName,
                                                    rigModelName,
                                                    rigModelNumber,
                                                    civAddress,
                                                    kenwoodYaesuId,
                                                    volLevelMax,
                                                    volLevelMin,
                                                    maxRitFreq,
                                                    terminator,
                                                    supportedBands,
                                                    supportedBandStartFreq,
                                                    supportedModes,
                                                    supportedModeCodes,
                                                    defaultSettings,
                                                    catCapMethodCode);

    }

}

void Elecraft::initRadio( Bandlist* bandList_, QStringList *supportedModes_, QStringList *supportedModeCodes_, QChar terminator_, QChar civAddress_, QString kenwoodYaesuId_)
{

    Q_UNUSED(civAddress_)

    bandList = bandList_;
    supportedModes = supportedModes_;
    supportedModeCodes = supportedModeCodes_;
    terminator = terminator_;

    if ((*supportedModes).size() == (*supportedModeCodes).size())
    {
        for (int i = 0; i < (*supportedModes).size(); i++)
        {
            modeStrToModeTable.insert((*supportedModes)[i], (*supportedModeCodes)[i]);

        }
    }
    else
    {
        trace("Error - supportedModes length != supportedModeCodes length");
    }
}

void Elecraft::setMaxRitFreq(int f)
{
    maxRitFreq = f;
}

void Elecraft::setMinRitFreq(int f)
{
   minRitFreq = f;
}




void Elecraft::handleCatMessage(QByteArray msg)
{
    QString m(msg);
    rxMessage = m;

    qDebug() << "handle message = " << rxMessage;
    if (rxMessage.contains("FA"))
    {
        if (rxMessage.size() == 3)
        {
            readCatFA_Response();
        }
        else
        {
            setFACommand();

        }

    }
    else if (rxMessage.contains("FB"))
    {
        if (rxMessage.size() == 3)
        {
            readCatFB_Response();
        }
        else
        {
            setFBCommand();
        }

    }
    else if (rxMessage.contains("BW")) // K3
    {
        QString subRx = "";
        if (rxMessage.size() == 3)
        {
            readCatBW_Response(subRx);
        }
        else if (rxMessage.mid(4,1) == "$")
        {
            subRx = rxMessage.mid(4,1);
            readCatBW_Response(subRx);
        }
    }
 /*
    else if (rxMessage.contains("DA"))
    {
        if (rxMessage.size() == 3)
        {
            readCatDA_Response();
        }
    }
    */
    else if (rxMessage.contains("ID"))   // K3
    {
        if (rxMessage.size() == 3)
        {
            readCatID_Response();
        }
    }
    else if (rxMessage.contains("IF"))
    {

        if (rxMessage.size() == 3)
        {

            readCatIF_Response();
        }
    }
    else if (rxMessage.contains("AI"))
    {
        if (rxMessage.size() == 3)
        {
            readCatAI_Response();
        }
    }
    else if (rxMessage.contains("K2")) // K3
    {
        if (rxMessage.size() == 3)
        {
            readCatK2_Response();
        }
    }
    else if (rxMessage.contains("K3")) // K3
    {
        if (rxMessage.size() == 3)
        {
            readCatK3_Response();
        }
    }
    else if (rxMessage.contains("PS"))
    {
        if (rxMessage.size() == 3)
        {
            readCatPS_Response();
        }
    }
    else if (rxMessage.contains("MD"))
    {
        if (rxMessage.size() == 3)
        {
            readCatMD_Response();
        }
        else
        {
            setMDCommand();
        }
    }
    else if (rxMessage.contains("SM"))
    {
        QString subRx = "";
        if (rxMessage.size() == 3)
        {
               readCatSM_Response(subRx);
        }
        else if (rxMessage.mid(2,1) == "$")
        {
             subRx = rxMessage.mid(2,1);
             readCatSM_Response(subRx);
        }

    }
    else if (rxMessage.contains("AG"))
    {

        QString subRx = "";
        if (rxMessage.mid(2,1) == "$")
        {
            subRx = "$";
            if (rxMessage.size() == 4)
            {
                readCatAG_Response(subRx);
            }
            else if (rxMessage.size() == 6)
            {
                setAGCommand(subRx, rxMessage.mid(3, 3));
            }

        }
        else
        {
            if (rxMessage.size() == 3)
            {
                readCatAG_Response(subRx);
            }
            else if (rxMessage.size() == 5)
            {
                setAGCommand(subRx, rxMessage.mid(2, 3));
            }
        }


        if (rxMessage.size() == 3)
        {
               readCatAG_Response(subRx);
        }
        else if (rxMessage.mid(2,1) == "$")
        {
            if (rxMessage.size() == 4)
            {
                subRx = rxMessage.mid(2,1);
                readCatAG_Response(subRx);
            }
            else
            {

            }


        }



    }
    else if (rxMessage.contains("RX"))
    {
        if (rxMessage.size() == 3)
        {
            readCatRX_Response();
        }
    }
 /*
    else if (rxMessage.contains("FV"))
    {
        readCatFV_Response();
    }
 */
    else if (rxMessage.contains("RC"))
    {
        setRCCommand();
    }
    else if (rxMessage.contains("RD"))
    {
        if (rxMessage.size() == 3)
        {
            setRIT_Down();
        }
    }
    else if (rxMessage.contains("RO"))
    {
        setRitOffsetCommand();
    }
    else if (rxMessage.contains("RT"))
    {
          if (rxMessage.size() == 3)
          {
              readCatRT_Response();

          }
          else if (rxMessage.size() == 4)
          {
              setRTCommand();
          }
    }
    else if (rxMessage.contains("RU"))
    {
        if (rxMessage.size() == 3)
        {
            setRIT_Up();
        }
    }

    else if (rxMessage.contains("RV"))  // K3
    {
        if (rxMessage.size() == 4)
        {
            QString subCmd = rxMessage.mid(2, 1);
            readCatRV_response(subCmd);
        }
    }
}


void Elecraft::readCatK2_Response()
{

    QString preamble = "K2";
    QString terminator = ";";
    QString msg = "0";

    assembleMsgToSendToCat(preamble, msg, terminator);
    sendCatTxMsg();

}


void Elecraft::readCatK3_Response()
{

    QString preamble = "K3";
    QString terminator = ";";
    QString msg = "0";

    assembleMsgToSendToCat(preamble, msg, terminator);
    sendCatTxMsg();

}

void Elecraft::readCatBW_Response(QString subRx)
{
    QString preamble = "BW";
    QString terminator = ";";

    QString msg;
    if (subRx.isEmpty())
    {
        msg = "2700";
    }
    else
    {
        msg = subRx + "2700";
    }

    assembleMsgToSendToCat(preamble, msg, terminator);
    sendCatTxMsg();
}

void Elecraft::readCatDA_Response()
{
    QString preamble = "DA";
    QString terminator = ";";
    QString msg = "0";




    assembleMsgToSendToCat(preamble, msg, terminator);
    sendCatTxMsg();

}


void Elecraft::readCatFA_Response()
{
    QString preamble = "FA";
    QString terminator = ";";
    QString msg;

    QString freq;

    getFreqToSendToCat(freq, radioStatus->VFOA);

    assembleMsgToSendToCat(preamble, freq, terminator);
    sendCatTxMsg();

}

void Elecraft::readCatFB_Response()
{
    QString preamble = "FB";
    QString terminator = ";";
    QString msg;

    QString freq;

    getFreqToSendToCat(freq, radioStatus->VFOB);

    assembleMsgToSendToCat(preamble, freq, terminator);
    sendCatTxMsg();
}


void Elecraft::assembleMsgToSendToCat( QString &preamble, QString &msgBody, QString &terminator)
{

    catTxMessage = preamble + msgBody + terminator;



}



void Elecraft::getFreqToSendToCat(QString &f, bool vfo)
{
    if (vfo)
    {
        f = QString::number(radioStatus->getFreqA());
    }
    else
    {
        f = QString::number(radioStatus->getFreqB());
    }
    while (f.size() != 11)
    {
        f.prepend("0");
    }
}


void Elecraft::getRitFreqToSendToCat(QString &f)
{
    f = QString::number(radioStatus->getRitFreq());

    int lenRitFreq = 4;

    while (f.size() != lenRitFreq)
    {
        f.prepend("0");
    }

    if (radioStatus->getRitFreq() < 0)
    {
        f.prepend("-");
    }
    else
    {
        f.prepend("+");
    }
}

void Elecraft::readCatAI_Response()
{
    QString preamble = "AI";
    QString terminator = ";";
    QString body = "1";

    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}

void Elecraft::readCatMD_Response()
{
    QString preamble = "MD";
    QString terminator = ";";
    QString msgBody = getStatusOfMode();

    assembleMsgToSendToCat(preamble, msgBody, terminator);
    sendCatTxMsg();
}


QString Elecraft::getStatusOfMode()
{
    QString m;
    if (radioStatus->getVfo())
    {
        m = radioStatus->getModeA();

    }
    else
    {
        m = radioStatus->getModeB();
    }
    m = convertQStringtoModeCode(m);
    qDebug() << "m = " << m;
    return m;
}

void Elecraft::readCatRX_Response()
{
    QString preamble = "RX";
    QString terminator = ";";
    QString body = "0";

    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}


void Elecraft::readCatRV_response(QString subCmd)
{
    QString preamble = "RV";
    QString terminator = ";";
    QString body;

    if (subCmd == "M")
    {
        body = "M01.00";        // Dummy Version number
    }
    else if (subCmd == "D")
    {
        body = "D01.00";
    }
    else if (subCmd == "A")
    {
        body = "A01.00";
    }
    else if (subCmd == "R")
    {
        body = "R01.00";
    }
    else if (subCmd == "F")
    {
        body = "F01.00";
    }

    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}



void Elecraft::readCatSM_Response(QString subRx)
{

    QString preamble = "SM";
    QString terminator = ";";
    QString body;
    QString strength;

    if (subRx.isEmpty())
    {
       strength = "0010";
       //while (strength.size() < 3)
       //{
      //     strength.prepend('0');
      // }
       body = strength;
    }
    else
    {
       strength = "0010";
       //while (strength.size() < 3)
       //{
       //    strength.prepend('0');
       //}
       body = subRx + strength;

    }


    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}

void Elecraft::readCatAG_Response(QString subRx)
{

    QString preamble = "AG";
    QString terminator = ";";
    QString body;
    QString vol;

    if (subRx.isEmpty())
    {
       vol = QString::number(radioStatus->getVolumeLevel(0));
       while (vol.size() < 3)
       {
           vol.prepend('0');
       }
       body = vol;
    }
    else
    {
       vol = QString::number(radioStatus->getVolumeLevel(1));
       while (vol.size() < 3)
       {
           vol.prepend('0');
       }
       body = subRx + vol;

    }


    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}

void Elecraft::readCatPS_Response()
{

    QString preamble = "PS";
    QString terminator = ";";
    QString body = "1"; //power on

    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}

void Elecraft::readCatFV_Response()
{
    QString preamble = "FV";
    QString terminator = ";";
    QString body = "1.00";

    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}

void Elecraft::readCatID_Response()
{
    QString preamble = "ID";
    QString terminator = ";";
    QString id = "017";

    assembleMsgToSendToCat(preamble, id, terminator);
    sendCatTxMsg();
}

void Elecraft::readCatRT_Response()
{
    QString preamble = "RT";
    QString terminator = ";";

    QString body;
    if (radioStatus->getRitState())
    {
        body = "1";
    }
    else
    {
        body = "0";
    }

    assembleMsgToSendToCat(preamble, body, terminator);
    sendCatTxMsg();
}

void Elecraft::readCatIF_Response()
{
    QString preamble = "IF";
    QString terminator = ";";
    QString msgBody;

    QString freq;                   // operating freq
    getFreqToSendToCat(freq, radioStatus->VFOA);

    msgBody += freq;      // space

    msgBody += ("     ");

    QString ritFreq;
    getRitFreqToSendToCat(ritFreq);

    msgBody += ritFreq.mid(0,1);  // +/-
    msgBody += ritFreq.mid(1,4);

    QString ritStatus;
    if (radioStatus->getRitState())
    {
        ritStatus = "1";
    }
    else
    {
        ritStatus = "0";
    }

    msgBody += ritStatus;

    QString xitStatus = "0";
    msgBody += xitStatus;

    msgBody += " 00";

    QString txStatus;
    if (radioStatus->getTxState())
    {
        txStatus = "1";
    }
    else
    {
        txStatus = "0";
    }

    msgBody += txStatus;

    QString mode = getStatusOfMode();

    msgBody += mode;


    QString vfo;

    if (radioStatus->getVfo())
    {
        vfo = "0"; // VFOA P10
    }
    else
    {
        vfo = "1";
    }

    msgBody += vfo;

    QString scan = "0";

    msgBody += scan;

    QString split;
    if (radioStatus->getSplit())
    {
        split = "1";
    }
    else
    {
        split = "0";
    }

    msgBody += split;

    msgBody += "0";        // b
    msgBody += "0";        // d
    msgBody += "0";     // l ???
    msgBody += " ";


    assembleMsgToSendToCat(preamble, msgBody, terminator);
    sendCatTxMsg();


}







void Elecraft::setFACommand()
{

    QStringList sl = rxMessage.split('A');
    if (sl.size() == 2)
    {
        QString freq = sl[1].remove(';');
        if (freq.size() == 11)
        {
            freq.remove( QRegularExpression("^[0]*") ); // remove leading zeros
            //radioStatus->setCurrentFreqA(freq);
            radioStatus->setFreqA(freq.toULong());
            emit radioStatusUpdated();
        }
    }
}

void Elecraft::setFBCommand()
{

    QStringList sl = rxMessage.split('B');
    if (sl.size() == 2)
    {
        QString freq = sl[1].remove(';');
        if (freq.size() == 11)
        {
            freq.remove( QRegularExpression("^[0]*") ); // remove leading zeros
            //radioStatus->setCurrentFreqB(freq);
            radioStatus->setFreqB(freq.toULong());
            emit radioStatusUpdated();
        }
    }
}

void Elecraft::setAGCommand(QString subRx, QString value)
{
    bool ok = false;
    int volVal = 0;

    if (subRx.isEmpty())
    {
        // main Rx
        volVal = value.toInt(&ok);
        if (ok)
        {
           radioStatus->setVolumeLevel(volVal, 0); //  main
           emit radioStatusUpdated();
        }


    }
    else
    {
        volVal = value.toInt(&ok);
        if (ok)
        {
           radioStatus->setVolumeLevel(volVal, 1); // sub
           emit radioStatusUpdated();
        }
    }




    if (rxMessage.size() == 6 )
    {
        bool ok = false;
        int volVal = rxMessage.mid(2, 3).toInt(&ok);
        if (ok)
        {
           radioStatus->setVolumeLevel(volVal, 0); // force main

        }

    }

}


void Elecraft::setRTCommand()
{
    if (rxMessage.size() == 4)
    {
        QString state = rxMessage.mid(2,1);
        if (state == "1")
        {
            radioStatus->setRitState(true);
        }
        else
        {
            radioStatus->setRitState(false);
        }
        emit radioStatusUpdated();
    }
}

void Elecraft::setRIT_Up()
{
    int f = radioStatus->getRitFreq() + 1;
    if (f > maxRitFreq)
    {
        return;
    }

    radioStatus->setRitFreq(f);
    emit radioStatusUpdated();

}


void Elecraft::setRIT_Down()
{
    int f = radioStatus->getRitFreq() - 1;
    if (f < maxRitFreq)
    {
        return;
    }

    radioStatus->setRitFreq(f);
    emit radioStatusUpdated();
}

void Elecraft::setRCCommand()
{
    if (rxMessage.size() == 3)
    {
        radioStatus->setRitFreq(0);
    }
    emit radioStatusUpdated();
}


void Elecraft::setRitOffsetCommand()
{
    bool ok;
    int offSet = rxMessage.mid(2,5).toInt(&ok);
    if (ok)
    {
        radioStatus->setRitFreq(offSet);
        emit radioStatusUpdated();

    }
}

void Elecraft::setMDCommand()
{
    if (rxMessage.size() == 4)
    {
        if (radioStatus->getVfo())
        {

            radioStatus->setModeA(convertModetoQString(rxMessage.mid(2,1)));
        }
        else
        {

            radioStatus->setModeB(convertModetoQString(rxMessage.mid(2,1)));
        }
        emit radioStatusUpdated();
    }
}


void Elecraft::sendCatTxMsg()
{
    QByteArray msg = catTxMessage.toLocal8Bit();
    sendCatMsg(msg);
}




QString Elecraft::convertModetoQString(QString mode)
{
    switch(mode.toInt())
    {
        case 1:
            return (*supportedModes)[0];

        case 2:
            return (*supportedModes)[1];

        case 3:
            return (*supportedModes)[2];

        case 4:
            return (*supportedModes)[3];

        case 5:
            return (*supportedModes)[4];

        case 6:
            return (*supportedModes)[5];

        case 7:
            return (*supportedModes)[6];

        case 9:
            return (*supportedModes)[7];

        default:
            return "";


    }


}


QChar Elecraft::convertQStringtoModeCode(QString m)
{
    if (modeStrToModeTable.contains(m))
    {
        return modeStrToModeTable.value(m)[0];
    }
    else
    {
        return '0';
    }
}
