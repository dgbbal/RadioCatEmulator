#include "radiocatemulatormainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    RadioCatEmulatorMainWindow w;
    w.show();
    return a.exec();
}
