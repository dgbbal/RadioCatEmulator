#include "rigcapabilities.h"

RigCapabilities::RigCapabilities(QString rigManufacturer_,
                                 QString rigName_,
                                 QString rigModelName_,
                                 int rigModelNumber_,
                                 QChar civAddress_,
                                 QString kenwoodYaesuId_,
                                 int volLeveLMax_,
                                 int volLevelMin_,
                                 int maxRitFreq_,
                                 QChar terminator_,
                                 QString supportedBands_,
                                 QString supportedBandStartFreq_,
                                 QString supportedModes_,
                                 QString supportedModeCodes_,
                                 QString defaultSettings_,
                                 CATCAPTURE catCaptureMethod_)



{

    rigManufacturer = rigManufacturer_;
    rigName = rigName_;
    rigModelName = rigModelName_;
    rigModelNumber = rigModelNumber_;
    civAddress = civAddress_;
    kenwoodYaesuId = kenwoodYaesuId_;
    volLevelMax = volLeveLMax_;
    volLevelMin = volLevelMin_;
    maxRitFreq = maxRitFreq_;
    terminator = terminator_;
    supportedBands = supportedBands_;
    supportedBandStartFreq = supportedBandStartFreq_;
    supportedModes = supportedModes_;
    supportedModeCodes = supportedModeCodes_;
    defaultSettings = defaultSettings_;
    catCaptureMethod = catCaptureMethod_;
}


RigCapabilities:: RigCapabilities( const RigCapabilities &rigcap)
{


    rigManufacturer = rigcap.rigManufacturer;
    rigModelName = rigcap.rigModelName;
    rigModelNumber = rigcap.rigModelNumber;
    civAddress = rigcap.civAddress;
    kenwoodYaesuId = rigcap.kenwoodYaesuId;
    volLevelMax = rigcap.volLevelMax;
    volLevelMin = rigcap.volLevelMin;
    maxRitFreq = rigcap.maxRitFreq;
    terminator = rigcap.terminator;
    supportedBands = rigcap.supportedBands;
    supportedBandStartFreq = rigcap.supportedBandStartFreq;
    supportedModes = rigcap.supportedModes;
    supportedModeCodes = rigcap.supportedModeCodes;
    defaultSettings = rigcap.defaultSettings;
    catCaptureMethod = rigcap.catCaptureMethod;

}


CATCAPTURE RigCapabilities::getCatCaptureCode(QString catCapture)
{
    if (catCapture == "Kenwood")
    {
        return CATCAPTURE::KENWOOD;
    }
    else if (catCapture == "Yaesu")
    {
        return CATCAPTURE::YAESU;
    }
    else if (catCapture == "Icom")
    {
        return CATCAPTURE::ICOM;
    }

    return CATCAPTURE::NONE;
}
